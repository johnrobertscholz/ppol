import os
import re
from numpy.distutils.core import setup


def find_version(*paths):
    fname = os.path.join(os.path.dirname(__file__), *paths)
    with open(fname) as fp:
        code = fp.read()
    match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]", code, re.M)
    if match:
        return match.group(1)
    raise RuntimeError("Unable to find version string.")


setup(
    name             = 'ppol',
    version          = find_version('ppol', '__init__.py'),
    keywords         = ["P-wave polarization", "seismometer orientation", "Obspy", "seismology", "geophysics", "seismic anisotropy"],
    description      = 'Calculate P-wave polarizations on earthquake data, using Python & Obspy',
    author           = 'John-Robert Scholz',
    maintainer       = 'John-Robert Scholz',
    maintainer_email = 'john.robert.scholz@gmail.com',
    classifiers      = ['Development Status :: 3 - Alpha',
                       'License :: OSI Approved :: MIT License',
                       'Programming Language :: Python :: 3.6',
                       'Programming Language :: Python :: 3.7'],
    python_requires  =  '>=3.6',
    install_requires = [],
    packages         = ['ppol'],
    scripts          = [os.path.join('Scripts',file) for file in os.listdir('Scripts/')],
    project_urls={  # Optional
        'Source': 'https://gitlab.com/johnrobertscholz/ppol',
        'Documentation' : 'https://ppol.readthedocs.io/en/latest/',
        'Bug report': 'https://gitlab.com/johnrobertscholz/ppol/-/issues'},
    )