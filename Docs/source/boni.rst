.. _boni:

Boni
====

Here is listed what the ``ppol`` package – besides what has been discussed in the :ref:`tutorial` –
can further do for you. Extra gadgets. Boni! Simpy open your Python interpreter
(correct environment), import the desired module and voilà!


----

In the following you will find functions for the module ``ppol.core``.
As the name suggests, these are the core functions needed for the
``ppol`` package. Nevertheless, you can import them and make use 
for 'outside' jobs – if you like!


.. automodule:: ppol.core
    :members: 
    :undoc-members:
    :show-inheritance:

----

In the following you will find functions for the module ``ppol.util``.
As the name suggests, these are the util functions (partly) needed for the
``ppol`` package. Nevertheless, you can import them and use them 
for 'outside' jobs – if you like!

.. automodule:: ppol.util
    :members:
    :undoc-members:
    :show-inheritance:
