Ppol
====


.. automodule:: ppol
    :members: 


----



Guide
^^^^^

.. toctree::
    :maxdepth: 2

    installation
    tutorial
    config_file
    example
    boni


Others
^^^^^^

.. toctree::
    :maxdepth: 1  

    help
    license
    changelog
    contributing


Indices and tables
^^^^^^^^^^^^^^^^^^

.. toctree::
    :includehidden:


* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`