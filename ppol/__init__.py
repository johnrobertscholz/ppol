#!/usr/bin/env python

# Copyright 2019 John-Robert Scholz
#
# This file is part of Ppol.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# -*- coding: utf-8 -*-



"""
**Ppol** – short for **P**-wave **pol**\ arization – is a Python package based on ObsPy_.
Its purpose is to determine (mis-)orientations of the horizontal components of ocean-bottom seismometers (OBS).
It is equally applicable to terrestrial seismometers. 
To retrieve these horizontal sensor orientations, ``ppol`` uses earthquake information and compares expected (theoretical) backazimuth angles with those calculated from earthquake 
P-wave arrivals contained in your waveform data (using 2-D and/or 3-D principle component analysis). 
The package is further capable to improve on the calculated station orientations by fitting for seismic anisotropies and dipping discontinuities beneath the station.

| 
| Find the actual Python code at:
| https://gitlab.com/johnrobertscholz/ppol
| 

The code is based on the following, peer-reviewed paper. If you find ``ppol`` useful, please consider citing it. :)

    Scholz, John-Robert, Guilhem Barruol, Fabrice R. Fontaine, Karin Sigloch, Wayne Crawford, and Martha Deen. 
    "Orienting Ocean-Bottom Seismometers from P-Wave and Rayleigh Wave Polarisations." 
    Geophysical Journal International 208, no. 3 (2017): 1277–1289. https://doi.org/10.1093/gji/ggw426.


.. _ObsPy: https://github.com/obspy/obspy/wiki
"""


### VERSION / AUTHOR
__version__ = '0.0.2'
__author__  = 'John-Robert Scholz'
