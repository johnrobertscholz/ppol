#!/usr/bin/env python

# Copyright 2019 John-Robert Scholz
#
# This file is part of Ppol.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# -*- coding: utf-8 -*-


"""
THIS FILE IS EXECUTED WHEN TYPING `PPOL` IS EXECUTED FROM THE TERMINAL. 
IT HANDLES THE PASSED ARGUMENTS AND EXECUTES THE PPOL FUNCTIONS
`ppol_main` and `ppol_eval` (defined in this file).
"""



#####  python modules import  #####
import os
import sys
import time
import shutil
import warnings
warnings.filterwarnings("ignore", category=UserWarning)
warnings.filterwarnings("ignore", category=FutureWarning)
warnings.filterwarnings("ignore", category=RuntimeWarning)
import datetime
import numpy as np
import pandas as pd


#####  obspy modules import  #####
from obspy import read, read_events, UTCDateTime
from obspy.core.stream import Stream, Trace
from obspy.taup import TauPyModel
from obspy.geodetics.base import gps2dist_azimuth, kilometer2degrees


#####  ppol package imports  #####
from ppol.util import read_config, get_files, _station_orientation_plot, sec2hms
from ppol.math import fit_function1, circ_stat
from ppol.core import ppol, ppol_fit


# P-wave polarization (wrappers to make all measurements & get statistics)
def ppol_main(config_file):

    """
    PERFORM ALL NEEDED PPOL TASKS


    Also calls, at the end, `ppol_eval()` to determine
    statistics for chosen station.
    """

    now = time.time()



    ### CONFIG VARIABLES
    params                     = read_config(config_file=config_file)
    
    data_top_dir               = params['general']['waveform_data']['data_top_dir']
    file_pattern               = params['general']['waveform_data']['file_pattern']
    network_ID                 = params['general']['station']['network_ID']
    station_ID                 = params['general']['station']['station_ID']
    channel_ID                 = params['general']['station']['channel_ID']
    station_lat                = params['general']['station']['latitude']
    station_lon                = params['general']['station']['longitude']
    
    Z_comp                     = params['general']['components']['Z_comp']
    N_comp                     = params['general']['components']['N_comp']
    E_comp                     = params['general']['components']['E_comp']
    catalog_file               = params['general']['catalog_file']
    output_dir                 = params['general']['output_dir']
    save_individual_ppol_plots = params['general']['save_individual_ppol_plots']
    
    v_model                    = params['phase']['v_model']
    phase_list                 = params['phase']['phase_list']
    
    linear                     = params['pre_processing']['linear']
    demean                     = params['pre_processing']['demean']
    taper                      = params['pre_processing']['taper']
    
    filters                    = params['measurement']['filters']
    presets                    = params['measurement']['presets']
    offsets                    = params['measurement']['offsets']



    ### OTHER VARIABLES
    files           = get_files(data_top_dir, pattern=file_pattern)
    net_sta_cha_ID  = '%s.%s.%s' % (network_ID, station_ID, channel_ID)
    outfile_text    = os.path.join( output_dir, '%s_ppol_results_all.txt' % net_sta_cha_ID)
    if save_individual_ppol_plots:
        output_dir_plot = os.path.join(output_dir, 'individual_ppol_plots')
        if os.path.exists(output_dir_plot):
            shutil.rmtree(output_dir_plot)
        os.makedirs( output_dir_plot )


    measurements    = []
    print()
    print('----------' + len(net_sta_cha_ID)*'-')
    print(u'PPOL for: %s' % net_sta_cha_ID)
    print('----------' + len(net_sta_cha_ID)*'-')



    ### READ CATALOG
    try:
        catalog = read_events( catalog_file )
        print()
        print(catalog)
        #print(catalog.__str__(print_all=True))
    except Exception as err:
        print(u"ERROR: Seems likes there is no (proper) catalog file given:")
        print(err)
        sys.exit()



    ### READ FILES' META-DATA (headonly=True)
    files_container = []
    for file in files:
        try:
            files_container.append( read(file, headonly=True) )
        except:     #non-seismoligcal files that ObsPy's read cannot understand
            pass



    ### LOOP STATION / EVENT / PHASE / FILTER PAIRS and CALCULATE PPOL FOR EACH
    header          = u'#USED           ID    STA_LAT    STA_LON             EVE_TIME    EVE_LAT    EVE_LON    EVE_DEP  EVE_MAG  EVE_EPI  EVE_DIS  EVE_BAZ  PHA_NAM  PHA_INC  PHA_TTs              PHA_ARR  FILTER                PRESET  OFFSET  BAZ_2D  ORI_2D  BAZ_ERR_2D  BAZ_3D  ORI_3D  BAZ_ERR_3D  INC_2D  INC_ERR_2D  INC_3D  INC_ERR_3D   SNR_HOR_2D       SNR_3D   SNR_RZ_2D   SNR_RZ_3D  POL_HOR_2D  POL_3D  POL_RZ_2D  POL_RZ_3D'
    line_formatter  = u'%5d  %11s  %9.4f  %9.4f  %15s  %9.4f  %9.4f  %9.1f  %7.1f  %7.1f  %7.1f  %7.1f  %7s  %7.1f  %7.1f  %15s  %-20s  %6.1f  %6.1f  %6.1f  %6.1f  %10.1f  %6.1f  %6.1f  %10.1f  %6.1f  %10.1f  %6.1f  %10.1f  %11.1f  %11.1f  %10.1f  %10.1f  %10.3f  %6.3f  %9.3f  %9.3f'
    print()
    print(u'Running Analysis:')
    i = 0
    for event in catalog:

        # event parameter
        event_time    = event.preferred_origin().time              or event.origins[0].time 
        event_lat     = event.preferred_origin().latitude          or event.origins[0].latitude
        event_lon     = event.preferred_origin().longitude         or event.origins[0].longitude
        event_dep     = event.preferred_origin().depth/1000.       or event.origins[0].depth/1000.     # in km
        event_mag     = event.preferred_magnitude().mag            or event.magnitudes[0].mag           
        event_magtype = event.preferred_magnitude().magnitude_type or event.magnitudes[0].magnitude_type
        event_dist_m, AZ_stat2event, AZ_event2stat = gps2dist_azimuth(station_lat, station_lon, event_lat, event_lon, a=6378137.0, f=0.0033528106647474805)
        event_dist_epic                            = kilometer2degrees(event_dist_m/1000., radius=6371)                 # in degree
        event_dist_km = event_dist_m/1000.
        event_BAZth   = AZ_stat2event

        # theoretical travel time and incidence angle. For more, check:
        # https://docs.obspy.org/packages/autogen/obspy.taup.helper_classes.Arrival.html#obspy.taup.helper_classes.Arrival
        model      = TauPyModel(model=v_model)
        arrivals   = model.get_travel_times_geo(event_dep, event_lat, event_lon, station_lat, station_lon, phase_list=phase_list)
        phase_used = []

        # all possible arrivals (depending on `v_model` and `phase_list` are analysed)
        for arrival in arrivals:

            phase_name      = arrival.name
            if phase_name in phase_used:    
                # for one v_model / event pair, there are sometimes more than one of the very same phase! (differing for P-phases usually 
                # just a couple of seconds in travel time). Exclude such cases to avoid multiple measurements.
                continue
            phase_INCth     = arrival.incident_angle  
            phase_TT        = arrival.time
            phase_arrival   = UTCDateTime(event_time) + phase_TT

            for preset in presets:
                for offset in offsets:

                    # assign ppol analysis window
                    start_ppol_window = phase_arrival - preset
                    end_ppol_window   = phase_arrival + offset

                    # find all components matching network, station, location. That includes also hydrophone (or other) channels
                    stream_ppol = Stream()
                    for k, stream in enumerate(files_container):
                        for m, trace in enumerate(stream):
                            if net_sta_cha_ID in trace.id:
                                if trace.stats.starttime<=start_ppol_window and trace.stats.endtime>=end_ppol_window:
                                    stream_ppol += read(files[k])[m:m+1]

                    # not enough components found for Ppol analysis
                    num_unique_comps = [tr.id for tr in stream_ppol]
                    if Z_comp:
                        if len(num_unique_comps)<3:
                            continue
                    else:
                        if len(num_unique_comps)<2:
                            continue

                    # Check if any filter is given (also if no filtering is wished, a filter must be specified)
                    if not filters:
                        print(u'WARNING: Ppol Main: At least one filter must be given to run calculations.')
                        print(u'                    If no filtering is wished, set one filter to `False`.')
                        sys.exit()
                    else:
                        print('Processing event: %s (preset=%ss, offset=%ss)' % (event_time.strftime('%Y-%m-%dT%H:%M:%S'), preset, offset))

                    # loop of specified filter. For each, run `ppol_calc`
                    for filter_num in filters.keys():
                        stream_ppol_filt = stream_ppol.copy()
                        filter_type      = filters[filter_num]['type']
                        filter_options   = filters[filter_num]['options']
                        filter_string    = filters[filter_num]['string']

                        if not filter_type or filter_type.lower()=='none':
                            pass
                        else:
                            # pre-process waveforms if a valid filter is specified
                            if linear:
                                stream_ppol_filt.detrend('linear')
                            if demean:
                                stream_ppol_filt.detrend('demean')
                            if taper:
                                stream_ppol_filt.taper(taper['max_percentage'], **taper['options'])
                            stream_ppol_filt.filter(filter_type, **filter_options)
                        stream_ppol_filt.trim(starttime=start_ppol_window, endtime=end_ppol_window)             # all traces important to have same start time & number of samples! 

                        # It could happen that a hyrdophone (or other) channel was in 'stream_ppol' but one of ZNE is missing. Then try block will fail.
                        if not Z_comp or Z_comp.lower()=='none':
                            Z_trace = Trace(np.array([]))
                        else:
                            try:                            
                                Z_trace = stream_ppol_filt.select(channel=Z_comp)[0]
                            except IndexError:
                                print(u"WARNING: Ppol Main: No data for Z-component '%s' found. Please check config file and/or header information." % Z_comp)
                                continue

                        try:
                            N_trace = stream_ppol_filt.select(channel=N_comp)[0]
                        except IndexError:
                            print(u"WARNING: Ppol Main: No data for N-component '%s' found. Please check config file and/or header information." % N_comp)
                            continue

                        try:
                            E_trace = stream_ppol_filt.select(channel=E_comp)[0]
                        except IndexError:
                            print(u"WARNING: Ppol Main: No data for E-component '%s' found. Please check config file and/or header information." % E_comp)
                            continue

                        # ppol_calc
                        ppol_measure = ppol(comp_N=N_trace, comp_E=E_trace, comp_Z=Z_trace)
                        BAZ_2D       = ppol_measure.results[0]
                        BAZ_err_2D   = ppol_measure.results[1]
                        BAZ_3D       = ppol_measure.results[2]
                        BAZ_err_3D   = ppol_measure.results[3]
                        INC_2D       = ppol_measure.results[4]
                        INC_err_2D   = ppol_measure.results[5]
                        INC_3D       = ppol_measure.results[6]
                        INC_err_3D   = ppol_measure.results[7]
                        SNR_HOR_2D   = ppol_measure.results[8]
                        SNR_3D       = ppol_measure.results[9]
                        SNR_RZ_2D    = ppol_measure.results[10]
                        SNR_RZ_3D    = ppol_measure.results[11]
                        POL_HOR_2D   = ppol_measure.results[12]
                        POL_3D       = ppol_measure.results[13]
                        POL_RZ_2D    = ppol_measure.results[14]
                        POL_RZ_3D    = ppol_measure.results[15]

                        # calculate station (mis-)orientation
                        orientation_2D = (event_BAZth-BAZ_2D) % 360             # Clockwise from geographic North to stated `N_comp` in config file
                        orientation_3D = (event_BAZth-BAZ_3D) % 360             # Clockwise from geographic North to stated `N_comp` in config file

                        # get together measurement information
                        measurement    = 0                                           ,\
                                         net_sta_cha_ID                              ,\
                                         station_lat                                 ,\
                                         station_lon                                 ,\
                                         event_time.strftime('%Y-%m-%dT%H:%M:%S')    ,\
                                         event_lat                                   ,\
                                         event_lon                                   ,\
                                         event_dep                                   ,\
                                         event_mag                                   ,\
                                         event_dist_epic                             ,\
                                         event_dist_km                               ,\
                                         event_BAZth                                 ,\
                                         phase_name                                  ,\
                                         phase_INCth                                 ,\
                                         phase_TT                                    ,\
                                         phase_arrival.strftime('%Y-%m-%dT%H:%M:%S') ,\
                                         filter_string                               ,\
                                         preset                                      ,\
                                         offset                                      ,\
                                         BAZ_2D                                      ,\
                                         orientation_2D                              ,\
                                         BAZ_err_2D                                  ,\
                                         BAZ_3D                                      ,\
                                         orientation_3D                              ,\
                                         BAZ_err_3D                                  ,\
                                         INC_2D                                      ,\
                                         INC_err_2D                                  ,\
                                         INC_3D                                      ,\
                                         INC_err_3D                                  ,\
                                         SNR_HOR_2D                                  ,\
                                         SNR_3D                                      ,\
                                         SNR_RZ_2D                                   ,\
                                         SNR_RZ_3D                                   ,\
                                         POL_HOR_2D                                  ,\
                                         POL_3D                                      ,\
                                         POL_RZ_2D                                   ,\
                                         POL_RZ_3D                



                        # store measurements
                        measurements.append( line_formatter % measurement )
                        phase_used.append(phase_name)
                        #print( line_formatter % measurement )

                        # create outplot, if wished
                        if save_individual_ppol_plots:
                            title        = '%s_EQ%s_%s%s_Phase%s_Preset%s_Offset%s_Filter%s' % (net_sta_cha_ID, event_time.strftime('%Y-%m-%dT%H:%M:%S'), event_magtype, event_mag, phase_name, preset, offset, filter_string)
                            outfile_plot = os.path.join( output_dir_plot, title+'.png' )
                            ppol_measure.plot(title=net_sta_cha_ID, verticals=[phase_arrival], outfile=outfile_plot, show=False)
                            print(outfile_plot)



    ### OUTPUT FILE
    np.savetxt(outfile_text, measurements, fmt='%s', header=header, comments='')



    ### ADD STATISTICS TO RESULTS & PLOT STATION ORIENTATION
    if len(measurements) == 0:
        print(u'WARNING: No Ppol measurements were done. This is because no data were found for')
        print(u'         the accordant quake times and/or header information were not matched.')
        print()
    else:
        ppol_eval(config_file, outfile_text)



    ### FINAL OUTPUT SHELL  
    print(outfile_text)
    print()
    print(u'Done in:   %s (h:m:s).' % sec2hms( time.time()-now ))
def ppol_eval(config_file, ppol_results_file_all):

    """
    EVALUATE ALL INDIVIDUAL PPOL MEASUREMENTS
    (STATISTICS PER STATION)


    APPLY CONDITIONS AND ADD STATISTICS AT BOTTOM OF INPUT FILE. PRINT STATION ORIENTATION PLOT.
    """



    ### CONFIG VARIABLES
    params     = read_config(config_file)
    
    conditions = params['statistics']['conditions']
    culling    = params['statistics']['culling']
    errors     = params['statistics']['sigmas']
    
    v_model    = params['phase']['v_model']
    N_comp     = params['general']['components']['N_comp']

    
    
    ### READ INPUT FILE (output txt-file of `ppol_main`)
    results_all = pd.read_fwf(ppol_results_file_all, header=0, comment='# ')
    results_all.dropna(axis=0, how='all', inplace=True)             # delete empty rows: hack to circumvent pandas bug (not minding `skip_blank_lines=True` command for read_fwf)
    station     = results_all['ID'][0]
    output_retained_file = ppol_results_file_all.replace('all.txt', 'retained.txt')
    output_retained_plot = output_retained_file.replace('retained.txt', 'retained.pdf')



    ### DO STATISTICS BASED ON THE CONFIG FILE CONDITIONS
    # Apply valid conditions (not those that run on only nan columns) and get new orientations in 2D and 3D
    conditions_apply = []
    results_new      = results_all
    if conditions:                                              # one or more conditions given 
        for condition in conditions.keys():

            condition = condition.upper()
            try:
                if results_new.loc[:,condition].isna().all(axis=0): # if column consists only of nan values, e.g. when not using Z-component for POL_3D
                    continue
            except KeyError:
                print(u"WARNING: column '%s' not found in measurements file. Check spelling in config condition block." % condition)
                continue
            
            conditions_apply.append( condition + conditions[condition] )
            condition_command = "results_new['" + condition + "']" + conditions[condition]
            results_new       = results_new.loc[eval(condition_command) ]

    orientations_2D = results_new.loc[:,'ORI_2D']
    orientations_3D = results_new.loc[:,'ORI_3D']

    # Error calculation for means / meadians (cirular statistics)
    mean2D, ang_deviation2D, circ_STD2D, median2D, mad2D, smad2D = circ_stat(orientations_2D)
    mean3D, ang_deviation3D, circ_STD3D, median3D, mad3D, smad3D = circ_stat(orientations_3D)

    STD_factor       = errors['mean']
    SMAD_factor      = errors['median']

    ang_deviation2D *= STD_factor                                   # use angular deviation for error, which is close to circ_STD (matter of choice)
    ang_deviation3D *= STD_factor                                   # use angular deviation for error, which is close to circ_STD (matter of choice)
    smad2D          *= SMAD_factor
    smad3D          *= SMAD_factor

    # Culling of data (retain only measurement within errors and re-calculate circ means/medians + errors)
    if culling:
        cull_string                         = 'data culled'

        # circular mean 2D
        lower_bound                         = (mean2D - ang_deviation2D) % 360
        upper_bound                         = (mean2D + ang_deviation2D) % 360
        if lower_bound<upper_bound:
            results_mean2D                  = results_new.loc[ (results_new['ORI_2D']>=lower_bound) & (results_new['ORI_2D']<=upper_bound)  ]
        else:    # case were upper bound went over 360° and thus is set to something >0 ..
            results_mean2D                  = results_new.loc[ (results_new['ORI_2D']>=lower_bound) | (results_new['ORI_2D']<=upper_bound)  ]
        mean2D, ang_deviation2D, _, _, _, _ = circ_stat( results_mean2D.loc[:,'ORI_2D'] )
        ang_deviation2D                    *= STD_factor
        ang_deviation2D                     = np.min([ang_deviation2D, 180])
        
        # circular median 2D
        lower_bound                         = (median2D - smad2D) % 360
        upper_bound                         = (median2D + smad2D) % 360
        if lower_bound<upper_bound:
            results_median2D                = results_new.loc[ (results_new['ORI_2D']>=lower_bound) & (results_new['ORI_2D']<=upper_bound)  ]
        else:    # case were upper bound went over 360° and is thus set to something >0 ..
            results_median2D                = results_new.loc[ (results_new['ORI_2D']>=lower_bound) | (results_new['ORI_2D']<=upper_bound)  ]
        _, _, _, median2D, _, smad2D        = circ_stat( results_median2D.loc[:,'ORI_2D'] )
        smad2D                             *= SMAD_factor
        smad2D                              = np.min([smad2D, 180])
        
        # circular mean 3D
        lower_bound                         = (mean3D - ang_deviation3D) % 360
        upper_bound                         = (mean3D + ang_deviation3D) % 360
        if lower_bound<upper_bound:
            results_mean3D                  = results_new.loc[ (results_new['ORI_3D']>=lower_bound) & (results_new['ORI_3D']<=upper_bound)  ]
        else:    # case were upper bound went over 360° and is thus set to something >0 ..
            results_mean3D                  = results_new.loc[ (results_new['ORI_3D']>=lower_bound) | (results_new['ORI_3D']<=upper_bound)  ]
        mean3D, ang_deviation3D, _, _, _, _ = circ_stat( results_mean3D.loc[:,'ORI_3D'] )
        ang_deviation3D                    *= STD_factor
        ang_deviation3D                     = np.min([ang_deviation3D, 180])
        
        # circular median 3D
        lower_bound                         = (median3D - smad3D) % 360
        upper_bound                         = (median3D + smad3D) % 360
        if lower_bound<upper_bound:
            results_median3D                = results_new.loc[ (results_new['ORI_3D']>=lower_bound) & (results_new['ORI_3D']<=upper_bound)  ]
        else:   # case were upper bound went over 360° and is thus set to something >0 ..
            results_median3D                = results_new.loc[ (results_new['ORI_3D']>=lower_bound) | (results_new['ORI_3D']<=upper_bound)  ]
        _, _, _, median3D, _, smad3D        = circ_stat( results_median3D.loc[:,'ORI_3D'] )
        smad3D                             *= SMAD_factor
        smad3D                              = np.min([smad3D, 180])

    else:
        cull_string                         = 'data not culled'

        results_mean2D   = results_new
        results_median2D = results_new
        results_mean3D   = results_new
        results_median3D = results_new



    ### PPOL FIT ANISOTROPY AND DIPPING DISCONTINUITIES
    x_mean2D                         = results_mean2D['EVE_BAZ']
    y_mean2D                         = results_mean2D['ORI_2D']
    y_sigma_mean2D                   = results_mean2D['BAZ_ERR_2D']
    initial_guesses                  = [mean2D, 1, 1, 1, 1]
    fits_mean2D, fits_err_mean2D     = ppol_fit(fit_function1, x_mean2D, y_mean2D, sigma=y_sigma_mean2D, p0=initial_guesses)
    residual_mean2D                  = y_mean2D-fit_function1(x_mean2D, *fits_mean2D)
    dof_mean2D                       = len(x_mean2D) - 5
    Xsq_red_mean2D                   = np.sum( (residual_mean2D / y_sigma_mean2D) ** 2) / dof_mean2D
    
    x_median2D                       = results_median2D['EVE_BAZ']
    y_median2D                       = results_median2D['ORI_2D']
    y_sigma_median2D                 = results_median2D['BAZ_ERR_2D']
    initial_guesses                  = [median2D, 1, 1, 1, 1]
    fits_median2D, fits_err_median2D = ppol_fit(fit_function1, x_median2D, y_median2D, sigma=y_sigma_median2D, p0=initial_guesses)
    residual_median2D                = y_median2D-fit_function1(x_median2D, *fits_median2D)
    dof_median2D                     = len(x_median2D) - 5
    Xsq_red_median2D                 = np.sum( (residual_median2D / y_sigma_median2D) ** 2) / dof_median2D
    
    x_mean3D                         = results_mean3D['EVE_BAZ']
    y_mean3D                         = results_mean3D['ORI_3D']
    y_sigma_mean3D                   = results_mean3D['BAZ_ERR_3D']
    initial_guesses                  = [mean3D, 1, 1, 1, 1]
    fits_mean3D, fits_err_mean3D     = ppol_fit(fit_function1, x_mean3D, y_mean3D, sigma=y_sigma_mean3D, p0=initial_guesses)
    residual_mean3D                  = y_mean3D-fit_function1(x_mean3D, *fits_mean3D)
    dof_mean3D                       = len(x_mean3D) - 5
    Xsq_red_mean3D                   = np.sum( (residual_mean3D / y_sigma_mean3D) ** 2) / dof_mean3D
    
    x_median3D                       = results_median3D['EVE_BAZ']
    y_median3D                       = results_median3D['ORI_3D']
    y_sigma_median3D                 = results_median3D['BAZ_ERR_3D']
    initial_guesses                  = [median3D, 1, 1, 1, 1]
    fits_median3D, fits_err_median3D = ppol_fit(fit_function1, x_median3D, y_median3D, sigma=y_sigma_median3D, p0=initial_guesses)
    residual_median3D                = y_median3D-fit_function1(x_median3D, *fits_median3D)
    dof_median3D                     = len(x_median3D) - 5
    Xsq_red_median3D                 = np.sum( (residual_median3D / y_sigma_median3D) ** 2) / dof_median3D
    
    # Ppol fits: A1 must be >=0° & <360° (for very bad fits it's not)
    fits_mean2D[0]                   = fits_mean2D[0]   % 360                   
    fits_median2D[0]                 = fits_median2D[0] % 360
    fits_mean3D[0]                   = fits_mean3D[0]   % 360
    fits_median3D[0]                 = fits_median3D[0] % 360

    # assing chosen sigma multiplicator
    FIT_STD_factor                   = errors['ppol_fit']
    fits_err_mean2D                  = fits_err_mean2D   * FIT_STD_factor
    fits_err_median2D                = fits_err_median2D * FIT_STD_factor
    fits_err_mean3D                  = fits_err_mean3D   * FIT_STD_factor
    fits_err_median3D                = fits_err_median3D * FIT_STD_factor
    
    # there is such thing as maximum error, 180°     (if error is nan, nan is minimum)
    fits_err_mean2D[0]               = np.min([180, fits_err_mean2D[0]  ])     #
    fits_err_median2D[0]             = np.min([180, fits_err_median2D[0]])
    fits_err_mean3D[0]               = np.min([180, fits_err_mean3D[0]  ])
    fits_err_median3D[0]             = np.min([180, fits_err_median3D[0]])



    ### PREPARE TEXT OF STATISTICS
    indices_used_mean2D     = list(results_mean2D.loc[:,'EVE_TIME'].index)
    indices_used_median2D   = list(results_median2D.loc[:,'EVE_TIME'].index)
    indices_used_mean3D     = list(results_mean3D.loc[:,'EVE_TIME'].index)
    indices_used_median3D   = list(results_median3D.loc[:,'EVE_TIME'].index)
    indices_used_all_unique = sorted( set( indices_used_mean2D   + \
                                           indices_used_median2D + \
                                           indices_used_mean3D   + \
                                           indices_used_median3D ))

    results_new             = results_new.loc[indices_used_all_unique,:]        # because culling may have thrown out some measurements
    unique_earthquakes_all  = sorted( set( results_all.loc[:,'EVE_TIME'] ))
    unique_earthquakes_new  = sorted( set( results_new.loc[:,'EVE_TIME'] ))
    conditions_str          = ['CONDITION' if len(conditions_apply)==1 else 'CONDITIONS' for _ in range(1)][0]
    timestamp               = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S')

    statistic = [u'%s %s APPLIED: %s' % (len(conditions_apply), conditions_str, ', '.join(conditions_apply)),
                 u'' ,
                 u'RETAINED %4d OUT OF %5d INDIVIDUAL MEASUREMENTS (USED!=0, %s), coming from'                                  % (len(indices_used_all_unique), len(results_all), cull_string) ,
                 u'         %4d OUT OF %5d UNIQUE EARTHQUAKES (v-model: %s)'                                                    % (len(unique_earthquakes_new), len(unique_earthquakes_all), v_model) ,
                 u'' ,
                 u'Orientations-2D (N --> %s):'                                                                                 % N_comp,
                 u'    - mean       = %5.1f\u00B1%5.1f (%s\u03C3, n=%4d)'                                                       % (mean2D,   ang_deviation2D, STD_factor,   len(results_mean2D)) ,
                 u'    - fit mean   = %5.1f\u00B1%5.1f (%s\u03C3, n=%4d, A2=%4.1f, A3=%4.1f, A4=%4.1f, A5=%4.1f, Xsq_red=%4.f)' % ((fits_mean2D[0],   fits_err_mean2D[0],   FIT_STD_factor, len(results_mean2D))   + tuple(fits_mean2D[1:]) + (Xsq_red_mean2D,)) ,
                 u'    - median     = %5.1f\u00B1%5.1f (%s\u03C3, n=%4d)'                                                       % (median2D, smad2D,  SMAD_factor,          len(results_median2D)) ,
                 u'    - fit median = %5.1f\u00B1%5.1f (%s\u03C3, n=%4d, A2=%4.1f, A3=%4.1f, A4=%4.1f, A5=%4.1f, Xsq_red=%4.f)' % ((fits_median2D[0], fits_err_median2D[0], FIT_STD_factor, len(results_median2D)) + tuple(fits_median2D[1:]) + (Xsq_red_median2D,)) ,
                 u'' ,
                 u'Orientations-3D (N --> %s):'                                                                                 % N_comp,
                 u'    - mean       = %5.1f\u00B1%5.1f (%s\u03C3, n=%4d)'                                                       % (mean3D,   ang_deviation3D, STD_factor,   len(results_mean3D)) ,
                 u'    - fit mean   = %5.1f\u00B1%5.1f (%s\u03C3, n=%4d, A2=%4.1f, A3=%4.1f, A4=%4.1f, A5=%4.1f, Xsq_red=%4.f)' % ((fits_mean3D[0],   fits_err_mean3D[0],   FIT_STD_factor, len(results_mean3D))   + tuple(fits_mean3D[1:]) + (Xsq_red_mean3D,)) ,
                 u'    - median     = %5.1f\u00B1%5.1f (%s\u03C3, n=%4d)'                                                       % (median3D, smad3D,  SMAD_factor,          len(results_median3D)) ,
                 u'    - fit median = %5.1f\u00B1%5.1f (%s\u03C3, n=%4d, A2=%4.1f, A3=%4.1f, A4=%4.1f, A5=%4.1f, Xsq_red=%4.f)' % ((fits_median3D[0], fits_err_median3D[0], FIT_STD_factor, len(results_median3D)) + tuple(fits_median3D[1:]) + (Xsq_red_median3D,)) ,
                 u'' ,
                 u"USED: 0 -> not used, a -> mean2D, b -> median2D, c -> mean3D, d -> median3D. E.g., abcd -> all .." ,
                 u'Timestamp: %s'                                                                                               % timestamp]

    # create good-looking output for later
    len_max_line  = max( [len(line) for line in statistic] )
    formatter_str = "\n#   | %-" + str(len_max_line) + 's |'

    statistic = [formatter_str % line for line in statistic]
    statistic.insert(0,              '\n\n#   +-' +  '-' * len_max_line + '-+'   )
    statistic.insert(len(statistic),   '\n#   +-' +  '-' * len_max_line + '-+\n' )



    ### MARK USED MEASUREMENTS IN INPUT FILE / ADD STATISTICS AT BOTTOM OF INPUT FILE
    with open(ppol_results_file_all, 'r') as fp_in:
        lines = fp_in.readlines()

    with open(ppol_results_file_all, 'w') as fp_out:

        for k, line in enumerate(lines):

            if line[:5].lstrip().isdigit() or line[:5].lstrip().isalpha():

                used = ''
                if k-1 in indices_used_mean2D:
                    used += 'a'
                if k-1 in indices_used_median2D:
                    used += 'b'
                if k-1 in indices_used_mean3D:
                    used += 'c'
                if k-1 in indices_used_median3D:
                    used += 'd'
                if used == '':
                    used = '0'
                
                # assign correct `#USED` factor to `results_new` for later output
                if k-1 in indices_used_all_unique:
                    results_new.loc[k-1,'#USED'] = '%5s' % used

                line = '%5s' % used + line[5:]
                    
            fp_out.write(line)

        for line in statistic:
            fp_out.write(line)



    ### CREATE OUTPUT FILE ONLY CONTAINING RETAINED MEASUREMENTS
    with open(ppol_results_file_all, 'r') as fp_in:
        lines = fp_in.readlines()
    
    k = 0
    with open(output_retained_file, 'w') as fp_out:

        for line in lines:
            if not line.startswith('    0'):
                fp_out.write(line)
                k += 1

            if k-1>=len(indices_used_all_unique):   # avoid writing out earlier statistics, too
                break

        for line in statistic:
            fp_out.write(line)



    ### PLOT STATION ORIENTATION (individual measurements+errors, averaged orientations+errors, ppol-fits+errors)
    results = [[x_mean2D,   y_mean2D,   y_sigma_mean2D,   mean2D,   ang_deviation2D, fits_mean2D,   fits_err_mean2D,   'Mean 2D'  , len(results_mean2D),   N_comp],
               [x_median2D, y_median2D, y_sigma_median2D, median2D, smad2D,          fits_median2D, fits_err_median2D, 'Median 2D', len(results_median2D), N_comp],
               [x_mean3D,   y_mean3D,   y_sigma_mean3D,   mean3D,   ang_deviation3D, fits_mean3D,   fits_err_mean3D,   'Mean 3D'  , len(results_mean3D),   N_comp],
               [x_median3D, y_median3D, y_sigma_median3D, median3D, smad3D,          fits_median3D, fits_err_median3D, 'Median 3D', len(results_median3D), N_comp]]

    _station_orientation_plot(results, station=station, outfile=output_retained_plot, show=False)



    ### OUTPUT TO SHELL
    print()
    print(u'CALCULATED STATISTIC AND ADDED SUMMARY TO RESULTS FILE. HERE (SOME OF) THE RETAINED MEASUREMENTS:')
    with pd.option_context("display.max_rows", 30):
        # implement that one the chosen condition columns will be displayed
        print(type(conditions))
        if conditions is not None and len(conditions.keys()) != 0 :
            columns = ['#USED', 'EVE_TIME', 'EVE_MAG', 'EVE_BAZ', 'BAZ_2D', 'ORI_2D']
            for condition in conditions.keys():
                try:
                    results_new.loc[:,condition]
                    if condition.upper() not in columns:
                        columns += [condition.upper()]
                except:
                    pass
        else:
            columns = ['#USED', 'EVE_TIME','EVE_MAG', 'EVE_BAZ', 'BAZ_2D', 'ORI_2D', 'BAZ_ERR_2D', 'BAZ_3D', 'ORI_3D', 'BAZ_ERR_3D', 'INC_2D', 'INC_3D', 'SNR_HOR_2D', 'POL_HOR_2D']
        print( results_new.loc[:,columns] )
    print()
    print(u'STATISTICS FOR %s:' % station)
    for line in statistic:
        print(line.strip('\n'))
    print()
    print(u'Created files:')
    print(output_retained_file)
    print(output_retained_plot)


#####  _ _ N A M E _ _ = = " _ _ M A I N _ _ "  #####
if __name__ == "__main__":
    pass 