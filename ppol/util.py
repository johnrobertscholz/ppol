#!/usr/bin/env python

# Copyright 2019 John-Robert Scholz
#
# This file is part of Ppol.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# -*- coding: utf-8 -*-


#####  python modules import  #####
import os
import re
import sys
import time
import glob
import yaml
import fnmatch
import datetime
import numpy as np


#####  matplotlib modules import  #####
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import proj3d


#####  obspy modules import  #####
from obspy import read, read_events, UTCDateTime
from obspy.core.stream import Stream, Trace
from obspy.signal import rotate
from obspy.core.event import Catalog


#####  ppol package imports  #####
from ppol.math import fit_function1, split_into_connecting_parts


# Convenient helpers
def sec2hms(seconds, digits=0):

    """
    Convert seconds given as float into hours, minutes and seconds.
    Optional 'digits' determines position after decimal point.

    Returns string.
    """


    string = str( datetime.timedelta(seconds=seconds) )
    parts  = string.split('.')

    try:
        frac_sec = parts[1]
        parts[1] = frac_sec[:digits]

    except IndexError:
        if digits>0:
            parts += [digits*'0']

    string = '.'.join(parts)
    string = string.rstrip('.')

    return string
def get_files(this, pattern='*'):

    """
    Return all files in dir 'this' (and sub-directories) if 'this' is dir,
    or return file (if 'this' is file). You may use 'pattern' to search only for
    certain file name patterns (eg: file endings, station names, ..).

    Script uses os.walk and thus lists all files in a directory, meaning also
    those in sub-directories. Bash wildcards are understood, python regex not. 

    :type this: str
    :param this: file or dir (bash wildcards work)

    :type pattern: str
    :param pattern: pattern to filter file(s) (bash wildcards work)
    
    :return: list of files (or empty list, if no matching files were found)
    """
    

    ### get files from 'this' to loop over
    uber  = glob.glob(this)
    files = []

    try:
        for u in uber:
            if os.path.isfile(u):
                files.append(u)

            else:
                # walk trough (sub)-directory and put ALL files into 'files'
                for root, dirs, files_walk in os.walk(u):
                    for file_walk in files_walk:

                        # take care on 'pattern'
                        file = os.path.join( root,file_walk )
                        if fnmatch.fnmatch( os.path.basename(file), pattern ) and not file in files:
                            files.append( file )

    except Exception:
        files = []

    return sorted(files)
def read_config(config_file):

    """
    Read yaml config file via `full_load`. See details in:
    https://github.com/yaml/pyyaml/wiki/PyYAML-yaml.load(input)-Deprecation

    Included is also a hack to read numbers like `1e-7` as floats and not
    as strings. Shouldn't be like this, but is: See details in:
    https://stackoverflow.com/questions/30458977/yaml-loads-5e-6-as-string-and-not-a-number
    """

    print()
    print(u'Reading config file:')
    print(os.path.abspath(config_file))


    try:
        with open(config_file, 'r') as yamlfile:

            loader = yaml.FullLoader
            loader.add_implicit_resolver(
                u'tag:yaml.org,2002:float',
                re.compile(u'''^(?:
                 [-+]?(?:[0-9][0-9_]*)\\.[0-9_]*(?:[eE][-+]?[0-9]+)?
                |[-+]?(?:[0-9][0-9_]*)(?:[eE][-+]?[0-9]+)
                |\\.[0-9_]+(?:[eE][-+][0-9]+)?
                |[-+]?[0-9][0-9_]*(?::[0-5]?[0-9])+\\.[0-9_]*
                |[-+]?\\.(?:inf|Inf|INF)
                |\\.(?:nan|NaN|NAN))$''', re.X),
                list(u'-+0123456789.'))

            params = yaml.load(yamlfile, Loader=loader)

    except FileNotFoundError:
        print()
        print(u'ERROR: read_config: config file not found.')
        sys.exit()

    return params
def sort_catalog(catalog, chronologic=True):

    """
    Sort ObsPy event catalog by origin times/dates 
    (True=recent events first), as e.g. two merged catalogs will not be date-time
    sorted anymore.
    
    :return: obspy catalogue object or False
    """

    try:
            
        dummy   = [[event, event.origins[0].time] for event in catalog]
        dummy.sort(key=lambda x: x[1], reverse=chronologic)
        events  = [event[0] for event in dummy]
        catalog = Catalog(events=events)

    except Exception as err:
        print(u'WARNING: sort catalog: %s' % err)
        catalog = False

    return catalog
def merge_catalogs(*catalogs, remove_duplicates=True, output='/Users/john/Desktop/catalog.ml', chronologic=True):

    """
    Merge earthquake catalogs and sort them. Identic events will be trown out
    if `remove_duplicates=True`.

    `chronologic=True` means oldest events are first in catalog.
    """

    events = []

    for catalog in catalogs:
        catalog = read_events( catalog )
        for event in catalog:
            if event in events and remove_duplicates:
                continue
            events.append( event )

    catalog = Catalog(events=events)
    catalog = sort_catalog(catalog, chronologic=chronologic)
    catalog.write( output, format="QUAKEML")

    print(u'Merged and sorted catalog(s) to:')
    print(output)
def headercheck_random(mother_dir=None, pattern='*', sample_size=30, level='normal'):

    """
    In case you have many seismological files and would like to
    quickly peek into the headers only, this script will help you.
    It uses ObsPy's `read(headonly=true)` function to do so.


    Parameters:

        mother_dir  (str): Mother directory of of where your files are located.
                           If no mother_dir, then read config. ...
        pattern     (str): File name pattern. This will be used in conunction with `mother_dir` to find all files.
        sample_size (int): Number of files (not traces) you would like to check.
        level       (str): Either `normal` or `dict`...


    Returns:
        None


    Notes: 
        - Traces found in a file are displayed in their order, 
          i.e., no sorting is done.

        - If `sample_size` is > the number of files found, then
          information of all files will be printed (in random order).
    """

    

    ### GET FILES
    if not mother_dir:
        params      = read_config()
        mother_dir  = params['general']['waveform_data']['data_folder']
        pattern     = params['general']['waveform_data']['file_pattern']
    files = get_files(mother_dir, pattern=pattern)



    ### IF NO FILES FOUND
    if not files:
        print(u'Sorry, no files found for the given parameters.')



    ### IF FILES FOUND, LOOP OVER RANDOMLY
    else:
        sample_size  = min([sample_size,len(files)])
        indices      = list( np.random.choice(len(files), sample_size, replace=False) )

        for j in indices:
            print()
            print(u'File (%s/%s):   %s' % (indices.index(j)+1,sample_size,files[j]))
            try:
                st = read(files[j], headonly=True)
            except:
                print('  not a seismological file. SKipped.')
                continue

            # print trace information
            for tr in st:
                if level.lower()=='normal':
                    print(u"%s | %s - %s | (sr=%4.1f Hz, %s samples, format='%s')"     % (tr.id, tr.stats.starttime, tr.stats.endtime, tr.stats.sampling_rate, tr.stats.npts, tr.stats._format))
                else:
                    diction  = getattr(tr.stats, tr.stats._format.lower())
                    dict_str = ', '.join( ['%s=%s' % (key,diction[key]) for key in diction.keys()] )
                    print(u"%s | %s - %s | (sr=%4.1f Hz, %s samples, format='%s', %s)" % (tr.id, tr.stats.starttime, tr.stats.endtime, tr.stats.sampling_rate, tr.stats.npts, tr.stats._format, dict_str))

        # FINAL OUTPUT
        print('Done. You checked on %s / %s possible file(s).' % (sample_size, len(files)) )


# Illustrational
class _Arrow3D(FancyArrowPatch):
    """
    Needed for nice arrow heads in ppol plot.
    Hack found online.
    """
    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0,0), (0,0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))
        FancyArrowPatch.draw(self, renderer)
def _station_orientation_plot(parameters, station='', outfile=None, show=True):

    """
    Helper function to create the station orientation plots
    based on the applied selection criteria (section 
    conditions in config file).

    Do not use it directly, use instread `ppol_eval()`.
    """

    # Figure instance
    fig, axes = plt.subplots(figsize=(12,8), nrows=2, ncols=2)
    fig.suptitle("Ppol results '%s'" % station, fontsize=15)

    # Helper function
    def _plot_axis_station_orientation(ax, x_data, y_data, y_sigmas, average, average_err, fits, fits_err, title, n, N_comp):


        # variables
        x_linspace     = np.linspace(0, 360, 361)
        y_fit          = fit_function1(x_linspace*np.pi/180, *fits)
        index_correct  = np.where((y_fit>=0) & (y_fit< 360))[0]
        index_ncorrect = np.where((y_fit< 0) | (y_fit>=360))[0]


        # set-up
        ax.set_title(title, fontsize=12)
        ax.set_xlim( [0,360] )
        ax.grid(lw=0.25)
        ax.xaxis.set_ticks( np.arange(0,361,45) )


        # shadow average error regions - making sure that above 360° and below 0° plots are correct
        if fits[0]+fits_err[0]>=360:
            ax.fill_between(x_linspace, 0,                         (fits[0]+fits_err[0])%360, facecolor='orange', alpha=0.2)
            ax.fill_between(x_linspace, fits[0]-fits_err[0],       360,                       facecolor='orange', alpha=0.2)
        elif fits[0]-fits_err[0]<0:
            ax.fill_between(x_linspace, 0,                         fits[0]+fits_err[0],       facecolor='orange', alpha=0.2)        
            ax.fill_between(x_linspace, (fits[0]-fits_err[0])%360, 360,                       facecolor='orange', alpha=0.2)
        else:
            ax.fill_between(x_linspace, fits[0]-fits_err[0],       fits[0]+fits_err[0],       facecolor='orange', alpha=0.2)

        if average+average_err>=360:
            ax.fill_between(x_linspace, 0,                         (average+average_err)%360, facecolor='seagreen', alpha=0.2)
            ax.fill_between(x_linspace, average-average_err,       360,                       facecolor='seagreen', alpha=0.2)
        elif average-average_err<0:
            ax.fill_between(x_linspace, 0,                         average+average_err,       facecolor='seagreen', alpha=0.2)      
            ax.fill_between(x_linspace, (average-average_err)%360, 360,                       facecolor='seagreen', alpha=0.2)
        else:
            ax.fill_between(x_linspace, average-average_err,       average+average_err,       facecolor='seagreen', alpha=0.2)
        

        # plot average result
        ax.plot([0,360], [average, average], ls='-', c='seagreen', label=r'%s=(%5.1f$\pm$%4.1f)$^{\circ}$, n=%s' % (title, average, average_err, n))


        # plot Ppol results
        ax.plot([0,360], [fits[0], fits[0]], ls='-', c='orange',   label=r'%s=(%5.1f$\pm$%4.1f)$^{\circ}$, n=%s' % ('Ppol-fit: A1', fits[0], fits_err[0], n))


        ## plot individual measurements + their errors 
        ax.errorbar(x_data, y_data, yerr=y_sigmas, fmt='ko', ls='', markersize=2, elinewidth=1, capsize=2, label='Individual Measurements')
        if np.any(y_data+y_sigmas>360):     # making sure error above 360° are continued at 0°
            ax.errorbar(x_data, y_data-360, yerr=y_sigmas, fmt='ko', ls='', markersize=2, elinewidth=1, capsize=2, label=None)
            ax.set_ylim(0,360)
        if np.any(y_data-y_sigmas<0):       # making sure error below 0° are from 360° downwards
            ax.errorbar(x_data, y_data+360, yerr=y_sigmas, fmt='ko', ls='', markersize=2, elinewidth=1, capsize=2, label=None)
            ax.set_ylim(0,360)
        
        
        # ppol fit function - making sure that above 360° and below 0° plots are correct
        for n, array in enumerate(split_into_connecting_parts(index_correct)):
            if n==0:
                ax.plot(x_linspace[array], (fit_function1(x_linspace[array]*np.pi/180, *fits)), ls='--',  c='orange', label=r'orient($\Theta$)=A1+A2 sin($\Theta$)+A3 cos($\Theta$)+A4 sin(2$\Theta$)+A5 cos(2$\Theta$)')
            else:
                ax.plot(x_linspace[array], (fit_function1(x_linspace[array]*np.pi/180, *fits)), ls='--',  c='orange')
        for array in split_into_connecting_parts(index_ncorrect):
            ax.plot(x_linspace[array], (fit_function1(x_linspace[array]*np.pi/180, *fits))%360, ls='--',  c='orange')


        # order legend
        handles, labels = ax.get_legend_handles_labels()
        #labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
        ax.legend(handles, labels, loc='upper right', prop={'size':6})
    
        return ax


    # Plot axes
    ylims = []
    for m, ax in enumerate(axes.flatten()):

        ax = _plot_axis_station_orientation(ax, *parameters[m])
        ylims.append( ax.get_ylim() )

        # make sure x- and y-labels are only plotted once
        if m==0:                # upper left
            ax.set_ylabel(r'%s orientation clock. from North ($^{\circ}$)' % (parameters[m][-1]), fontsize=10)
        if m==1:                # upper right
            pass
        if m==2:               # lower left
            ax.set_xlabel(r'Event backazimuth $\Theta$ ($^{\circ}$)', fontsize=10)          
            ax.set_ylabel(r'%s orientation clock. from North ($^{\circ}$)' % (parameters[m][-1]), fontsize=10)
        if m==3:                # lower right
            ax.set_xlabel(r'Event backazimuth $\Theta$ ($^{\circ}$)', fontsize=10)          


    # setting correct ylim for all 4 axes
    best_ylim = np.array( [np.min(np.array(ylims)), np.max(np.array(ylims))] )
    best_ylim = [ max(0,best_ylim[0]), min(360,best_ylim[1]) ]  # limit maximum ylim range to 0..360°
    for ax in axes.flatten():
        if best_ylim[1]-best_ylim[0]<1:         # case only nan as angles were originally given (ylim thus something like [0.04, -0.04])
            ax.set_ylim( -15,15 )
        elif best_ylim[1]-best_ylim[0]>=180:    # when large ylim, please use 45° multiplier because results are more intuitive to read
            ax.set_ylim( best_ylim )
            ax.yaxis.set_major_locator(plt.MultipleLocator(45))
        else:
            ax.set_ylim( best_ylim )


    # Closing
    plt.tight_layout(rect=[0, 0.03, 1, 0.95])
    if outfile:
        plt.savefig(outfile)

    if show:        
        plt.show()
    plt.close()
def quick_plot(*y, 
    x            = None, 
    data_labels  = (), 
    ls           = '-',
    lw           = 1.5,
    lc           = None,
    win_title    = '', 
    title        = '',
    title_kwargs = {},
    xlabel       = 'Data points', 
    ylabel       = 'Amplitude',
    grid         = True,
    xscale       = None,
    yscale       = None,
    xinvert      = False, 
    yinvert      = False, 
    xlim         = None, 
    ylim         = None,
    date_format  = '%Y-%m-%d\n%H:%M:%S',
    verts        = None,
    vertsc       = None,
    horis        = None,
    horisc       = None,
    figsize      = (10,8),
    legend_loc   = 'best',
    axis         = False,
    outfile      = None, 
    show         = True, 
    keep_open    = False,
    **kwargs):

    """
    This function allows for some convenient, quick 2-D plotting.
    
    It requires loaded classes `Trace`, `Stream`,
    `UTCDateTime`, and `datetime.datetime`.
    
    Otherwise, it should be very user friendly. Times will
    be displayed as '%d/%m %H:%M:%S', which is harcoded.


    PARAMETERS
    ----------

    `y` : - y-data to be plotted, as many as you like
          - if your data are `Trace` object(s), it is plotted
            with date strings (if no `x` is given).
          - if your data are a `Stream` object, it is plotted
            with their internal plot functions and further parameters
            are not passed.
          - if none of above, then x-data are determined using np.arange
    `x` : - this overrides x-data (dates) when passing `Trace` objects as `y`,
            so you can assign anything as x-data and still conveniently use
            `Trace` objects.
          - `UTCDateTime`, `datetime.datetime`, and `str` objects will
             be converted to `UTCDateTime` and then matplotlib times,
             then plotted as date strings '%d/%m %H:%M:%S'
    `data_labels` : - will appear in legend
                    - should have as many elements as you pass in `y`,
                      otherwise it will not used but data simply numbered
    `lw`       : - linewidth of plots (lines are harcoded, no option to plot points only)
    `win_title`: - title of figures canvas (does not appear in plot)
    `title`    : - sup-title of figure instance (appears as title in plot)
    `xlabel    : - will appear beneath x-axis
    `ylabel    : - will appear next to y-axis
    `x_invert` : - will invert direction of x-axis, if True
    `y_invert` : - will invert direction of y-axis, if True
    `xlim`     : - list (or similar) with two elements. Choose same type as `y`. If 'xlim' is set, `ylim` is set automatically as to the y-range within these xlim
    `ylim`     : - list (or similar) with two elements.
    `verts`    : - either list or list of lists
                 - elements will be interpreted as x-data where vertical lines shall be plotted
                 - each list gets an own colour for all elements
                 - when elements are: - `UTCDateTime`, `datetime.datetime`, and `str` 
                                         --> conversion to matplotlib times (so x-data should be times, too)
                                      - `float` or `int`
                                         --> where you would expect it to happen (each sample on x-axis numbered).
                                             If x-data are times (e.g. matplotlib), you can pass here matplotlib times as well
                                      - `complex` (e.g. 3+0j)
                                         --> real part will be interpreted as relative position on x-axis,
                                             so (0.5+0j) will plot a vertical line in the middle of the x-axis.
                                             Imaginery part is neglected.
    `legend_loc`: - position of legend, by default automaically determined. Choose e.g. 'upper left' or 'lower right' or ...
    `axis`     : - if passed, all will be plotted into this axis and returned and next two options are ignored.                                            
    `outfile`  : - (absolut) path where plot shall be saved. Use endings like '.pdf' or '.png'
    `show`     : - if True (default), plot will be shown, otherwise not until the next show=True command ;)
                 - if False, matplotlib figure instance of is returned so it can be further used
                 - no matter the status of `show`, if an `outfile` is specified a plot will be saved

    NOTES
    -----
      - Matplotlib internally uses matplotlib times, always.
        (example: 733643.01392361, type=numpy.float64)

        This holds true also when extracting times, for example via
        ax.get_ylim() or similar, no matter the original input format. Therefore,
        this script converts `str`, `UTCDateTime` and `datetime.datetime` objects all to matplotb times
        before the plot command (although matplotlib can plot datetime.datetime objects natively), so
        these converted times later can be used to adjust xlim and ylim (if specified).
      - natively, matplotlib plots `datetime.datetime` objects with labels
        hour, minute, and second. This is true for both ax.plot and ax.plot_date ..
      - as I wished a personal format including month, day, hour, minute and second, and
        as times are converted to matplotlib times regardless (which matplotlib then does not
        plot with a nice string but as simples numbers), the following two commands are used:
        
        myFmt = mdates.DateFormatter('%d/%m %H:%M:%S')
        ax.xaxis.set_major_formatter(myFmt)
    """


    ### FIXED VARIABLES
    fs_title  = 12
    fs_label  = 11
    fs_legend = 9



    ### Figure instance & settings
    if not axis:
        fig = plt.figure(num=title, figsize=figsize)
        fig.canvas.set_window_title(win_title)
        ax = fig.add_subplot(111)
    else:
        ax = axis

    ax.ticklabel_format(axis='y', style='sci', scilimits=(-3, 3))
    ax.set_title(title, fontsize=fs_title, **title_kwargs)
    ax.set_xlabel(xlabel, fontsize=fs_label)
    ax.set_ylabel(ylabel, fontsize=fs_label)
    if grid:
        ax.grid(ls='-.', lw=0.5)
    if xscale:
        ax.set_xscale(xscale)
    if yscale:
        ax.set_yscale(yscale)

    if xinvert:
        ax.invert_xaxis()
    if yinvert:
        ax.invert_yaxis()



    ### Labels
    if data_labels is not None:
        if len(data_labels)!=len(y):    # not same amount of entries (also true if none given)
            
            if all(isinstance(ele, Trace) for ele in y):
                labels = [trace.id if trace.data.any() else None for trace in y]    # empty data don't get a label

            else:
                labels = ['Data %s' % (i+1) for i in range(len(y))]

        else:
            labels = data_labels

    else:
        legend_loc = None
        labels     = [None for i in range(len(y))]



    ### Data plotting (empty data are not plotted)
    if isinstance(ls, str):
        ls = [ls]

    if isinstance(lw, (int, float)):
        lw = [lw]

    if isinstance(lc, str) or lc is None:
        lc = [lc]

    if x is None:
        x = [None for i in y]
    elif len(x)==len(y) and all(isinstance(i, (tuple, list, np.ndarray)) for i in x):
        pass
    else:
        x = [x for _ in range(len(y))]


    for j, data in enumerate(y):

        if len(ls)!=len(y):
            linestyle = ls[0]
        else:
            linestyle = ls[j]

        if len(lw)!=len(y):
            linewidth = lw[0]
        else:
            linewidth = lw[j]

        if len(lc)!=len(y):
            linecolor = lc[0]
        else:
            linecolor = lc[j]

        if isinstance(data, Trace):
            if x[j] is not None:
                xdata = x[j]
                if all(isinstance(ele, (UTCDateTime, datetime.datetime, str)) for ele in xdata):
                    xdata = [UTCDateTime(e).datetime for e in xdata]    # convert all to datetime.datetime objects
                    xdata = mdates.date2num(xdata)                      # convert all to matplotlib times (wouldn't need to, but for later it is need as ax.get_xlim() retrieves matplotlib times!)
                    ax.plot_date(xdata, data.data, ls=linestyle, lw=linewidth, c=linecolor, label=labels[j])
                    myFmt = mdates.DateFormatter(date_format)
                    ax.xaxis.set_major_formatter(myFmt)                # because we want dates in customised way, not matplotlib times
                else:   # normal array, relative times, matlab times, or POSIX timestamps
                    ax.plot(xdata, data, ls=linestyle, lw=linewidth, c=linecolor, label=labels[j])        
            else:
                xdata = data.times(type="matplotlib")
                ax.plot_date(xdata, data.data, ls=linestyle, marker=None, lw=linewidth, c=linecolor, label=labels[j])
                myFmt = mdates.DateFormatter(date_format)
                ax.xaxis.set_major_formatter(myFmt)

        elif isinstance(data, (Stream)):
            print(u'WARNING: Using plot() method of %s object and then return.' % type(data))
            plt.close()
            data.plot()     # use Stream, respectively, object's plot function
            return

        else:
            if x[j] is not None:
                xdata = x[j]
                if all(isinstance(ele, (UTCDateTime, datetime.datetime, str)) for ele in xdata):
                    xdata = [UTCDateTime(e).datetime for e in xdata]    # convert all to datetime.datetime objects
                    myFmt = mdates.DateFormatter(date_format)
                    ax.plot(xdata, data, ls=linestyle, lw=linewidth, c=linecolor, label=labels[j])
                    ax.xaxis.set_major_formatter(myFmt)                # because we want dates in customised way, not matplotlib times
                else:   # normal array, relative times, matlab times, or POSIX timestamps
                    ax.plot(xdata, data, ls=linestyle, lw=linewidth, c=linecolor, label=labels[j])
            else:
                xdata = np.arange(len(data))
                ax.plot(xdata, data, ls=linestyle, lw=linewidth, c=linecolor, label=labels[j])



    ### Limits of x- and y-axis
    if xlim is not None and not all(x is None for x in xlim):
        if all(isinstance(ele, (UTCDateTime, datetime.datetime, str)) for ele in xlim):
            xlim = [UTCDateTime(e).datetime for e in xlim]
            xlim = [mdates.date2num(e) for e in xlim]
        ax.set_xlim( xlim )
    

    if ylim is not None and not all(y is None for y in ylim):
        ax.set_ylim( ylim )
    else:           # make sure ylim is according to newly set xlim
        y_mins = []
        y_maxs = []
        for line in ax.lines:
            x = line.get_xdata()
            y = line.get_ydata()
            try:                                                                    # e.g. if one of the `y` is empty
                i = np.where( (x >= ax.get_xlim()[0]) & (x <= ax.get_xlim()[1]) )[0]    # all indexes of y_data according to xlim
                line_min = np.nanmin(y[i])                                          # get minimum y within all data according to xlim
                line_max = np.nanmax(y[i])                                          # get maximum y within all data according to xlim
                y_mins.append(line_min)
                y_maxs.append(line_max)
            except:
                continue

        try:
            y_min = np.nanmin(y_mins)
            y_max = np.nanmax(y_maxs)
            ylim = [y_min-0.025*np.abs(y_max-y_min), y_max+0.025*np.abs(y_max-y_min)] # give 2.5% margin that works both for pos. & neg. values 
            ax.set_ylim( ylim )
        except:
            pass
    xlim = ax.get_xlim()
    ylim = ax.get_ylim()

    

    ### Vertical lines for data indications
    if isinstance(vertsc, str):
        colours = [vertsc]
    elif vertsc is None:
        colours = ['k']
    else:
        colours = vertsc
    
    if verts is not None: 

        for k, vert in enumerate(verts):
            colour_index = k % len(colours)
                    
            if isinstance(vert, (np.ndarray, tuple, list)): 
                pass

            else:
                vert = [vert]


            for vline in vert:

                if isinstance(vline, (UTCDateTime, datetime.datetime, str)):
                    vline = UTCDateTime( vline )
                    vline = mdates.date2num(vline.datetime)
                    
                elif isinstance(vline, (complex)):
                    vline = vline.real
                    vline = xlim[0] + (xlim[1]-xlim[0])*vline   # relative to beginning in %

                else:
                    pass

                if vline>=xlim[0] and vline<=xlim[1]:
                    ax.plot( [vline, vline], ylim, lw=1, color=colours[colour_index])



    ### Horinzontal lines for data indications
    if isinstance(horisc, str):
        colours = [horisc]
    elif horisc is None:
        colours = ['k']
    else:
        colours = horisc

    if horis is not None:
                                                          # make list of lists so fo-loops work
        for k, hori in enumerate(horis):
            colour_index = k % len(colours)

            if isinstance(hori, (np.ndarray, tuple, list)):
                pass

            else:
                hori = [hori]


            for hline in hori:
                ax.plot( xlim, [hline, hline], lw=1, color=colours[colour_index])



    ### Saving & showing ..
    if legend_loc is not None:
        handles, labels = ax.get_legend_handles_labels()
        by_label = dict(zip(labels, handles))
        ax.legend(by_label.values(), by_label.keys(), loc=legend_loc, prop={'size': fs_legend})

    if axis:
        return ax

    plt.tight_layout()

    if outfile:
        plt.savefig(outfile)

    if show:
        plt.show()

    if not keep_open:
        plt.close()

        
#####  _ _ N A M E _ _ = = " _ _ M A I N _ _ "  #####
if __name__ == "__main__":
    pass