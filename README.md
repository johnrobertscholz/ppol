# Ppol (P-wave polarization)


**Ppol** is a Python package based on [ObsPy](https://github.com/obspy/obspy/wiki).
Its purpose is to determine horizontal sensor (mis-)orientations of ocean-bottom seismometers (OBS). 
It also works for terrestrial seismometers.

Installation guide and full documentation can be found at:  
https://ppol.readthedocs.io/en/latest/