<?xml version='1.0' encoding='utf-8'?>
<q:quakeml xmlns:ns0="http://service.iris.edu/fdsnws/event/1/" xmlns:q="http://quakeml.org/xmlns/quakeml/1.2" xmlns="http://quakeml.org/xmlns/bed/1.2">
  <eventParameters publicID="smi:local/79b4297e-74a8-44cf-a363-f27800d41be1">
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4368905">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=13703022</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181659389</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="409">
        <text>SOUTH ATLANTIC OCEAN</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=13703022" ns0:contributorOriginId="07489746" ns0:contributor="ISC" ns0:contributorEventId="603810699" ns0:catalog="ISC">
        <time>
          <value>2013-11-25T07:21:18.210000Z</value>
        </time>
        <latitude>
          <value>-53.8912</value>
        </latitude>
        <longitude>
          <value>-53.9511</value>
        </longitude>
        <depth>
          <value>10000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181659389">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4368902">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=13702828</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181659302</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="409">
        <text>SOUTH ATLANTIC OCEAN</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=13702828" ns0:contributorOriginId="07489738" ns0:contributor="ISC" ns0:contributorEventId="606907619" ns0:catalog="ISC">
        <time>
          <value>2013-11-25T06:27:34.680000Z</value>
        </time>
        <latitude>
          <value>-53.9716</value>
        </latitude>
        <longitude>
          <value>-54.9648</value>
        </longitude>
        <depth>
          <value>10000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181659302">
        <mag>
          <value>6.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4368901">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=13702775</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181659245</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="221">
        <text>KURIL ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=13702775" ns0:contributorOriginId="07489735" ns0:contributor="ISC" ns0:contributorEventId="603810696" ns0:catalog="ISC">
        <time>
          <value>2013-11-25T05:56:50.770000Z</value>
        </time>
        <latitude>
          <value>45.4838</value>
        </latitude>
        <longitude>
          <value>150.9624</value>
        </longitude>
        <depth>
          <value>43100.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181659245">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4368866">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=13691880</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181655307</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="181">
        <text>FIJI ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=13691880" ns0:contributorOriginId="07489516" ns0:contributor="ISC" ns0:contributorEventId="603824898" ns0:catalog="ISC">
        <time>
          <value>2013-11-23T07:48:32.020000Z</value>
        </time>
        <latitude>
          <value>-17.1613</value>
        </latitude>
        <longitude>
          <value>-176.5188</value>
        </longitude>
        <depth>
          <value>370100.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181655307">
        <mag>
          <value>6.5</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9413854">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=13673580</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181648530</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="691">
        <text>PACIFIC-ANTARCTIC RIDGE</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=13673580" ns0:contributorOriginId="07489167" ns0:contributor="ISC" ns0:contributorEventId="603798180" ns0:catalog="ISC">
        <time>
          <value>2013-11-19T23:18:00.520000Z</value>
        </time>
        <latitude>
          <value>-63.265</value>
        </latitude>
        <longitude>
          <value>-164.8659</value>
        </longitude>
        <depth>
          <value>10000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181648530">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4368801">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=13672075</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181647930</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="216">
        <text>MARIANA ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=13672075" ns0:contributorOriginId="07489130" ns0:contributor="ISC" ns0:contributorEventId="603797981" ns0:catalog="ISC">
        <time>
          <value>2013-11-19T17:00:45.010000Z</value>
        </time>
        <latitude>
          <value>18.4629</value>
        </latitude>
        <longitude>
          <value>145.165</value>
        </longitude>
        <depth>
          <value>531100.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181647930">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9413458">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=13671624</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181647755</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="209">
        <text>W. CAROLINE ISLANDS, MICRONESIA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=13671624" ns0:contributorOriginId="07489121" ns0:contributor="ISC" ns0:contributorEventId="603797977" ns0:catalog="ISC">
        <time>
          <value>2013-11-19T15:16:50.140000Z</value>
        </time>
        <latitude>
          <value>8.8537</value>
        </latitude>
        <longitude>
          <value>138.5662</value>
        </longitude>
        <depth>
          <value>23600.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181647755">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4368797">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=13671225</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181647579</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="267">
        <text>HALMAHERA, INDONESIA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=13671225" ns0:contributorOriginId="07489110" ns0:contributor="ISC" ns0:contributorEventId="603797971" ns0:catalog="ISC">
        <time>
          <value>2013-11-19T13:32:53.410000Z</value>
        </time>
        <latitude>
          <value>2.7153</value>
        </latitude>
        <longitude>
          <value>128.4474</value>
        </longitude>
        <depth>
          <value>56100.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181647579">
        <mag>
          <value>6.1</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9410972">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=13659178</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181642207</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="150">
        <text>SCOTIA SEA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=13659178" ns0:contributorOriginId="07488724" ns0:contributor="ISC" ns0:contributorEventId="603816199" ns0:catalog="ISC">
        <time>
          <value>2013-11-17T09:11:00.920000Z</value>
        </time>
        <latitude>
          <value>-60.4308</value>
        </latitude>
        <longitude>
          <value>-45.1787</value>
        </longitude>
        <depth>
          <value>10000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181642207">
        <mag>
          <value>5.8</value>
        </mag>
        <type>mb</type>
        <creationInfo>
          <author>NEIC</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4368729">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=13659173</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181642182</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="150">
        <text>SCOTIA SEA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=13659173" ns0:contributorOriginId="07488723" ns0:contributor="ISC" ns0:contributorEventId="603794680" ns0:catalog="ISC">
        <time>
          <value>2013-11-17T09:04:56.260000Z</value>
        </time>
        <latitude>
          <value>-60.3783</value>
        </latitude>
        <longitude>
          <value>-46.5876</value>
        </longitude>
        <depth>
          <value>7900.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181642182">
        <mag>
          <value>7.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4368698">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=13652180</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181639594</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="150">
        <text>SCOTIA SEA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=13652180" ns0:contributorOriginId="07488567" ns0:contributor="ISC" ns0:contributorEventId="603824875" ns0:catalog="ISC">
        <time>
          <value>2013-11-16T03:34:34.790000Z</value>
        </time>
        <latitude>
          <value>-60.3079</value>
        </latitude>
        <longitude>
          <value>-47.199</value>
        </longitude>
        <depth>
          <value>24000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181639594">
        <mag>
          <value>6.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4368654">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=13640428</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181635306</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="150">
        <text>SCOTIA SEA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=13640428" ns0:contributorOriginId="07488344" ns0:contributor="ISC" ns0:contributorEventId="603788612" ns0:catalog="ISC">
        <time>
          <value>2013-11-13T23:45:48.200000Z</value>
        </time>
        <latitude>
          <value>-60.3091</value>
        </latitude>
        <longitude>
          <value>-47.2872</value>
        </longitude>
        <depth>
          <value>10000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181635306">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9406193">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=13635279</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181633573</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="7">
        <text>ANDREANOF ISLANDS, ALEUTIAN IS.</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=13635279" ns0:contributorOriginId="07488249" ns0:contributor="ISC" ns0:contributorEventId="603786068" ns0:catalog="ISC">
        <time>
          <value>2013-11-13T03:57:42.140000Z</value>
        </time>
        <latitude>
          <value>51.487</value>
        </latitude>
        <longitude>
          <value>-178.8839</value>
        </longitude>
        <depth>
          <value>32000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181633573">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4368624">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=13629974</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181631521</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="218">
        <text>NEAR EAST COAST OF KAMCHATKA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=13629974" ns0:contributorOriginId="07488122" ns0:contributor="ISC" ns0:contributorEventId="603782753" ns0:catalog="ISC">
        <time>
          <value>2013-11-12T07:03:52.470000Z</value>
        </time>
        <latitude>
          <value>54.6769</value>
        </latitude>
        <longitude>
          <value>162.1432</value>
        </longitude>
        <depth>
          <value>60500.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181631521">
        <mag>
          <value>6.5</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9395055">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=13579581</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181614331</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="262">
        <text>CELEBES SEA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=13579581" ns0:contributorOriginId="07487250" ns0:contributor="ISC" ns0:contributorEventId="603768440" ns0:catalog="ISC">
        <time>
          <value>2013-11-03T11:03:40.460000Z</value>
        </time>
        <latitude>
          <value>4.6415</value>
        </latitude>
        <longitude>
          <value>123.4</value>
        </longitude>
        <depth>
          <value>556800.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181614331">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9394647">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=13577573</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181613453</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="192">
        <text>NEW BRITAIN REGION, P.N.G.</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=13577573" ns0:contributorOriginId="07487202" ns0:contributor="ISC" ns0:contributorEventId="603768420" ns0:catalog="ISC">
        <time>
          <value>2013-11-03T02:42:54.150000Z</value>
        </time>
        <latitude>
          <value>-5.9243</value>
        </latitude>
        <longitude>
          <value>148.8457</value>
        </longitude>
        <depth>
          <value>91900.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181613453">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4368495">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=13575528</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181612621</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="174">
        <text>TONGA ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=13575528" ns0:contributorOriginId="07487158" ns0:contributor="ISC" ns0:contributorEventId="603768165" ns0:catalog="ISC">
        <time>
          <value>2013-11-02T18:53:47.930000Z</value>
        </time>
        <latitude>
          <value>-19.3532</value>
        </latitude>
        <longitude>
          <value>-172.6354</value>
        </longitude>
        <depth>
          <value>19300.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181612621">
        <mag>
          <value>6.2</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4368490">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=13574579</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181612305</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="685">
        <text>EASTER ISLAND REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=13574579" ns0:contributorOriginId="07487141" ns0:contributor="ISC" ns0:contributorEventId="603824830" ns0:catalog="ISC">
        <time>
          <value>2013-11-02T15:52:45.490000Z</value>
        </time>
        <latitude>
          <value>-23.6356</value>
        </latitude>
        <longitude>
          <value>-112.6783</value>
        </longitude>
        <depth>
          <value>8500.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181612305">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4368469">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12552222</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181608568</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="135">
        <text>NEAR COAST OF CENTRAL CHILE</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12552222" ns0:contributorOriginId="07314396" ns0:contributor="ISC" ns0:contributorEventId="609095356" ns0:catalog="ISC">
        <time>
          <value>2013-10-31T23:03:58.620000Z</value>
        </time>
        <latitude>
          <value>-30.3139</value>
        </latitude>
        <longitude>
          <value>-71.5019</value>
        </longitude>
        <depth>
          <value>19800.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181608568">
        <mag>
          <value>6.5</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4368465">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12547619</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181607173</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="244">
        <text>TAIWAN</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12547619" ns0:contributorOriginId="07314316" ns0:contributor="ISC" ns0:contributorEventId="609078666" ns0:catalog="ISC">
        <time>
          <value>2013-10-31T12:02:09.350000Z</value>
        </time>
        <latitude>
          <value>23.5436</value>
        </latitude>
        <longitude>
          <value>121.458</value>
        </longitude>
        <depth>
          <value>14800.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181607173">
        <mag>
          <value>6.3</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4368448">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12539319</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181604135</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="134">
        <text>OFF COAST OF CENTRAL CHILE</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12539319" ns0:contributorOriginId="07314173" ns0:contributor="ISC" ns0:contributorEventId="603764109" ns0:catalog="ISC">
        <time>
          <value>2013-10-30T02:51:49.440000Z</value>
        </time>
        <latitude>
          <value>-35.4018</value>
        </latitude>
        <longitude>
          <value>-73.1515</value>
        </longitude>
        <depth>
          <value>11900.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181604135">
        <mag>
          <value>6.3</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9297850">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12539219</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181604090</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="134">
        <text>OFF COAST OF CENTRAL CHILE</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12539219" ns0:contributorOriginId="07314170" ns0:contributor="ISC" ns0:contributorEventId="609078660" ns0:catalog="ISC">
        <time>
          <value>2013-10-30T02:29:11.490000Z</value>
        </time>
        <latitude>
          <value>-35.4314</value>
        </latitude>
        <longitude>
          <value>-73.15</value>
        </longitude>
        <depth>
          <value>12800.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181604090">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9297066">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12535275</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181602781</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="702">
        <text>BALLENY ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12535275" ns0:contributorOriginId="07314119" ns0:contributor="ISC" ns0:contributorEventId="609078646" ns0:catalog="ISC">
        <time>
          <value>2013-10-29T10:37:56.300000Z</value>
        </time>
        <latitude>
          <value>-61.7154</value>
        </latitude>
        <longitude>
          <value>154.8294</value>
        </longitude>
        <depth>
          <value>10000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181602781">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9293303">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12516374</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181596025</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="153">
        <text>SOUTH SANDWICH ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12516374" ns0:contributorOriginId="07313748" ns0:contributor="ISC" ns0:contributorEventId="603770059" ns0:catalog="ISC">
        <time>
          <value>2013-10-25T21:51:57.960000Z</value>
        </time>
        <latitude>
          <value>-56.3237</value>
        </latitude>
        <longitude>
          <value>-27.4137</value>
        </longitude>
        <depth>
          <value>112000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181596025">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4368379">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12515370</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181595525</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="229">
        <text>OFF EAST COAST OF HONSHU, JAPAN</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12515370" ns0:contributorOriginId="07313706" ns0:contributor="ISC" ns0:contributorEventId="609078601" ns0:catalog="ISC">
        <time>
          <value>2013-10-25T17:10:17.090000Z</value>
        </time>
        <latitude>
          <value>37.1457</value>
        </latitude>
        <longitude>
          <value>144.754</value>
        </longitude>
        <depth>
          <value>14700.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181595525">
        <mag>
          <value>7.1</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9292212">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12510923</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181593926</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="171">
        <text>SOUTH OF FIJI ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12510923" ns0:contributorOriginId="07313524" ns0:contributor="ISC" ns0:contributorEventId="603759778" ns0:catalog="ISC">
        <time>
          <value>2013-10-24T20:32:44.730000Z</value>
        </time>
        <latitude>
          <value>-22.6514</value>
        </latitude>
        <longitude>
          <value>-176.5636</value>
        </longitude>
        <depth>
          <value>123500.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181593926">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4368365">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12510719</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181593823</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="732">
        <text>EAST OF SOUTH SANDWICH ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12510719" ns0:contributorOriginId="07313518" ns0:contributor="ISC" ns0:contributorEventId="603770052" ns0:catalog="ISC">
        <time>
          <value>2013-10-24T19:25:10.300000Z</value>
        </time>
        <latitude>
          <value>-58.2594</value>
        </latitude>
        <longitude>
          <value>-12.8022</value>
        </longitude>
        <depth>
          <value>10000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181593823">
        <mag>
          <value>6.6</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4362026">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12503070</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181591110</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="171">
        <text>SOUTH OF FIJI ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12503070" ns0:contributorOriginId="07313368" ns0:contributor="ISC" ns0:contributorEventId="603770048" ns0:catalog="ISC">
        <time>
          <value>2013-10-23T08:23:29.510000Z</value>
        </time>
        <latitude>
          <value>-22.9722</value>
        </latitude>
        <longitude>
          <value>-177.0404</value>
        </longitude>
        <depth>
          <value>157700.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181591110">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4311182">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12484621</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181584124</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="49">
        <text>GULF OF CALIFORNIA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12484621" ns0:contributorOriginId="07313013" ns0:contributor="ISC" ns0:contributorEventId="603750265" ns0:catalog="ISC">
        <time>
          <value>2013-10-19T17:54:57.220000Z</value>
        </time>
        <latitude>
          <value>25.9612</value>
        </latitude>
        <longitude>
          <value>-110.3639</value>
        </longitude>
        <depth>
          <value>15000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181584124">
        <mag>
          <value>6.6</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4310759">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12466327</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181576804</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="193">
        <text>SOLOMON ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12466327" ns0:contributorOriginId="07312642" ns0:contributor="ISC" ns0:contributorEventId="609095279" ns0:catalog="ISC">
        <time>
          <value>2013-10-16T10:31:00.990000Z</value>
        </time>
        <latitude>
          <value>-6.4787</value>
        </latitude>
        <longitude>
          <value>154.9648</value>
        </longitude>
        <depth>
          <value>58000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181576804">
        <mag>
          <value>6.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4310709">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12458127</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181572471</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="259">
        <text>MINDANAO, PHILIPPINES</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12458127" ns0:contributorOriginId="07312399" ns0:contributor="ISC" ns0:contributorEventId="609078498" ns0:catalog="ISC">
        <time>
          <value>2013-10-15T00:12:33.250000Z</value>
        </time>
        <latitude>
          <value>9.8331</value>
        </latitude>
        <longitude>
          <value>124.1231</value>
        </longitude>
        <depth>
          <value>23200.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181572471">
        <mag>
          <value>7.1</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4310676">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12445572</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181567890</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="370">
        <text>CRETE, GREECE</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12445572" ns0:contributorOriginId="07312180" ns0:contributor="ISC" ns0:contributorEventId="609096383" ns0:catalog="ISC">
        <time>
          <value>2013-10-12T13:11:53.650000Z</value>
        </time>
        <latitude>
          <value>35.5277</value>
        </latitude>
        <longitude>
          <value>23.3718</value>
        </longitude>
        <depth>
          <value>46900.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181567890">
        <mag>
          <value>6.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4310668">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12443274</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181567086</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="97">
        <text>NEAR COAST OF VENEZUELA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12443274" ns0:contributorOriginId="07312143" ns0:contributor="ISC" ns0:contributorEventId="609095262" ns0:catalog="ISC">
        <time>
          <value>2013-10-12T02:10:28.910000Z</value>
        </time>
        <latitude>
          <value>10.8358</value>
        </latitude>
        <longitude>
          <value>-62.3025</value>
        </longitude>
        <depth>
          <value>82100.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181567086">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4310665">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12442319</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181566731</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="178">
        <text>KERMADEC ISLANDS, NEW ZEALAND</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12442319" ns0:contributorOriginId="07312122" ns0:contributor="ISC" ns0:contributorEventId="603734159" ns0:catalog="ISC">
        <time>
          <value>2013-10-11T21:24:59.360000Z</value>
        </time>
        <latitude>
          <value>-30.729</value>
        </latitude>
        <longitude>
          <value>-178.4264</value>
        </longitude>
        <depth>
          <value>152900.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181566731">
        <mag>
          <value>6.2</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9277929">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12439422</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181565647</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="662">
        <text>SAKHALIN, RUSSIA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12439422" ns0:contributorOriginId="05598017" ns0:contributor="ISC" ns0:contributorEventId="606825406" ns0:catalog="ISC">
        <time>
          <value>2013-10-11T09:28:47.300000Z</value>
        </time>
        <latitude>
          <value>48.55</value>
        </latitude>
        <longitude>
          <value>141.5</value>
        </longitude>
        <depth>
          <value>10000.0</value>
        </depth>
        <creationInfo>
          <author>SKHL</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181565647">
        <mag>
          <value>6.6</value>
        </mag>
        <type>mb</type>
        <creationInfo>
          <author>SKHL</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4310243">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12414627</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181556980</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="686">
        <text>WEST CHILE RISE</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12414627" ns0:contributorOriginId="07311650" ns0:contributor="ISC" ns0:contributorEventId="603517938" ns0:catalog="ISC">
        <time>
          <value>2013-10-06T21:33:19.710000Z</value>
        </time>
        <latitude>
          <value>-36.6341</value>
        </latitude>
        <longitude>
          <value>-97.6029</value>
        </longitude>
        <depth>
          <value>12000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181556980">
        <mag>
          <value>6.2</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4310236">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12413421</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181556541</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="210">
        <text>SOUTH OF MARIANA ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12413421" ns0:contributorOriginId="07311627" ns0:contributor="ISC" ns0:contributorEventId="603988736" ns0:catalog="ISC">
        <time>
          <value>2013-10-06T16:38:10.390000Z</value>
        </time>
        <latitude>
          <value>12.2738</value>
        </latitude>
        <longitude>
          <value>141.7102</value>
        </longitude>
        <depth>
          <value>118400.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181556541">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4310209">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12402924</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181552743</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="429">
        <text>MID-INDIAN RIDGE</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12402924" ns0:contributorOriginId="07311325" ns0:contributor="ISC" ns0:contributorEventId="603516211" ns0:catalog="ISC">
        <time>
          <value>2013-10-04T17:26:14.250000Z</value>
        </time>
        <latitude>
          <value>-38.707</value>
        </latitude>
        <longitude>
          <value>78.5007</value>
        </longitude>
        <depth>
          <value>11000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181552743">
        <mag>
          <value>6.4</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9267756">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12388569</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181547469</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="740">
        <text>OWEN FRACTURE ZONE REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12388569" ns0:contributorOriginId="07311079" ns0:contributor="ISC" ns0:contributorEventId="603511253" ns0:catalog="ISC">
        <time>
          <value>2013-10-02T01:06:38.640000Z</value>
        </time>
        <latitude>
          <value>11.2463</value>
        </latitude>
        <longitude>
          <value>57.5989</value>
        </longitude>
        <depth>
          <value>16300.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181547469">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4310156">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12383820</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181545908</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="663">
        <text>SEA OF OKHOTSK</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12383820" ns0:contributorOriginId="07311010" ns0:contributor="ISC" ns0:contributorEventId="603503926" ns0:catalog="ISC">
        <time>
          <value>2013-10-01T03:38:21.390000Z</value>
        </time>
        <latitude>
          <value>53.1368</value>
        </latitude>
        <longitude>
          <value>152.8959</value>
        </longitude>
        <depth>
          <value>578400.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181545908">
        <mag>
          <value>6.7</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4310146">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12184219</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181543980</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="178">
        <text>KERMADEC ISLANDS, NEW ZEALAND</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12184219" ns0:contributorOriginId="07268495" ns0:contributor="ISC" ns0:contributorEventId="603503050" ns0:catalog="ISC">
        <time>
          <value>2013-09-30T05:55:54.870000Z</value>
        </time>
        <latitude>
          <value>-30.8752</value>
        </latitude>
        <longitude>
          <value>-178.2538</value>
        </longitude>
        <depth>
          <value>37800.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181543980">
        <mag>
          <value>6.4</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4310115">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12173272</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181540174</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="710">
        <text>PAKISTAN</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12173272" ns0:contributorOriginId="07268321" ns0:contributor="ISC" ns0:contributorEventId="609059670" ns0:catalog="ISC">
        <time>
          <value>2013-09-28T07:34:07.460000Z</value>
        </time>
        <latitude>
          <value>27.1617</value>
        </latitude>
        <longitude>
          <value>65.6192</value>
        </longitude>
        <depth>
          <value>18700.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181540174">
        <mag>
          <value>6.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4310063">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12159724</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181535324</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="115">
        <text>NEAR COAST OF PERU</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12159724" ns0:contributorOriginId="07268072" ns0:contributor="ISC" ns0:contributorEventId="608607960" ns0:catalog="ISC">
        <time>
          <value>2013-09-25T16:42:43.180000Z</value>
        </time>
        <latitude>
          <value>-15.9001</value>
        </latitude>
        <longitude>
          <value>-74.6012</value>
        </longitude>
        <depth>
          <value>38200.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181535324">
        <mag>
          <value>7.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4310020">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12157120</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181534350</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="684">
        <text>SOUTHERN EAST PACIFIC RISE</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12157120" ns0:contributorOriginId="07268018" ns0:contributor="ISC" ns0:contributorEventId="608607944" ns0:catalog="ISC">
        <time>
          <value>2013-09-25T06:51:23.970000Z</value>
        </time>
        <latitude>
          <value>-49.9853</value>
        </latitude>
        <longitude>
          <value>-113.7895</value>
        </longitude>
        <depth>
          <value>10000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181534350">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9222460">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12153977</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181533199</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="710">
        <text>PAKISTAN</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12153977" ns0:contributorOriginId="07267962" ns0:contributor="ISC" ns0:contributorEventId="603472346" ns0:catalog="ISC">
        <time>
          <value>2013-09-24T17:20:14.480000Z</value>
        </time>
        <latitude>
          <value>27.1131</value>
        </latitude>
        <longitude>
          <value>65.5461</value>
        </longitude>
        <depth>
          <value>16100.000000000002</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181533199">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9222173">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12152570</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181532661</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="710">
        <text>PAKISTAN</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12152570" ns0:contributorOriginId="07267928" ns0:contributor="ISC" ns0:contributorEventId="609059621" ns0:catalog="ISC">
        <time>
          <value>2013-09-24T11:36:27.720000Z</value>
        </time>
        <latitude>
          <value>27.1674</value>
        </latitude>
        <longitude>
          <value>65.5724</value>
        </longitude>
        <depth>
          <value>10000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181532661">
        <mag>
          <value>5.8</value>
        </mag>
        <type>mb</type>
        <creationInfo>
          <author>NEIC</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4309987">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12152526</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181532635</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="710">
        <text>PAKISTAN</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12152526" ns0:contributorOriginId="07267927" ns0:contributor="ISC" ns0:contributorEventId="608458290" ns0:catalog="ISC">
        <time>
          <value>2013-09-24T11:29:48.230000Z</value>
        </time>
        <latitude>
          <value>26.9109</value>
        </latitude>
        <longitude>
          <value>65.5315</value>
        </longitude>
        <depth>
          <value>15500.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181532635">
        <mag>
          <value>7.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4309836">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12136021</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181526677</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="279">
        <text>FLORES SEA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12136021" ns0:contributorOriginId="07267610" ns0:contributor="ISC" ns0:contributorEventId="609059456" ns0:catalog="ISC">
        <time>
          <value>2013-09-21T01:39:15.480000Z</value>
        </time>
        <latitude>
          <value>-7.2679</value>
        </latitude>
        <longitude>
          <value>119.9508</value>
        </longitude>
        <depth>
          <value>550100.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181526677">
        <mag>
          <value>6.1</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4309350">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12105125</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181515823</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="7">
        <text>ANDREANOF ISLANDS, ALEUTIAN IS.</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12105125" ns0:contributorOriginId="07267066" ns0:contributor="ISC" ns0:contributorEventId="609059383" ns0:catalog="ISC">
        <time>
          <value>2013-09-15T16:21:38.490000Z</value>
        </time>
        <latitude>
          <value>51.4857</value>
        </latitude>
        <longitude>
          <value>-174.6891</value>
        </longitude>
        <depth>
          <value>31100.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181515823">
        <mag>
          <value>6.1</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9211666">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12099974</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181513938</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="7">
        <text>ANDREANOF ISLANDS, ALEUTIAN IS.</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12099974" ns0:contributorOriginId="07266966" ns0:contributor="ISC" ns0:contributorEventId="603429475" ns0:catalog="ISC">
        <time>
          <value>2013-09-14T15:42:46.670000Z</value>
        </time>
        <latitude>
          <value>51.4073</value>
        </latitude>
        <longitude>
          <value>-174.6918</value>
        </longitude>
        <depth>
          <value>29400.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181513938">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4276127">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12080622</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181507749</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="694">
        <text>CENTRAL EAST PACIFIC RISE</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12080622" ns0:contributorOriginId="07266680" ns0:contributor="ISC" ns0:contributorEventId="609059318" ns0:catalog="ISC">
        <time>
          <value>2013-09-11T12:44:14.520000Z</value>
        </time>
        <latitude>
          <value>-4.591</value>
        </latitude>
        <longitude>
          <value>-104.8068</value>
        </longitude>
        <depth>
          <value>13000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181507749">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4225744">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12054025</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181498342</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="69">
        <text>NEAR COAST OF CHIAPAS, MEXICO</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12054025" ns0:contributorOriginId="07266204" ns0:contributor="ISC" ns0:contributorEventId="609059260" ns0:catalog="ISC">
        <time>
          <value>2013-09-07T00:13:29.910000Z</value>
        </time>
        <latitude>
          <value>14.6678</value>
        </latitude>
        <longitude>
          <value>-92.0911</value>
        </longitude>
        <depth>
          <value>74100.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181498342">
        <mag>
          <value>6.4</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9201887">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12051026</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181497422</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="248">
        <text>PHILIPPINE ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12051026" ns0:contributorOriginId="07266161" ns0:contributor="ISC" ns0:contributorEventId="603384798" ns0:catalog="ISC">
        <time>
          <value>2013-09-06T11:33:54.790000Z</value>
        </time>
        <latitude>
          <value>20.1862</value>
        </latitude>
        <longitude>
          <value>122.3336</value>
        </longitude>
        <depth>
          <value>189400.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181497422">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9200748">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12045327</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181495356</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="77">
        <text>OFF COAST OF COSTA RICA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12045327" ns0:contributorOriginId="07266054" ns0:contributor="ISC" ns0:contributorEventId="603383493" ns0:catalog="ISC">
        <time>
          <value>2013-09-05T12:29:16.830000Z</value>
        </time>
        <latitude>
          <value>10.6454</value>
        </latitude>
        <longitude>
          <value>-86.1017</value>
        </longitude>
        <depth>
          <value>33000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181495356">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4225139">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12043576</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181494568</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="403">
        <text>NORTHERN MID-ATLANTIC RIDGE</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12043576" ns0:contributorOriginId="07266010" ns0:contributor="ISC" ns0:contributorEventId="609059217" ns0:catalog="ISC">
        <time>
          <value>2013-09-05T04:01:36.070000Z</value>
        </time>
        <latitude>
          <value>15.2518</value>
        </latitude>
        <longitude>
          <value>-45.2139</value>
        </longitude>
        <depth>
          <value>10400.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181494568">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4225100">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12038769</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181492651</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="7">
        <text>ANDREANOF ISLANDS, ALEUTIAN IS.</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12038769" ns0:contributorOriginId="07265909" ns0:contributor="ISC" ns0:contributorEventId="608607284" ns0:catalog="ISC">
        <time>
          <value>2013-09-04T06:27:05.350000Z</value>
        </time>
        <latitude>
          <value>51.5201</value>
        </latitude>
        <longitude>
          <value>-174.7733</value>
        </longitude>
        <depth>
          <value>30200.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181492651">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9199418">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12038677</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181492608</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="7">
        <text>ANDREANOF ISLANDS, ALEUTIAN IS.</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12038677" ns0:contributorOriginId="07265908" ns0:contributor="ISC" ns0:contributorEventId="609059189" ns0:catalog="ISC">
        <time>
          <value>2013-09-04T06:16:50.110000Z</value>
        </time>
        <latitude>
          <value>51.4884</value>
        </latitude>
        <longitude>
          <value>-174.8132</value>
        </longitude>
        <depth>
          <value>25600.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181492608">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9199333">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12038272</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181492407</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="7">
        <text>ANDREANOF ISLANDS, ALEUTIAN IS.</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12038272" ns0:contributorOriginId="07265892" ns0:contributor="ISC" ns0:contributorEventId="608607269" ns0:catalog="ISC">
        <time>
          <value>2013-09-04T04:16:30.730000Z</value>
        </time>
        <latitude>
          <value>51.5093</value>
        </latitude>
        <longitude>
          <value>-174.7004</value>
        </longitude>
        <depth>
          <value>33400.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181492407">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4225094">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12037823</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181492179</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="7">
        <text>ANDREANOF ISLANDS, ALEUTIAN IS.</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12037823" ns0:contributorOriginId="07265875" ns0:contributor="ISC" ns0:contributorEventId="609059160" ns0:catalog="ISC">
        <time>
          <value>2013-09-04T02:32:32.010000Z</value>
        </time>
        <latitude>
          <value>51.4813</value>
        </latitude>
        <longitude>
          <value>-174.6949</value>
        </longitude>
        <depth>
          <value>30300.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181492179">
        <mag>
          <value>6.5</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4225092">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12037421</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181491988</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="22">
        <text>QUEEN CHARLOTTE ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12037421" ns0:contributorOriginId="07265860" ns0:contributor="ISC" ns0:contributorEventId="608607254" ns0:catalog="ISC">
        <time>
          <value>2013-09-04T00:23:10.920000Z</value>
        </time>
        <latitude>
          <value>51.1711</value>
        </latitude>
        <longitude>
          <value>-130.2671</value>
        </longitude>
        <depth>
          <value>10000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181491988">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4225091">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12037378</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181491970</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="211">
        <text>SOUTHEAST OF HONSHU, JAPAN</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12037378" ns0:contributorOriginId="07265858" ns0:contributor="ISC" ns0:contributorEventId="608458267" ns0:catalog="ISC">
        <time>
          <value>2013-09-04T00:18:24.450000Z</value>
        </time>
        <latitude>
          <value>29.9768</value>
        </latitude>
        <longitude>
          <value>138.8376</value>
        </longitude>
        <depth>
          <value>414400.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181491970">
        <mag>
          <value>6.5</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4225081">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12036627</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181491580</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="22">
        <text>QUEEN CHARLOTTE ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12036627" ns0:contributorOriginId="07265835" ns0:contributor="ISC" ns0:contributorEventId="603378547" ns0:catalog="ISC">
        <time>
          <value>2013-09-03T20:19:06.670000Z</value>
        </time>
        <latitude>
          <value>51.0979</value>
        </latitude>
        <longitude>
          <value>-130.4234</value>
        </longitude>
        <depth>
          <value>2500.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181491580">
        <mag>
          <value>6.1</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9197249">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12027770</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181488159</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="193">
        <text>SOLOMON ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12027770" ns0:contributorOriginId="07265649" ns0:contributor="ISC" ns0:contributorEventId="609059119" ns0:catalog="ISC">
        <time>
          <value>2013-09-02T04:30:18.720000Z</value>
        </time>
        <latitude>
          <value>-6.5243</value>
        </latitude>
        <longitude>
          <value>155.0371</value>
        </longitude>
        <depth>
          <value>54300.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181488159">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4224999">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12024126</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181486821</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="280">
        <text>BANDA SEA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12024126" ns0:contributorOriginId="07265581" ns0:contributor="ISC" ns0:contributorEventId="603369359" ns0:catalog="ISC">
        <time>
          <value>2013-09-01T11:52:32.120000Z</value>
        </time>
        <latitude>
          <value>-7.5393</value>
        </latitude>
        <longitude>
          <value>128.2569</value>
        </longitude>
        <depth>
          <value>135200.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181486821">
        <mag>
          <value>6.5</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9195230">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12017620</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181484103</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="7">
        <text>ANDREANOF ISLANDS, ALEUTIAN IS.</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12017620" ns0:contributorOriginId="06936950" ns0:contributor="ISC" ns0:contributorEventId="608588145" ns0:catalog="ISC">
        <time>
          <value>2013-08-31T06:38:38.040000Z</value>
        </time>
        <latitude>
          <value>51.2897</value>
        </latitude>
        <longitude>
          <value>-174.9417</value>
        </longitude>
        <depth>
          <value>27600.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181484103">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4224881">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12013825</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181482489</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="7">
        <text>ANDREANOF ISLANDS, ALEUTIAN IS.</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12013825" ns0:contributorOriginId="06936853" ns0:contributor="ISC" ns0:contributorEventId="603366810" ns0:catalog="ISC">
        <time>
          <value>2013-08-30T16:25:02.120000Z</value>
        </time>
        <latitude>
          <value>51.5001</value>
        </latitude>
        <longitude>
          <value>-175.2137</value>
        </longitude>
        <depth>
          <value>30200.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181482489">
        <mag>
          <value>7.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9193780">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12010321</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181481369</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="192">
        <text>NEW BRITAIN REGION, P.N.G.</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12010321" ns0:contributorOriginId="06936802" ns0:contributor="ISC" ns0:contributorEventId="608161716" ns0:catalog="ISC">
        <time>
          <value>2013-08-30T02:11:02.890000Z</value>
        </time>
        <latitude>
          <value>-4.4495</value>
        </latitude>
        <longitude>
          <value>151.6228</value>
        </longitude>
        <depth>
          <value>211000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181481369">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9193194">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12007375</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181480380</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="181">
        <text>FIJI ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12007375" ns0:contributorOriginId="06936754" ns0:contributor="ISC" ns0:contributorEventId="608161700" ns0:catalog="ISC">
        <time>
          <value>2013-08-29T13:52:27.370000Z</value>
        </time>
        <latitude>
          <value>-19.3976</value>
        </latitude>
        <longitude>
          <value>-179.021</value>
        </longitude>
        <depth>
          <value>674300.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181480380">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4224692">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11998925</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181477588</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="177">
        <text>KERMADEC ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11998925" ns0:contributorOriginId="06936615" ns0:contributor="ISC" ns0:contributorEventId="608161661" ns0:catalog="ISC">
        <time>
          <value>2013-08-28T02:54:41.470000Z</value>
        </time>
        <latitude>
          <value>-27.7888</value>
        </latitude>
        <longitude>
          <value>179.7461</value>
        </longitude>
        <depth>
          <value>485100.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181477588">
        <mag>
          <value>6.2</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9188607">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11984419</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181472895</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="428">
        <text>SOUTHWEST INDIAN RIDGE</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11984419" ns0:contributorOriginId="06936391" ns0:contributor="ISC" ns0:contributorEventId="603345086" ns0:catalog="ISC">
        <time>
          <value>2013-08-25T16:07:16.970000Z</value>
        </time>
        <latitude>
          <value>-33.3654</value>
        </latitude>
        <longitude>
          <value>57.0592</value>
        </longitude>
        <depth>
          <value>14000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181472895">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9186070">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11971722</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181468546</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="123">
        <text>NORTHERN CHILE</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11971722" ns0:contributorOriginId="06936191" ns0:contributor="ISC" ns0:contributorEventId="608161566" ns0:catalog="ISC">
        <time>
          <value>2013-08-23T08:34:06.010000Z</value>
        </time>
        <latitude>
          <value>-22.2926</value>
        </latitude>
        <longitude>
          <value>-68.6418</value>
        </longitude>
        <depth>
          <value>103000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181468546">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4224321">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11962426</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181465323</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="58">
        <text>NEAR COAST OF GUERRERO, MEXICO</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11962426" ns0:contributorOriginId="06936027" ns0:contributor="ISC" ns0:contributorEventId="608588091" ns0:catalog="ISC">
        <time>
          <value>2013-08-21T12:38:29.510000Z</value>
        </time>
        <latitude>
          <value>16.8572</value>
        </latitude>
        <longitude>
          <value>-99.5188</value>
        </longitude>
        <depth>
          <value>18900.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181465323">
        <mag>
          <value>6.2</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4224113">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11941371</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181458131</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="428">
        <text>SOUTHWEST INDIAN RIDGE</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11941371" ns0:contributorOriginId="06935715" ns0:contributor="ISC" ns0:contributorEventId="603337743" ns0:catalog="ISC">
        <time>
          <value>2013-08-17T16:32:31.830000Z</value>
        </time>
        <latitude>
          <value>-34.8867</value>
        </latitude>
        <longitude>
          <value>54.1278</value>
        </longitude>
        <depth>
          <value>10900.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181458131">
        <mag>
          <value>6.2</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9178134">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11931928</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181454406</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="163">
        <text>COOK STRAIT, NEW ZEALAND</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11931928" ns0:contributorOriginId="06935549" ns0:contributor="ISC" ns0:contributorEventId="604020968" ns0:catalog="ISC">
        <time>
          <value>2013-08-16T05:31:17.300000Z</value>
        </time>
        <latitude>
          <value>-41.6615</value>
        </latitude>
        <longitude>
          <value>174.2294</value>
        </longitude>
        <depth>
          <value>17200.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181454406">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9177961">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11931075</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181453989</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="163">
        <text>COOK STRAIT, NEW ZEALAND</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11931075" ns0:contributorOriginId="06935525" ns0:contributor="ISC" ns0:contributorEventId="604020966" ns0:catalog="ISC">
        <time>
          <value>2013-08-16T03:09:08.910000Z</value>
        </time>
        <latitude>
          <value>-41.7651</value>
        </latitude>
        <longitude>
          <value>174.0479</value>
        </longitude>
        <depth>
          <value>6900.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181453989">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4223977">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11930869</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181453831</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="163">
        <text>COOK STRAIT, NEW ZEALAND</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11930869" ns0:contributorOriginId="06935512" ns0:contributor="ISC" ns0:contributorEventId="604020963" ns0:catalog="ISC">
        <time>
          <value>2013-08-16T02:31:09.200000Z</value>
        </time>
        <latitude>
          <value>-41.7685</value>
        </latitude>
        <longitude>
          <value>174.0796</value>
        </longitude>
        <depth>
          <value>13800.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181453831">
        <mag>
          <value>6.5</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4223856">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11915126</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181448918</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="83">
        <text>SOUTH OF PANAMA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11915126" ns0:contributorOriginId="06935299" ns0:contributor="ISC" ns0:contributorEventId="608161323" ns0:catalog="ISC">
        <time>
          <value>2013-08-13T15:43:15.910000Z</value>
        </time>
        <latitude>
          <value>5.718</value>
        </latitude>
        <longitude>
          <value>-78.1251</value>
        </longitude>
        <depth>
          <value>18600.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181448918">
        <mag>
          <value>6.6</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4223758">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11907522</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181446549</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="109">
        <text>NEAR COAST OF NORTHERN PERU</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11907522" ns0:contributorOriginId="06935187" ns0:contributor="ISC" ns0:contributorEventId="608161305" ns0:catalog="ISC">
        <time>
          <value>2013-08-12T09:49:31.920000Z</value>
        </time>
        <latitude>
          <value>-5.4117</value>
        </latitude>
        <longitude>
          <value>-81.979</value>
        </longitude>
        <depth>
          <value>12800.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181446549">
        <mag>
          <value>6.2</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4223754">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11906519</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181446118</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="177">
        <text>KERMADEC ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11906519" ns0:contributorOriginId="06935161" ns0:contributor="ISC" ns0:contributorEventId="604020958" ns0:catalog="ISC">
        <time>
          <value>2013-08-12T04:16:47.810000Z</value>
        </time>
        <latitude>
          <value>-30.6243</value>
        </latitude>
        <longitude>
          <value>-179.6421</value>
        </longitude>
        <depth>
          <value>341400.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181446118">
        <mag>
          <value>6.2</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4223750">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11905728</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181445853</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="280">
        <text>BANDA SEA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11905728" ns0:contributorOriginId="06935148" ns0:contributor="ISC" ns0:contributorEventId="604020957" ns0:catalog="ISC">
        <time>
          <value>2013-08-12T00:53:44.440000Z</value>
        </time>
        <latitude>
          <value>-7.1167</value>
        </latitude>
        <longitude>
          <value>129.7914</value>
        </longitude>
        <depth>
          <value>104800.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181445853">
        <mag>
          <value>6.1</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9172754">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11905019</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181445508</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="306">
        <text>XIZANG</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11905019" ns0:contributorOriginId="06935134" ns0:contributor="ISC" ns0:contributorEventId="604645882" ns0:catalog="ISC">
        <time>
          <value>2013-08-11T21:23:43.870000Z</value>
        </time>
        <latitude>
          <value>30.0413</value>
        </latitude>
        <longitude>
          <value>97.9708</value>
        </longitude>
        <depth>
          <value>23000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181445508">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9166029">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11871325</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181433859</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="189">
        <text>SOUTHEAST OF LOYALTY ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11871325" ns0:contributorOriginId="06934608" ns0:contributor="ISC" ns0:contributorEventId="604020943" ns0:catalog="ISC">
        <time>
          <value>2013-08-06T10:41:34.730000Z</value>
        </time>
        <latitude>
          <value>-22.5917</value>
        </latitude>
        <longitude>
          <value>173.8109</value>
        </longitude>
        <depth>
          <value>35000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181433859">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9164202">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11862169</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181430579</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="663">
        <text>SEA OF OKHOTSK</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11862169" ns0:contributorOriginId="06934440" ns0:contributor="ISC" ns0:contributorEventId="605285642" ns0:catalog="ISC">
        <time>
          <value>2013-08-04T15:56:34.640000Z</value>
        </time>
        <latitude>
          <value>46.9353</value>
        </latitude>
        <longitude>
          <value>145.3073</value>
        </longitude>
        <depth>
          <value>374600.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181430579">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9163555">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11858922</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181429586</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="228">
        <text>NEAR EAST COAST OF HONSHU, JAPAN</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11858922" ns0:contributorOriginId="06934397" ns0:contributor="ISC" ns0:contributorEventId="608161129" ns0:catalog="ISC">
        <time>
          <value>2013-08-04T03:28:50.320000Z</value>
        </time>
        <latitude>
          <value>38.2361</value>
        </latitude>
        <longitude>
          <value>141.7821</value>
        </longitude>
        <depth>
          <value>52700.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181429586">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4222885">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11846126</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181425357</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="173">
        <text>TONGA ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11846126" ns0:contributorOriginId="06934204" ns0:contributor="ISC" ns0:contributorEventId="604020932" ns0:catalog="ISC">
        <time>
          <value>2013-08-01T20:01:42.990000Z</value>
        </time>
        <latitude>
          <value>-15.2651</value>
        </latitude>
        <longitude>
          <value>-173.4982</value>
        </longitude>
        <depth>
          <value>29000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181425357">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4222560">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11812125</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181413394</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="153">
        <text>SOUTH SANDWICH ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11812125" ns0:contributorOriginId="06836584" ns0:contributor="ISC" ns0:contributorEventId="608441212" ns0:catalog="ISC">
        <time>
          <value>2013-07-26T21:33:00.060000Z</value>
        </time>
        <latitude>
          <value>-58.0352</value>
        </latitude>
        <longitude>
          <value>-24.28</value>
        </longitude>
        <depth>
          <value>17000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181413394">
        <mag>
          <value>6.3</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4222514">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11808473</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181412059</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="186">
        <text>VANUATU ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11808473" ns0:contributorOriginId="06836512" ns0:contributor="ISC" ns0:contributorEventId="608441202" ns0:catalog="ISC">
        <time>
          <value>2013-07-26T07:07:17.330000Z</value>
        </time>
        <latitude>
          <value>-15.3205</value>
        </latitude>
        <longitude>
          <value>167.6206</value>
        </longitude>
        <depth>
          <value>141000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181412059">
        <mag>
          <value>6.1</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9150956">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11795873</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181407676</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="171">
        <text>SOUTH OF FIJI ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11795873" ns0:contributorOriginId="06836301" ns0:contributor="ISC" ns0:contributorEventId="603250607" ns0:catalog="ISC">
        <time>
          <value>2013-07-24T03:32:34.430000Z</value>
        </time>
        <latitude>
          <value>-23.1593</value>
        </latitude>
        <longitude>
          <value>-177.1534</value>
        </longitude>
        <depth>
          <value>168000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181407676">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4222300">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11784923</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181403876</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="431">
        <text>PRINCE EDWARD ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11784923" ns0:contributorOriginId="06836142" ns0:contributor="ISC" ns0:contributorEventId="608441186" ns0:catalog="ISC">
        <time>
          <value>2013-07-22T07:01:41.750000Z</value>
        </time>
        <latitude>
          <value>-46.0946</value>
        </latitude>
        <longitude>
          <value>34.8615</value>
        </longitude>
        <depth>
          <value>3200.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181403876">
        <mag>
          <value>6.3</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9148459">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11783376</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181403255</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="322">
        <text>GANSU, CHINA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11783376" ns0:contributorOriginId="06836112" ns0:contributor="ISC" ns0:contributorEventId="608441184" ns0:catalog="ISC">
        <time>
          <value>2013-07-21T23:45:55.920000Z</value>
        </time>
        <latitude>
          <value>34.5047</value>
        </latitude>
        <longitude>
          <value>104.2608</value>
        </longitude>
        <depth>
          <value>5100.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181403255">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4222241">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11777720</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181400736</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="163">
        <text>COOK STRAIT, NEW ZEALAND</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11777720" ns0:contributorOriginId="06835996" ns0:contributor="ISC" ns0:contributorEventId="603306074" ns0:catalog="ISC">
        <time>
          <value>2013-07-21T05:09:32.170000Z</value>
        </time>
        <latitude>
          <value>-41.6856</value>
        </latitude>
        <longitude>
          <value>174.3589</value>
        </longitude>
        <depth>
          <value>16300.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181400736">
        <mag>
          <value>6.5</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9145121">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11766678</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181397009</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="177">
        <text>KERMADEC ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11766678" ns0:contributorOriginId="06835827" ns0:contributor="ISC" ns0:contributorEventId="603306065" ns0:catalog="ISC">
        <time>
          <value>2013-07-19T11:40:42.500000Z</value>
        </time>
        <latitude>
          <value>-30.5559</value>
        </latitude>
        <longitude>
          <value>-176.2362</value>
        </longitude>
        <depth>
          <value>26300.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181397009">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4222027">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11752873</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181392128</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="117">
        <text>SOUTHERN PERU</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11752873" ns0:contributorOriginId="06835620" ns0:contributor="ISC" ns0:contributorEventId="603236456" ns0:catalog="ISC">
        <time>
          <value>2013-07-17T02:37:43.740000Z</value>
        </time>
        <latitude>
          <value>-15.6661</value>
        </latitude>
        <longitude>
          <value>-71.7458</value>
        </longitude>
        <depth>
          <value>13000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181392128">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9141285">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11747424</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181390398</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="193">
        <text>SOLOMON ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11747424" ns0:contributorOriginId="06835546" ns0:contributor="ISC" ns0:contributorEventId="603367329" ns0:catalog="ISC">
        <time>
          <value>2013-07-16T09:35:55.530000Z</value>
        </time>
        <latitude>
          <value>-6.2479</value>
        </latitude>
        <longitude>
          <value>154.8112</value>
        </longitude>
        <depth>
          <value>57400.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181390398">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4221969">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11742573</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181388627</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="153">
        <text>SOUTH SANDWICH ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11742573" ns0:contributorOriginId="06835458" ns0:contributor="ISC" ns0:contributorEventId="603285344" ns0:catalog="ISC">
        <time>
          <value>2013-07-15T14:03:41.730000Z</value>
        </time>
        <latitude>
          <value>-61.0527</value>
        </latitude>
        <longitude>
          <value>-25.2274</value>
        </longitude>
        <depth>
          <value>19900.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181388627">
        <mag>
          <value>7.3</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9140307">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11742526</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181388585</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="169">
        <text>SAMOA ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11742526" ns0:contributorOriginId="06835455" ns0:contributor="ISC" ns0:contributorEventId="603232945" ns0:catalog="ISC">
        <time>
          <value>2013-07-15T13:59:06.680000Z</value>
        </time>
        <latitude>
          <value>-15.5503</value>
        </latitude>
        <longitude>
          <value>-172.971</value>
        </longitude>
        <depth>
          <value>29000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181388585">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4221619">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11696019</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181372446</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="192">
        <text>NEW BRITAIN REGION, P.N.G.</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11696019" ns0:contributorOriginId="06834658" ns0:contributor="ISC" ns0:contributorEventId="603210636" ns0:catalog="ISC">
        <time>
          <value>2013-07-07T20:30:07.380000Z</value>
        </time>
        <latitude>
          <value>-6.036</value>
        </latitude>
        <longitude>
          <value>149.7262</value>
        </longitude>
        <depth>
          <value>65900.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181372446">
        <mag>
          <value>6.6</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4221617">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11695624</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181372319</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="190">
        <text>NEW IRELAND REGION, P.N.G.</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11695624" ns0:contributorOriginId="06834655" ns0:contributor="ISC" ns0:contributorEventId="603179557" ns0:catalog="ISC">
        <time>
          <value>2013-07-07T18:35:31.380000Z</value>
        </time>
        <latitude>
          <value>-4.0134</value>
        </latitude>
        <longitude>
          <value>153.9277</value>
        </longitude>
        <depth>
          <value>387100.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181372319">
        <mag>
          <value>7.3</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4221531">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11686526</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181369536</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="274">
        <text>SOUTHERN SUMATRA, INDONESIA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11686526" ns0:contributorOriginId="06834545" ns0:contributor="ISC" ns0:contributorEventId="603179116" ns0:catalog="ISC">
        <time>
          <value>2013-07-06T05:05:06.960000Z</value>
        </time>
        <latitude>
          <value>-3.294</value>
        </latitude>
        <longitude>
          <value>100.5312</value>
        </longitude>
        <depth>
          <value>26100.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181369536">
        <mag>
          <value>6.1</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4221491">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11677724</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181366541</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="193">
        <text>SOLOMON ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11677724" ns0:contributorOriginId="06834403" ns0:contributor="ISC" ns0:contributorEventId="603308099" ns0:catalog="ISC">
        <time>
          <value>2013-07-04T17:15:57.680000Z</value>
        </time>
        <latitude>
          <value>-7.0042</value>
        </latitude>
        <longitude>
          <value>155.7104</value>
        </longitude>
        <depth>
          <value>55200.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181366541">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4221326">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11662822</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181361488</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="706">
        <text>NORTHERN SUMATRA, INDONESIA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11662822" ns0:contributorOriginId="06834189" ns0:contributor="ISC" ns0:contributorEventId="603173687" ns0:catalog="ISC">
        <time>
          <value>2013-07-02T07:37:05.090000Z</value>
        </time>
        <latitude>
          <value>4.6907</value>
        </latitude>
        <longitude>
          <value>96.5824</value>
        </longitude>
        <depth>
          <value>25400.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181361488">
        <mag>
          <value>6.1</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9118623">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11633824</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181351831</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="267">
        <text>HALMAHERA, INDONESIA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11633824" ns0:contributorOriginId="06594297" ns0:contributor="ISC" ns0:contributorEventId="603157278" ns0:catalog="ISC">
        <time>
          <value>2013-06-27T08:38:09.950000Z</value>
        </time>
        <latitude>
          <value>1.1025</value>
        </latitude>
        <longitude>
          <value>127.1248</value>
        </longitude>
        <depth>
          <value>136400.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181351831">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4220606">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11617477</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181346381</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="403">
        <text>NORTHERN MID-ATLANTIC RIDGE</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11617477" ns0:contributorOriginId="06594059" ns0:contributor="ISC" ns0:contributorEventId="607922711" ns0:catalog="ISC">
        <time>
          <value>2013-06-24T22:04:14.180000Z</value>
        </time>
        <latitude>
          <value>10.6905</value>
        </latitude>
        <longitude>
          <value>-42.6687</value>
        </longitude>
        <depth>
          <value>17500.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181346381">
        <mag>
          <value>6.5</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4220253">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11567569</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181328887</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="370">
        <text>CRETE, GREECE</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11567569" ns0:contributorOriginId="06593287" ns0:contributor="ISC" ns0:contributorEventId="603075641" ns0:catalog="ISC">
        <time>
          <value>2013-06-16T21:39:06.600000Z</value>
        </time>
        <latitude>
          <value>34.4242</value>
        </latitude>
        <longitude>
          <value>25.1864</value>
        </longitude>
        <depth>
          <value>25000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181328887">
        <mag>
          <value>6.1</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9104463">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11563027</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181327287</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="59">
        <text>GUERRERO, MEXICO</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11563027" ns0:contributorOriginId="06593212" ns0:contributor="ISC" ns0:contributorEventId="603077575" ns0:catalog="ISC">
        <time>
          <value>2013-06-16T05:19:00.660000Z</value>
        </time>
        <latitude>
          <value>18.1653</value>
        </latitude>
        <longitude>
          <value>-99.1506</value>
        </longitude>
        <depth>
          <value>55000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181327287">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4220195">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11559574</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181325699</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="74">
        <text>NEAR COAST OF NICARAGUA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11559574" ns0:contributorOriginId="06593113" ns0:contributor="ISC" ns0:contributorEventId="603074603" ns0:catalog="ISC">
        <time>
          <value>2013-06-15T17:34:29.170000Z</value>
        </time>
        <latitude>
          <value>11.7418</value>
        </latitude>
        <longitude>
          <value>-87.0457</value>
        </longitude>
        <depth>
          <value>41400.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181325699">
        <mag>
          <value>6.4</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4220191">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11559174</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181325317</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="370">
        <text>CRETE, GREECE</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11559174" ns0:contributorOriginId="06593083" ns0:contributor="ISC" ns0:contributorEventId="606825246" ns0:catalog="ISC">
        <time>
          <value>2013-06-15T16:11:02.090000Z</value>
        </time>
        <latitude>
          <value>34.4507</value>
        </latitude>
        <longitude>
          <value>25.044</value>
        </longitude>
        <depth>
          <value>21500.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181325317">
        <mag>
          <value>6.3</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4220149">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11557878</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181324870</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="179">
        <text>SOUTH OF KERMADEC ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11557878" ns0:contributorOriginId="06593057" ns0:contributor="ISC" ns0:contributorEventId="603236815" ns0:catalog="ISC">
        <time>
          <value>2013-06-15T11:20:37.660000Z</value>
        </time>
        <latitude>
          <value>-33.9023</value>
        </latitude>
        <longitude>
          <value>179.4859</value>
        </longitude>
        <depth>
          <value>210000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181324870">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4220111">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11547224</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181321554</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="282">
        <text>SOUTH OF JAVA, INDONESIA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11547224" ns0:contributorOriginId="06592914" ns0:contributor="ISC" ns0:contributorEventId="606825233" ns0:catalog="ISC">
        <time>
          <value>2013-06-13T16:47:25.800000Z</value>
        </time>
        <latitude>
          <value>-9.9513</value>
        </latitude>
        <longitude>
          <value>107.3023</value>
        </longitude>
        <depth>
          <value>20600.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181321554">
        <mag>
          <value>6.6</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9093758">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11509522</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181309377</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="171">
        <text>SOUTH OF FIJI ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11509522" ns0:contributorOriginId="06592383" ns0:contributor="ISC" ns0:contributorEventId="603055231" ns0:catalog="ISC">
        <time>
          <value>2013-06-07T12:54:10.470000Z</value>
        </time>
        <latitude>
          <value>-23.8633</value>
        </latitude>
        <longitude>
          <value>179.0992</value>
        </longitude>
        <depth>
          <value>557700.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181309377">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4219680">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11495326</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181304406</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="184">
        <text>SANTA CRUZ ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11495326" ns0:contributorOriginId="06592147" ns0:contributor="ISC" ns0:contributorEventId="603051431" ns0:catalog="ISC">
        <time>
          <value>2013-06-05T04:47:27.260000Z</value>
        </time>
        <latitude>
          <value>-11.4032</value>
        </latitude>
        <longitude>
          <value>166.3017</value>
        </longitude>
        <depth>
          <value>53600.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181304406">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4219504">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11475126</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181297709</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="244">
        <text>TAIWAN</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11475126" ns0:contributorOriginId="06591846" ns0:contributor="ISC" ns0:contributorEventId="603042353" ns0:catalog="ISC">
        <time>
          <value>2013-06-02T05:43:03.800000Z</value>
        </time>
        <latitude>
          <value>23.8318</value>
        </latitude>
        <longitude>
          <value>121.059</value>
        </longitude>
        <depth>
          <value>17000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181297709">
        <mag>
          <value>6.3</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9261366">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12349169</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181286499</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="80">
        <text>PANAMA-COSTA RICA BORDER REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12349169" ns0:contributorOriginId="06518957" ns0:contributor="ISC" ns0:contributorEventId="603009419" ns0:catalog="ISC">
        <time>
          <value>2013-05-27T09:41:14.670000Z</value>
        </time>
        <latitude>
          <value>9.3613</value>
        </latitude>
        <longitude>
          <value>-82.6543</value>
        </longitude>
        <depth>
          <value>15900.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181286499">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9261102">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12347825</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181285985</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="740">
        <text>OWEN FRACTURE ZONE REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12347825" ns0:contributorOriginId="06518928" ns0:contributor="ISC" ns0:contributorEventId="603161566" ns0:catalog="ISC">
        <time>
          <value>2013-05-27T03:36:29.690000Z</value>
        </time>
        <latitude>
          <value>14.6508</value>
        </latitude>
        <longitude>
          <value>53.8151</value>
        </longitude>
        <depth>
          <value>3100.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181285985">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9260096">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12342819</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181284283</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="714">
        <text>SOUTHEASTERN UZBEKISTAN</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12342819" ns0:contributorOriginId="06518838" ns0:contributor="ISC" ns0:contributorEventId="607679918" ns0:catalog="ISC">
        <time>
          <value>2013-05-26T06:08:15.310000Z</value>
        </time>
        <latitude>
          <value>39.9496</value>
        </latitude>
        <longitude>
          <value>67.4358</value>
        </longitude>
        <depth>
          <value>15500.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181284283">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4218719">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12333374</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181281199</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="663">
        <text>SEA OF OKHOTSK</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12333374" ns0:contributorOriginId="06518676" ns0:contributor="ISC" ns0:contributorEventId="603007143" ns0:catalog="ISC">
        <time>
          <value>2013-05-24T14:56:31.600000Z</value>
        </time>
        <latitude>
          <value>52.1357</value>
        </latitude>
        <longitude>
          <value>151.5688</value>
        </longitude>
        <depth>
          <value>632000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181281199">
        <mag>
          <value>6.7</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9258040">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12332474</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181280852</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="175">
        <text>SOUTH OF TONGA ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12332474" ns0:contributorOriginId="06518650" ns0:contributor="ISC" ns0:contributorEventId="603006918" ns0:catalog="ISC">
        <time>
          <value>2013-05-24T11:10:48.990000Z</value>
        </time>
        <latitude>
          <value>-24.4007</value>
        </latitude>
        <longitude>
          <value>-175.0012</value>
        </longitude>
        <depth>
          <value>12900.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181280852">
        <mag>
          <value>5.8</value>
        </mag>
        <type>mb</type>
        <creationInfo>
          <author>NEIC</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4218658">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12331019</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181280212</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="663">
        <text>SEA OF OKHOTSK</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12331019" ns0:contributorOriginId="06518593" ns0:contributor="ISC" ns0:contributorEventId="603007131" ns0:catalog="ISC">
        <time>
          <value>2013-05-24T05:44:49.880000Z</value>
        </time>
        <latitude>
          <value>54.815</value>
        </latitude>
        <longitude>
          <value>153.3912</value>
        </longitude>
        <depth>
          <value>607000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181280212">
        <mag>
          <value>8.3</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4218648">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12328828</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181279385</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="173">
        <text>TONGA ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12328828" ns0:contributorOriginId="06518549" ns0:contributor="ISC" ns0:contributorEventId="603044135" ns0:catalog="ISC">
        <time>
          <value>2013-05-23T21:07:46.050000Z</value>
        </time>
        <latitude>
          <value>-20.6244</value>
        </latitude>
        <longitude>
          <value>-175.7552</value>
        </longitude>
        <depth>
          <value>151600.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181279385">
        <mag>
          <value>6.3</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4218647">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12327872</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181279035</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="171">
        <text>SOUTH OF FIJI ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12327872" ns0:contributorOriginId="06518529" ns0:contributor="ISC" ns0:contributorEventId="603044134" ns0:catalog="ISC">
        <time>
          <value>2013-05-23T17:19:04.840000Z</value>
        </time>
        <latitude>
          <value>-23.0783</value>
        </latitude>
        <longitude>
          <value>-177.2133</value>
        </longitude>
        <depth>
          <value>177700.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181279035">
        <mag>
          <value>7.4</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4218468">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12310775</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181273109</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="219">
        <text>OFF EAST COAST OF KAMCHATKA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12310775" ns0:contributorOriginId="06518250" ns0:contributor="ISC" ns0:contributorEventId="603161292" ns0:catalog="ISC">
        <time>
          <value>2013-05-21T05:43:22.120000Z</value>
        </time>
        <latitude>
          <value>52.2164</value>
        </latitude>
        <longitude>
          <value>160.2246</value>
        </longitude>
        <depth>
          <value>45900.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181273109">
        <mag>
          <value>6.2</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9253507">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12309674</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181272584</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="219">
        <text>OFF EAST COAST OF KAMCHATKA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12309674" ns0:contributorOriginId="06518211" ns0:contributor="ISC" ns0:contributorEventId="602987353" ns0:catalog="ISC">
        <time>
          <value>2013-05-21T03:08:21.230000Z</value>
        </time>
        <latitude>
          <value>52.3757</value>
        </latitude>
        <longitude>
          <value>160.2592</value>
        </longitude>
        <depth>
          <value>43800.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181272584">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9253502">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12309669</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181272557</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="219">
        <text>OFF EAST COAST OF KAMCHATKA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12309669" ns0:contributorOriginId="06518210" ns0:contributor="ISC" ns0:contributorEventId="602987352" ns0:catalog="ISC">
        <time>
          <value>2013-05-21T03:05:52.320000Z</value>
        </time>
        <latitude>
          <value>52.2681</value>
        </latitude>
        <longitude>
          <value>160.4429</value>
        </longitude>
        <depth>
          <value>27000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181272557">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4218461">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12309370</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181272410</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="219">
        <text>OFF EAST COAST OF KAMCHATKA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12309370" ns0:contributorOriginId="06518200" ns0:contributor="ISC" ns0:contributorEventId="602987347" ns0:catalog="ISC">
        <time>
          <value>2013-05-21T01:55:06.670000Z</value>
        </time>
        <latitude>
          <value>52.414</value>
        </latitude>
        <longitude>
          <value>160.5733</value>
        </longitude>
        <depth>
          <value>23400.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181272410">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4218406">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12305370</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181270751</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="143">
        <text>OFF COAST OF SOUTHERN CHILE</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12305370" ns0:contributorOriginId="06518107" ns0:contributor="ISC" ns0:contributorEventId="602972660" ns0:catalog="ISC">
        <time>
          <value>2013-05-20T09:49:08.210000Z</value>
        </time>
        <latitude>
          <value>-45.0062</value>
        </latitude>
        <longitude>
          <value>-80.7244</value>
        </longitude>
        <depth>
          <value>35000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181270751">
        <mag>
          <value>6.3</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9251851">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12301378</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181268699</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="219">
        <text>OFF EAST COAST OF KAMCHATKA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12301378" ns0:contributorOriginId="06517969" ns0:contributor="ISC" ns0:contributorEventId="603161182" ns0:catalog="ISC">
        <time>
          <value>2013-05-19T18:44:11.520000Z</value>
        </time>
        <latitude>
          <value>52.1909</value>
        </latitude>
        <longitude>
          <value>160.3548</value>
        </longitude>
        <depth>
          <value>26200.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181268699">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4218257">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12292075</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181265045</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="228">
        <text>NEAR EAST COAST OF HONSHU, JAPAN</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12292075" ns0:contributorOriginId="06517775" ns0:contributor="ISC" ns0:contributorEventId="602958409" ns0:catalog="ISC">
        <time>
          <value>2013-05-18T05:48:00.000000Z</value>
        </time>
        <latitude>
          <value>37.7604</value>
        </latitude>
        <longitude>
          <value>141.5665</value>
        </longitude>
        <depth>
          <value>45600.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181265045">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4218030">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12266926</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181255834</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="216">
        <text>MARIANA ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12266926" ns0:contributorOriginId="06517281" ns0:contributor="ISC" ns0:contributorEventId="602949682" ns0:catalog="ISC">
        <time>
          <value>2013-05-14T00:32:26.430000Z</value>
        </time>
        <latitude>
          <value>18.7672</value>
        </latitude>
        <longitude>
          <value>145.2903</value>
        </longitude>
        <depth>
          <value>611700.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181255834">
        <mag>
          <value>6.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4217937">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12253920</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181250982</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="173">
        <text>TONGA ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12253920" ns0:contributorOriginId="06517025" ns0:contributor="ISC" ns0:contributorEventId="603044077" ns0:catalog="ISC">
        <time>
          <value>2013-05-11T20:46:57.260000Z</value>
        </time>
        <latitude>
          <value>-17.9392</value>
        </latitude>
        <longitude>
          <value>-175.1186</value>
        </longitude>
        <depth>
          <value>210300.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181250982">
        <mag>
          <value>6.3</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4217886">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12249670</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181249232</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="353">
        <text>SOUTHERN IRAN</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12249670" ns0:contributorOriginId="06516931" ns0:contributor="ISC" ns0:contributorEventId="607304923" ns0:catalog="ISC">
        <time>
          <value>2013-05-11T02:08:11.290000Z</value>
        </time>
        <latitude>
          <value>26.6583</value>
        </latitude>
        <longitude>
          <value>57.8438</value>
        </longitude>
        <depth>
          <value>15100.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181249232">
        <mag>
          <value>6.2</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9241265">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12248227</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181248720</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="410">
        <text>SOUTHERN MID-ATLANTIC RIDGE</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12248227" ns0:contributorOriginId="06516905" ns0:contributor="ISC" ns0:contributorEventId="602945737" ns0:catalog="ISC">
        <time>
          <value>2013-05-10T19:56:06.610000Z</value>
        </time>
        <latitude>
          <value>-28.9678</value>
        </latitude>
        <longitude>
          <value>-13.1831</value>
        </longitude>
        <depth>
          <value>13800.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181248720">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9236846">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12226120</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181241472</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="171">
        <text>SOUTH OF FIJI ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12226120" ns0:contributorOriginId="06516581" ns0:contributor="ISC" ns0:contributorEventId="603044059" ns0:catalog="ISC">
        <time>
          <value>2013-05-07T10:10:51.650000Z</value>
        </time>
        <latitude>
          <value>-19.6485</value>
        </latitude>
        <longitude>
          <value>175.2114</value>
        </longitude>
        <depth>
          <value>33300.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181241472">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9335151">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12796128</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181226619</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="404">
        <text>AZORES ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12796128" ns0:contributorOriginId="06348790" ns0:contributor="ISC" ns0:contributorEventId="602909923" ns0:catalog="ISC">
        <time>
          <value>2013-04-30T06:25:24.270000Z</value>
        </time>
        <latitude>
          <value>37.5889</value>
        </latitude>
        <longitude>
          <value>-24.9594</value>
        </longitude>
        <depth>
          <value>19700.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181226619">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4216012">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12771177</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181218618</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="177">
        <text>KERMADEC ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12771177" ns0:contributorOriginId="06348398" ns0:contributor="ISC" ns0:contributorEventId="602894394" ns0:catalog="ISC">
        <time>
          <value>2013-04-26T06:53:28.910000Z</value>
        </time>
        <latitude>
          <value>-28.7301</value>
        </latitude>
        <longitude>
          <value>-178.8979</value>
        </longitude>
        <depth>
          <value>355400.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181218618">
        <mag>
          <value>6.2</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4215888">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12756775</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181213851</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="190">
        <text>NEW IRELAND REGION, P.N.G.</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12756775" ns0:contributorOriginId="06348159" ns0:contributor="ISC" ns0:contributorEventId="602880302" ns0:catalog="ISC">
        <time>
          <value>2013-04-23T23:14:43.060000Z</value>
        </time>
        <latitude>
          <value>-3.8359</value>
        </latitude>
        <longitude>
          <value>152.2033</value>
        </longitude>
        <depth>
          <value>24400.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181213851">
        <mag>
          <value>6.5</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4215747">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12739921</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181208706</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="57">
        <text>MICHOACAN, MEXICO</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12739921" ns0:contributorOriginId="06347917" ns0:contributor="ISC" ns0:contributorEventId="602874659" ns0:catalog="ISC">
        <time>
          <value>2013-04-22T01:16:32.560000Z</value>
        </time>
        <latitude>
          <value>18.0412</value>
        </latitude>
        <longitude>
          <value>-102.1983</value>
        </longitude>
        <depth>
          <value>31500.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181208706">
        <mag>
          <value>6.1</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4215721">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12731678</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181206140</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="211">
        <text>SOUTHEAST OF HONSHU, JAPAN</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12731678" ns0:contributorOriginId="06347788" ns0:contributor="ISC" ns0:contributorEventId="602873208" ns0:catalog="ISC">
        <time>
          <value>2013-04-21T03:22:16.920000Z</value>
        </time>
        <latitude>
          <value>29.9644</value>
        </latitude>
        <longitude>
          <value>138.9741</value>
        </longitude>
        <depth>
          <value>431300.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181206140">
        <mag>
          <value>6.1</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4215707">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12726822</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181204272</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="221">
        <text>KURIL ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12726822" ns0:contributorOriginId="06347691" ns0:contributor="ISC" ns0:contributorEventId="602871480" ns0:catalog="ISC">
        <time>
          <value>2013-04-20T13:12:52.080000Z</value>
        </time>
        <latitude>
          <value>50.0079</value>
        </latitude>
        <longitude>
          <value>157.3684</value>
        </longitude>
        <depth>
          <value>32800.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181204272">
        <mag>
          <value>6.1</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9320854">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12724425</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181203377</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="412">
        <text>BOUVET ISLAND REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12724425" ns0:contributorOriginId="06347642" ns0:contributor="ISC" ns0:contributorEventId="602866270" ns0:catalog="ISC">
        <time>
          <value>2013-04-20T05:11:57.520000Z</value>
        </time>
        <latitude>
          <value>-54.9059</value>
        </latitude>
        <longitude>
          <value>1.1227</value>
        </longitude>
        <depth>
          <value>10000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181203377">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4215694">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12724374</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181203330</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="280">
        <text>BANDA SEA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12724374" ns0:contributorOriginId="06347639" ns0:contributor="ISC" ns0:contributorEventId="604743867" ns0:catalog="ISC">
        <time>
          <value>2013-04-20T04:51:12.270000Z</value>
        </time>
        <latitude>
          <value>-6.2786</value>
        </latitude>
        <longitude>
          <value>130.2233</value>
        </longitude>
        <depth>
          <value>113900.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181203330">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4215680">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12722971</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181202500</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="307">
        <text>SICHUAN, CHINA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12722971" ns0:contributorOriginId="06347570" ns0:contributor="ISC" ns0:contributorEventId="607304721" ns0:catalog="ISC">
        <time>
          <value>2013-04-20T00:02:47.980000Z</value>
        </time>
        <latitude>
          <value>30.2688</value>
        </latitude>
        <longitude>
          <value>103.0202</value>
        </longitude>
        <depth>
          <value>18000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181202500">
        <mag>
          <value>6.6</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4215671">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12721877</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181202106</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="222">
        <text>EAST OF KURIL ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12721877" ns0:contributorOriginId="06347549" ns0:contributor="ISC" ns0:contributorEventId="607304709" ns0:catalog="ISC">
        <time>
          <value>2013-04-19T19:58:41.520000Z</value>
        </time>
        <latitude>
          <value>49.9301</value>
        </latitude>
        <longitude>
          <value>157.7172</value>
        </longitude>
        <depth>
          <value>24600.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181202106">
        <mag>
          <value>6.1</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9320239">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12721373</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181201925</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="293">
        <text>SOUTH OF TIMOR, INDONESIA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12721373" ns0:contributorOriginId="06347540" ns0:contributor="ISC" ns0:contributorEventId="602854906" ns0:catalog="ISC">
        <time>
          <value>2013-04-19T17:51:42.660000Z</value>
        </time>
        <latitude>
          <value>-11.9123</value>
        </latitude>
        <longitude>
          <value>121.6642</value>
        </longitude>
        <depth>
          <value>24200.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181201925">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4215630">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12717521</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181200677</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="221">
        <text>KURIL ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12717521" ns0:contributorOriginId="06347471" ns0:contributor="ISC" ns0:contributorEventId="602836134" ns0:catalog="ISC">
        <time>
          <value>2013-04-19T03:05:52.630000Z</value>
        </time>
        <latitude>
          <value>46.0722</value>
        </latitude>
        <longitude>
          <value>150.924</value>
        </longitude>
        <depth>
          <value>109000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181200677">
        <mag>
          <value>7.2</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9317136">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12705823</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181196041</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="228">
        <text>NEAR EAST COAST OF HONSHU, JAPAN</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12705823" ns0:contributorOriginId="06347189" ns0:contributor="ISC" ns0:contributorEventId="602822821" ns0:catalog="ISC">
        <time>
          <value>2013-04-17T12:03:31.910000Z</value>
        </time>
        <latitude>
          <value>38.5205</value>
        </latitude>
        <longitude>
          <value>141.551</value>
        </longitude>
        <depth>
          <value>51400.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181196041">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4215465">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12701477</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181193686</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="200">
        <text>NEAR N COAST OF NEW GUINEA, PNG.</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12701477" ns0:contributorOriginId="06346994" ns0:contributor="ISC" ns0:contributorEventId="602945524" ns0:catalog="ISC">
        <time>
          <value>2013-04-16T22:55:27.360000Z</value>
        </time>
        <latitude>
          <value>-3.2237</value>
        </latitude>
        <longitude>
          <value>142.5671</value>
        </longitude>
        <depth>
          <value>16300.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181193686">
        <mag>
          <value>6.6</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4215452">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12698127</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181192589</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="353">
        <text>SOUTHERN IRAN</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12698127" ns0:contributorOriginId="06346936" ns0:contributor="ISC" ns0:contributorEventId="606824152" ns0:catalog="ISC">
        <time>
          <value>2013-04-16T10:44:19.030000Z</value>
        </time>
        <latitude>
          <value>27.9704</value>
        </latitude>
        <longitude>
          <value>62.136</value>
        </longitude>
        <depth>
          <value>63100.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181192589">
        <mag>
          <value>7.7</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4215378">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12684175</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181187579</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="193">
        <text>SOLOMON ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12684175" ns0:contributorOriginId="06346672" ns0:contributor="ISC" ns0:contributorEventId="602787767" ns0:catalog="ISC">
        <time>
          <value>2013-04-14T01:32:24.640000Z</value>
        </time>
        <latitude>
          <value>-6.5017</value>
        </latitude>
        <longitude>
          <value>154.6407</value>
        </longitude>
        <depth>
          <value>47100.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181187579">
        <mag>
          <value>6.5</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4215377">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12683519</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181187330</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="186">
        <text>VANUATU ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12683519" ns0:contributorOriginId="06346661" ns0:contributor="ISC" ns0:contributorEventId="602787761" ns0:catalog="ISC">
        <time>
          <value>2013-04-13T22:49:49.200000Z</value>
        </time>
        <latitude>
          <value>-19.103</value>
        </latitude>
        <longitude>
          <value>169.6524</value>
        </longitude>
        <depth>
          <value>269300.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181187330">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9310852">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12674320</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181184489</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="233">
        <text>NEAR S. COAST OF WESTERN HONSHU</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12674320" ns0:contributorOriginId="06346537" ns0:contributor="ISC" ns0:contributorEventId="602787145" ns0:catalog="ISC">
        <time>
          <value>2013-04-12T20:33:17.930000Z</value>
        </time>
        <latitude>
          <value>34.4012</value>
        </latitude>
        <longitude>
          <value>134.8652</value>
        </longitude>
        <depth>
          <value>16500.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181184489">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9308265">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12661319</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181179767</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="248">
        <text>PHILIPPINE ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12661319" ns0:contributorOriginId="06346292" ns0:contributor="ISC" ns0:contributorEventId="603040345" ns0:catalog="ISC">
        <time>
          <value>2013-04-10T20:20:27.760000Z</value>
        </time>
        <latitude>
          <value>20.7141</value>
        </latitude>
        <longitude>
          <value>122.1812</value>
        </longitude>
        <depth>
          <value>17800.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181179767">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4215083">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=12652525</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181175672</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="353">
        <text>SOUTHERN IRAN</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=12652525" ns0:contributorOriginId="06346046" ns0:contributor="ISC" ns0:contributorEventId="607304565" ns0:catalog="ISC">
        <time>
          <value>2013-04-09T11:52:50.190000Z</value>
        </time>
        <latitude>
          <value>28.4463</value>
        </latitude>
        <longitude>
          <value>51.629</value>
        </longitude>
        <depth>
          <value>12800.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181175672">
        <mag>
          <value>6.3</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4214964">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11465419</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181169409</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="201">
        <text>IRIAN JAYA, INDONESIA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11465419" ns0:contributorOriginId="06345720" ns0:contributor="ISC" ns0:contributorEventId="602761452" ns0:catalog="ISC">
        <time>
          <value>2013-04-06T04:42:36.190000Z</value>
        </time>
        <latitude>
          <value>-3.4714</value>
        </latitude>
        <longitude>
          <value>138.4743</value>
        </longitude>
        <depth>
          <value>69400.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181169409">
        <mag>
          <value>7.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9084935">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11464726</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181169173</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="657">
        <text>E. RUSSIA-N.E. CHINA BORDER REG.</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11464726" ns0:contributorOriginId="06345708" ns0:contributor="ISC" ns0:contributorEventId="602761442" ns0:catalog="ISC">
        <time>
          <value>2013-04-06T00:29:55.510000Z</value>
        </time>
        <latitude>
          <value>42.7356</value>
        </latitude>
        <longitude>
          <value>131.0457</value>
        </longitude>
        <depth>
          <value>572300.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181169173">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4214963">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11462425</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181168263</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="657">
        <text>E. RUSSIA-N.E. CHINA BORDER REG.</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11462425" ns0:contributorOriginId="06345663" ns0:contributor="ISC" ns0:contributorEventId="607304526" ns0:catalog="ISC">
        <time>
          <value>2013-04-05T13:00:02.360000Z</value>
        </time>
        <latitude>
          <value>42.7359</value>
        </latitude>
        <longitude>
          <value>131.064</value>
        </longitude>
        <depth>
          <value>571300.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181168263">
        <mag>
          <value>6.3</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9081124">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11445675</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181162154</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="428">
        <text>SOUTHWEST INDIAN RIDGE</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11445675" ns0:contributorOriginId="06345353" ns0:contributor="ISC" ns0:contributorEventId="606824064" ns0:catalog="ISC">
        <time>
          <value>2013-04-02T14:34:56.830000Z</value>
        </time>
        <latitude>
          <value>-40.3937</value>
        </latitude>
        <longitude>
          <value>45.1857</value>
        </longitude>
        <depth>
          <value>14300.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181162154">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4214860">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=11440472</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181160381</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="229">
        <text>OFF EAST COAST OF HONSHU, JAPAN</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=11440472" ns0:contributorOriginId="06345258" ns0:contributor="ISC" ns0:contributorEventId="603039734" ns0:catalog="ISC">
        <time>
          <value>2013-04-01T18:53:17.550000Z</value>
        </time>
        <latitude>
          <value>39.5072</value>
        </latitude>
        <longitude>
          <value>143.2662</value>
        </longitude>
        <depth>
          <value>16200.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181160381">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9059335">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6150933</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181148209</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="244">
        <text>TAIWAN</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6150933" ns0:contributorOriginId="06194143" ns0:contributor="ISC" ns0:contributorEventId="602751080" ns0:catalog="ISC">
        <time>
          <value>2013-03-27T02:03:19.940000Z</value>
        </time>
        <latitude>
          <value>23.8815</value>
        </latitude>
        <longitude>
          <value>121.135</value>
        </longitude>
        <depth>
          <value>18600.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181148209">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4214565">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6149725</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181145923</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="70">
        <text>GUATEMALA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6149725" ns0:contributorOriginId="06194015" ns0:contributor="ISC" ns0:contributorEventId="607304477" ns0:catalog="ISC">
        <time>
          <value>2013-03-25T23:02:12.520000Z</value>
        </time>
        <latitude>
          <value>14.5905</value>
        </latitude>
        <longitude>
          <value>-90.4772</value>
        </longitude>
        <depth>
          <value>193700.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181145923">
        <mag>
          <value>6.2</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4214443">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6147871</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181142597</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="185">
        <text>VANUATU ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6147871" ns0:contributorOriginId="06193841" ns0:contributor="ISC" ns0:contributorEventId="602731244" ns0:catalog="ISC">
        <time>
          <value>2013-03-24T08:13:45.160000Z</value>
        </time>
        <latitude>
          <value>-20.8706</value>
        </latitude>
        <longitude>
          <value>173.4536</value>
        </longitude>
        <depth>
          <value>11700.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181142597">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9056123">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6147721</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181142292</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="222">
        <text>EAST OF KURIL ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6147721" ns0:contributorOriginId="06193820" ns0:contributor="ISC" ns0:contributorEventId="602735716" ns0:catalog="ISC">
        <time>
          <value>2013-03-24T04:18:37.210000Z</value>
        </time>
        <latitude>
          <value>50.6623</value>
        </latitude>
        <longitude>
          <value>160.2335</value>
        </longitude>
        <depth>
          <value>30000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181142292">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9050504">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6142102</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181131916</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="153">
        <text>SOUTH SANDWICH ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6142102" ns0:contributorOriginId="06193304" ns0:contributor="ISC" ns0:contributorEventId="602758641" ns0:catalog="ISC">
        <time>
          <value>2013-03-19T03:29:02.700000Z</value>
        </time>
        <latitude>
          <value>-58.9815</value>
        </latitude>
        <longitude>
          <value>-24.5697</value>
        </longitude>
        <depth>
          <value>52000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181131916">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9043893">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6135491</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181120190</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="671">
        <text>EASTERN SIBERIA, RUSSIA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6135491" ns0:contributorOriginId="06192701" ns0:contributor="ISC" ns0:contributorEventId="602602456" ns0:catalog="ISC">
        <time>
          <value>2013-03-13T03:12:52.680000Z</value>
        </time>
        <latitude>
          <value>60.1064</value>
        </latitude>
        <longitude>
          <value>163.5095</value>
        </longitude>
        <depth>
          <value>9700.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181120190">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4213179">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6132633</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181114922</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="192">
        <text>NEW BRITAIN REGION, P.N.G.</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6132633" ns0:contributorOriginId="06192428" ns0:contributor="ISC" ns0:contributorEventId="602591360" ns0:catalog="ISC">
        <time>
          <value>2013-03-10T22:51:51.130000Z</value>
        </time>
        <latitude>
          <value>-6.6966</value>
        </latitude>
        <longitude>
          <value>148.2433</value>
        </longitude>
        <depth>
          <value>28900.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181114922">
        <mag>
          <value>6.5</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9039117">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6130715</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181111837</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="221">
        <text>KURIL ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6130715" ns0:contributorOriginId="06192299" ns0:contributor="ISC" ns0:contributorEventId="602591093" ns0:catalog="ISC">
        <time>
          <value>2013-03-09T14:56:29.360000Z</value>
        </time>
        <latitude>
          <value>50.8127</value>
        </latitude>
        <longitude>
          <value>157.3582</value>
        </longitude>
        <depth>
          <value>46000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181111837">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9032309">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6123907</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181101088</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="192">
        <text>NEW BRITAIN REGION, P.N.G.</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6123907" ns0:contributorOriginId="06191824" ns0:contributor="ISC" ns0:contributorEventId="602569626" ns0:catalog="ISC">
        <time>
          <value>2013-03-05T06:06:36.260000Z</value>
        </time>
        <latitude>
          <value>-5.2248</value>
        </latitude>
        <longitude>
          <value>152.6503</value>
        </longitude>
        <depth>
          <value>35000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181101088">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4212443">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6119753</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181093773</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="221">
        <text>KURIL ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6119753" ns0:contributorOriginId="06191466" ns0:contributor="ISC" ns0:contributorEventId="602566723" ns0:catalog="ISC">
        <time>
          <value>2013-03-01T13:20:52.900000Z</value>
        </time>
        <latitude>
          <value>50.8322</value>
        </latitude>
        <longitude>
          <value>157.625</value>
        </longitude>
        <depth>
          <value>58000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181093773">
        <mag>
          <value>6.5</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4212442">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6119732</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181093722</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="221">
        <text>KURIL ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6119732" ns0:contributorOriginId="06191464" ns0:contributor="ISC" ns0:contributorEventId="602566720" ns0:catalog="ISC">
        <time>
          <value>2013-03-01T12:53:53.930000Z</value>
        </time>
        <latitude>
          <value>50.9049</value>
        </latitude>
        <longitude>
          <value>157.6005</value>
        </longitude>
        <depth>
          <value>58300.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181093722">
        <mag>
          <value>6.4</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9027261">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6118859</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181092260</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="184">
        <text>SANTA CRUZ ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6118859" ns0:contributorOriginId="06014193" ns0:contributor="ISC" ns0:contributorEventId="602563850" ns0:catalog="ISC">
        <time>
          <value>2013-02-28T18:07:48.190000Z</value>
        </time>
        <latitude>
          <value>-10.942</value>
        </latitude>
        <longitude>
          <value>166.0219</value>
        </longitude>
        <depth>
          <value>40500.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181092260">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4212392">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6118640</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181091942</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="221">
        <text>KURIL ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6118640" ns0:contributorOriginId="06014180" ns0:contributor="ISC" ns0:contributorEventId="602562017" ns0:catalog="ISC">
        <time>
          <value>2013-02-28T14:05:50.870000Z</value>
        </time>
        <latitude>
          <value>50.7866</value>
        </latitude>
        <longitude>
          <value>157.4851</value>
        </longitude>
        <depth>
          <value>52800.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181091942">
        <mag>
          <value>6.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9026531">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6118129</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181091083</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="186">
        <text>VANUATU ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6118129" ns0:contributorOriginId="06014140" ns0:contributor="ISC" ns0:contributorEventId="606807158" ns0:catalog="ISC">
        <time>
          <value>2013-02-28T03:09:44.930000Z</value>
        </time>
        <latitude>
          <value>-17.7315</value>
        </latitude>
        <longitude>
          <value>167.4074</value>
        </longitude>
        <depth>
          <value>22300.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181091083">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9025015">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6116613</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181088471</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="181">
        <text>FIJI ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6116613" ns0:contributorOriginId="06014018" ns0:contributor="ISC" ns0:contributorEventId="602563843" ns0:catalog="ISC">
        <time>
          <value>2013-02-26T19:57:54.010000Z</value>
        </time>
        <latitude>
          <value>-21.45</value>
        </latitude>
        <longitude>
          <value>-179.2791</value>
        </longitude>
        <depth>
          <value>603500.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181088471">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9022603">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6114201</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181084356</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="227">
        <text>EASTERN HONSHU, JAPAN</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6114201" ns0:contributorOriginId="06013827" ns0:contributor="ISC" ns0:contributorEventId="602550753" ns0:catalog="ISC">
        <time>
          <value>2013-02-25T07:23:56.270000Z</value>
        </time>
        <latitude>
          <value>36.8846</value>
        </latitude>
        <longitude>
          <value>139.3749</value>
        </longitude>
        <depth>
          <value>13800.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181084356">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9020402">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6112000</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181080257</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="289">
        <text>TIMOR REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6112000" ns0:contributorOriginId="06013609" ns0:contributor="ISC" ns0:contributorEventId="602550186" ns0:catalog="ISC">
        <time>
          <value>2013-02-23T11:09:21.910000Z</value>
        </time>
        <latitude>
          <value>-8.5949</value>
        </latitude>
        <longitude>
          <value>127.4488</value>
        </longitude>
        <depth>
          <value>34500.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181080257">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9020302">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6111900</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181080061</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="184">
        <text>SANTA CRUZ ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6111900" ns0:contributorOriginId="06013599" ns0:contributor="ISC" ns0:contributorEventId="602563815" ns0:catalog="ISC">
        <time>
          <value>2013-02-23T08:59:09.260000Z</value>
        </time>
        <latitude>
          <value>-10.6335</value>
        </latitude>
        <longitude>
          <value>165.3222</value>
        </longitude>
        <depth>
          <value>16000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181080061">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4212015">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6110972</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181078403</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="132">
        <text>SANTIAGO DEL ESTERO PROV., ARG.</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6110972" ns0:contributorOriginId="06013523" ns0:contributor="ISC" ns0:contributorEventId="602539432" ns0:catalog="ISC">
        <time>
          <value>2013-02-22T12:01:58.670000Z</value>
        </time>
        <latitude>
          <value>-27.9052</value>
        </latitude>
        <longitude>
          <value>-63.1349</value>
        </longitude>
        <depth>
          <value>585600.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181078403">
        <mag>
          <value>6.1</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9014547">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6106145</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181069663</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="178">
        <text>KERMADEC ISLANDS, NEW ZEALAND</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6106145" ns0:contributorOriginId="06013043" ns0:contributor="ISC" ns0:contributorEventId="602447121" ns0:catalog="ISC">
        <time>
          <value>2013-02-18T12:19:18.460000Z</value>
        </time>
        <latitude>
          <value>-30.6582</value>
        </latitude>
        <longitude>
          <value>-177.9564</value>
        </longitude>
        <depth>
          <value>35300.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181069663">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9011459">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6103057</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181064076</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="160">
        <text>OFF E. COAST OF N. ISLAND, N.Z.</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6103057" ns0:contributorOriginId="06012722" ns0:contributor="ISC" ns0:contributorEventId="602563766" ns0:catalog="ISC">
        <time>
          <value>2013-02-16T05:16:16.760000Z</value>
        </time>
        <latitude>
          <value>-36.2346</value>
        </latitude>
        <longitude>
          <value>178.0295</value>
        </longitude>
        <depth>
          <value>198400.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181064076">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4211642">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6103026</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181064006</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="259">
        <text>MINDANAO, PHILIPPINES</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6103026" ns0:contributorOriginId="06012717" ns0:contributor="ISC" ns0:contributorEventId="602444221" ns0:catalog="ISC">
        <time>
          <value>2013-02-16T04:37:37.320000Z</value>
        </time>
        <latitude>
          <value>5.7785</value>
        </latitude>
        <longitude>
          <value>125.7792</value>
        </longitude>
        <depth>
          <value>116400.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181064006">
        <mag>
          <value>6.2</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4211576">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6100200</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181060217</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="671">
        <text>EASTERN SIBERIA, RUSSIA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6100200" ns0:contributorOriginId="06012516" ns0:contributor="ISC" ns0:contributorEventId="602437689" ns0:catalog="ISC">
        <time>
          <value>2013-02-14T13:13:52.720000Z</value>
        </time>
        <latitude>
          <value>67.5173</value>
        </latitude>
        <longitude>
          <value>142.7017</value>
        </longitude>
        <depth>
          <value>8900.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181060217">
        <mag>
          <value>6.7</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9005239">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6096837</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181052754</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="184">
        <text>SANTA CRUZ ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6096837" ns0:contributorOriginId="06012075" ns0:contributor="ISC" ns0:contributorEventId="602431767" ns0:catalog="ISC">
        <time>
          <value>2013-02-11T09:40:22.750000Z</value>
        </time>
        <latitude>
          <value>-11.0271</value>
        </latitude>
        <longitude>
          <value>166.8953</value>
        </longitude>
        <depth>
          <value>30300.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181052754">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4211297">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6096167</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181051171</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="184">
        <text>SANTA CRUZ ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6096167" ns0:contributorOriginId="06011988" ns0:contributor="ISC" ns0:contributorEventId="602430344" ns0:catalog="ISC">
        <time>
          <value>2013-02-10T18:39:36.130000Z</value>
        </time>
        <latitude>
          <value>-10.9733</value>
        </latitude>
        <longitude>
          <value>165.5442</value>
        </longitude>
        <depth>
          <value>35700.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181051171">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4211244">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6095032</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181048547</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="184">
        <text>SANTA CRUZ ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6095032" ns0:contributorOriginId="06011821" ns0:contributor="ISC" ns0:contributorEventId="602430301" ns0:catalog="ISC">
        <time>
          <value>2013-02-09T21:02:22.690000Z</value>
        </time>
        <latitude>
          <value>-10.9577</value>
        </latitude>
        <longitude>
          <value>165.8868</value>
        </longitude>
        <depth>
          <value>17900.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181048547">
        <mag>
          <value>6.6</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4209613">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6094637</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181047737</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="103">
        <text>COLOMBIA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6094637" ns0:contributorOriginId="06011768" ns0:contributor="ISC" ns0:contributorEventId="602430285" ns0:catalog="ISC">
        <time>
          <value>2013-02-09T14:16:06.960000Z</value>
        </time>
        <latitude>
          <value>1.0345</value>
        </latitude>
        <longitude>
          <value>-77.462</value>
        </longitude>
        <depth>
          <value>144200.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181047737">
        <mag>
          <value>7.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9002080">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6093678</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181045330</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="184">
        <text>SANTA CRUZ ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6093678" ns0:contributorOriginId="06011600" ns0:contributor="ISC" ns0:contributorEventId="602563708" ns0:catalog="ISC">
        <time>
          <value>2013-02-08T18:07:47.540000Z</value>
        </time>
        <latitude>
          <value>-10.8286</value>
        </latitude>
        <longitude>
          <value>166.5745</value>
        </longitude>
        <depth>
          <value>10000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181045330">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4184250">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6093535</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181044896</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="184">
        <text>SANTA CRUZ ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6093535" ns0:contributorOriginId="06011555" ns0:contributor="ISC" ns0:contributorEventId="602563705" ns0:catalog="ISC">
        <time>
          <value>2013-02-08T15:26:38.770000Z</value>
        </time>
        <latitude>
          <value>-10.8933</value>
        </latitude>
        <longitude>
          <value>166.1453</value>
        </longitude>
        <depth>
          <value>22400.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181044896">
        <mag>
          <value>7.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9001920">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6093518</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181044847</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="184">
        <text>SANTA CRUZ ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6093518" ns0:contributorOriginId="06011553" ns0:contributor="ISC" ns0:contributorEventId="606936801" ns0:catalog="ISC">
        <time>
          <value>2013-02-08T15:00:11.700000Z</value>
        </time>
        <latitude>
          <value>-10.9019</value>
        </latitude>
        <longitude>
          <value>166.0015</value>
        </longitude>
        <depth>
          <value>10000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181044847">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4179722">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6093277</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181044120</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="184">
        <text>SANTA CRUZ ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6093277" ns0:contributorOriginId="06011485" ns0:contributor="ISC" ns0:contributorEventId="602428508" ns0:catalog="ISC">
        <time>
          <value>2013-02-08T11:12:13.280000Z</value>
        </time>
        <latitude>
          <value>-10.8703</value>
        </latitude>
        <longitude>
          <value>166.0088</value>
        </longitude>
        <depth>
          <value>12700.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181044120">
        <mag>
          <value>6.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9000913">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6092511</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181041955</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="184">
        <text>SANTA CRUZ ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6092511" ns0:contributorOriginId="06011335" ns0:contributor="ISC" ns0:contributorEventId="606936582" ns0:catalog="ISC">
        <time>
          <value>2013-02-07T19:48:17.840000Z</value>
        </time>
        <latitude>
          <value>-10.8225</value>
        </latitude>
        <longitude>
          <value>165.8507</value>
        </longitude>
        <depth>
          <value>10000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181041955">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4157285">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6092455</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181041795</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="184">
        <text>SANTA CRUZ ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6092455" ns0:contributorOriginId="06011322" ns0:contributor="ISC" ns0:contributorEventId="602426976" ns0:catalog="ISC">
        <time>
          <value>2013-02-07T18:59:16.580000Z</value>
        </time>
        <latitude>
          <value>-11.0207</value>
        </latitude>
        <longitude>
          <value>165.7077</value>
        </longitude>
        <depth>
          <value>13700.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181041795">
        <mag>
          <value>6.7</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9000212">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6091810</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181040061</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="183">
        <text>SANTA CRUZ ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6091810" ns0:contributorOriginId="06011206" ns0:contributor="ISC" ns0:contributorEventId="602563687" ns0:catalog="ISC">
        <time>
          <value>2013-02-07T08:03:42.690000Z</value>
        </time>
        <latitude>
          <value>-11.0974</value>
        </latitude>
        <longitude>
          <value>164.8014</value>
        </longitude>
        <depth>
          <value>23600.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181040061">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=9000172">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6091770</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181039947</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="169">
        <text>SAMOA ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6091770" ns0:contributorOriginId="06011197" ns0:contributor="ISC" ns0:contributorEventId="602857735" ns0:catalog="ISC">
        <time>
          <value>2013-02-07T07:19:52.580000Z</value>
        </time>
        <latitude>
          <value>-15.8342</value>
        </latitude>
        <longitude>
          <value>-172.9224</value>
        </longitude>
        <depth>
          <value>26000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181039947">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4131985">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6091433</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181039069</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="184">
        <text>SANTA CRUZ ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6091433" ns0:contributorOriginId="06011136" ns0:contributor="ISC" ns0:contributorEventId="602426049" ns0:catalog="ISC">
        <time>
          <value>2013-02-07T00:30:12.810000Z</value>
        </time>
        <latitude>
          <value>-11.7566</value>
        </latitude>
        <longitude>
          <value>165.0307</value>
        </longitude>
        <depth>
          <value>25300.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181039069">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8999121">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6090719</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181037066</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="184">
        <text>SANTA CRUZ ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6090719" ns0:contributorOriginId="06010971" ns0:contributor="ISC" ns0:contributorEventId="602857508" ns0:catalog="ISC">
        <time>
          <value>2013-02-06T13:54:56.220000Z</value>
        </time>
        <latitude>
          <value>-10.8134</value>
        </latitude>
        <longitude>
          <value>166.5028</value>
        </longitude>
        <depth>
          <value>24000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181037066">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8999025">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6090623</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181036739</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="184">
        <text>SANTA CRUZ ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6090623" ns0:contributorOriginId="06010943" ns0:contributor="ISC" ns0:contributorEventId="602422873" ns0:catalog="ISC">
        <time>
          <value>2013-02-06T12:44:32.930000Z</value>
        </time>
        <latitude>
          <value>-11.6465</value>
        </latitude>
        <longitude>
          <value>165.5487</value>
        </longitude>
        <depth>
          <value>30900.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181036739">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4114504">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6090555</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181036498</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="184">
        <text>SANTA CRUZ ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6090555" ns0:contributorOriginId="06010924" ns0:contributor="ISC" ns0:contributorEventId="602563676" ns0:catalog="ISC">
        <time>
          <value>2013-02-06T11:53:54.820000Z</value>
        </time>
        <latitude>
          <value>-11.2312</value>
        </latitude>
        <longitude>
          <value>165.8721</value>
        </longitude>
        <depth>
          <value>15000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181036498">
        <mag>
          <value>6.1</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8998896">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6090494</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181036340</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="184">
        <text>SANTA CRUZ ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6090494" ns0:contributorOriginId="06010911" ns0:contributor="ISC" ns0:contributorEventId="602422239" ns0:catalog="ISC">
        <time>
          <value>2013-02-06T11:03:45.880000Z</value>
        </time>
        <latitude>
          <value>-10.6776</value>
        </latitude>
        <longitude>
          <value>165.2943</value>
        </longitude>
        <depth>
          <value>10000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181036340">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8998863">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6090461</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181036233</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="183">
        <text>SANTA CRUZ ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6090461" ns0:contributorOriginId="06010902" ns0:contributor="ISC" ns0:contributorEventId="602563674" ns0:catalog="ISC">
        <time>
          <value>2013-02-06T10:33:17.270000Z</value>
        </time>
        <latitude>
          <value>-10.6644</value>
        </latitude>
        <longitude>
          <value>164.9301</value>
        </longitude>
        <depth>
          <value>10000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181036233">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8998848">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6090446</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181036186</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="184">
        <text>SANTA CRUZ ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6090446" ns0:contributorOriginId="06010900" ns0:contributor="ISC" ns0:contributorEventId="602563673" ns0:catalog="ISC">
        <time>
          <value>2013-02-06T10:20:35.090000Z</value>
        </time>
        <latitude>
          <value>-10.8184</value>
        </latitude>
        <longitude>
          <value>165.3027</value>
        </longitude>
        <depth>
          <value>14300.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181036186">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8998582">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6090180</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181035253</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="183">
        <text>SANTA CRUZ ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6090180" ns0:contributorOriginId="06010810" ns0:contributor="ISC" ns0:contributorEventId="606936077" ns0:catalog="ISC">
        <time>
          <value>2013-02-06T06:53:19.670000Z</value>
        </time>
        <latitude>
          <value>-10.7009</value>
        </latitude>
        <longitude>
          <value>164.9865</value>
        </longitude>
        <depth>
          <value>8700.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181035253">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4107319">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6090158</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181035160</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="183">
        <text>SANTA CRUZ ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6090158" ns0:contributorOriginId="06010798" ns0:contributor="ISC" ns0:contributorEventId="602563665" ns0:catalog="ISC">
        <time>
          <value>2013-02-06T06:35:22.030000Z</value>
        </time>
        <latitude>
          <value>-10.8605</value>
        </latitude>
        <longitude>
          <value>164.6145</value>
        </longitude>
        <depth>
          <value>29000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181035160">
        <mag>
          <value>6.1</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4100745">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6089804</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181033807</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="184">
        <text>SANTA CRUZ ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6089804" ns0:contributorOriginId="06010647" ns0:contributor="ISC" ns0:contributorEventId="606935895" ns0:catalog="ISC">
        <time>
          <value>2013-02-06T01:54:15.040000Z</value>
        </time>
        <latitude>
          <value>-10.5087</value>
        </latitude>
        <longitude>
          <value>165.8168</value>
        </longitude>
        <depth>
          <value>9900.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181033807">
        <mag>
          <value>7.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4100102">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6089769</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181033647</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="183">
        <text>SANTA CRUZ ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6089769" ns0:contributorOriginId="06010626" ns0:contributor="ISC" ns0:contributorEventId="602419455" ns0:catalog="ISC">
        <time>
          <value>2013-02-06T01:23:23.580000Z</value>
        </time>
        <latitude>
          <value>-11.2569</value>
        </latitude>
        <longitude>
          <value>164.93</value>
        </longitude>
        <depth>
          <value>34000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181033647">
        <mag>
          <value>7.1</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8998162">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6089760</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181033604</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="183">
        <text>SANTA CRUZ ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6089760" ns0:contributorOriginId="06010621" ns0:contributor="ISC" ns0:contributorEventId="602563637" ns0:catalog="ISC">
        <time>
          <value>2013-02-06T01:16:41.580000Z</value>
        </time>
        <latitude>
          <value>-11.0516</value>
        </latitude>
        <longitude>
          <value>164.7289</value>
        </longitude>
        <depth>
          <value>29000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181033604">
        <mag>
          <value>5.9</value>
        </mag>
        <type>mb</type>
        <creationInfo>
          <author>NEIC</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4099810">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6089758</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181033577</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="184">
        <text>SANTA CRUZ ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6089758" ns0:contributorOriginId="06010619" ns0:contributor="ISC" ns0:contributorEventId="602419452" ns0:catalog="ISC">
        <time>
          <value>2013-02-06T01:12:26.380000Z</value>
        </time>
        <latitude>
          <value>-10.864</value>
        </latitude>
        <longitude>
          <value>165.1418</value>
        </longitude>
        <depth>
          <value>23800.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181033577">
        <mag>
          <value>7.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4098494">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6089702</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181033438</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="184">
        <text>SANTA CRUZ ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6089702" ns0:contributorOriginId="06010611" ns0:contributor="ISC" ns0:contributorEventId="602419445" ns0:catalog="ISC">
        <time>
          <value>2013-02-06T00:07:21.730000Z</value>
        </time>
        <latitude>
          <value>-10.7807</value>
        </latitude>
        <longitude>
          <value>165.3291</value>
        </longitude>
        <depth>
          <value>7500.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181033438">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8994065">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6085663</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181025870</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="184">
        <text>SANTA CRUZ ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6085663" ns0:contributorOriginId="06010222" ns0:contributor="ISC" ns0:contributorEventId="602563618" ns0:catalog="ISC">
        <time>
          <value>2013-02-02T18:58:09.790000Z</value>
        </time>
        <latitude>
          <value>-11.0006</value>
        </latitude>
        <longitude>
          <value>165.3544</value>
        </longitude>
        <depth>
          <value>26400.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181025870">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4093861">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6085331</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181025364</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="224">
        <text>HOKKAIDO, JAPAN REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6085331" ns0:contributorOriginId="06010205" ns0:contributor="ISC" ns0:contributorEventId="602392263" ns0:catalog="ISC">
        <time>
          <value>2013-02-02T14:17:34.760000Z</value>
        </time>
        <latitude>
          <value>42.7847</value>
        </latitude>
        <longitude>
          <value>143.1474</value>
        </longitude>
        <depth>
          <value>105000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181025364">
        <mag>
          <value>6.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8993294">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6084892</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181024388</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="184">
        <text>SANTA CRUZ ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6084892" ns0:contributorOriginId="06010148" ns0:contributor="ISC" ns0:contributorEventId="602563615" ns0:catalog="ISC">
        <time>
          <value>2013-02-02T04:16:18.860000Z</value>
        </time>
        <latitude>
          <value>-10.9864</value>
        </latitude>
        <longitude>
          <value>165.3787</value>
        </longitude>
        <depth>
          <value>30000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181024388">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4075900">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6084615</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181023698</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="184">
        <text>SANTA CRUZ ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6084615" ns0:contributorOriginId="06010109" ns0:contributor="ISC" ns0:contributorEventId="602386888" ns0:catalog="ISC">
        <time>
          <value>2013-02-01T22:18:36.330000Z</value>
        </time>
        <latitude>
          <value>-11.1551</value>
        </latitude>
        <longitude>
          <value>165.4699</value>
        </longitude>
        <depth>
          <value>30000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181023698">
        <mag>
          <value>6.3</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4073005">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6084614</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181023676</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="184">
        <text>SANTA CRUZ ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6084614" ns0:contributorOriginId="06010108" ns0:contributor="ISC" ns0:contributorEventId="602386887" ns0:catalog="ISC">
        <time>
          <value>2013-02-01T22:16:37.420000Z</value>
        </time>
        <latitude>
          <value>-11.0263</value>
        </latitude>
        <longitude>
          <value>165.4374</value>
        </longitude>
        <depth>
          <value>30000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181023676">
        <mag>
          <value>6.4</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4057475">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6083711</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181022083</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="184">
        <text>SANTA CRUZ ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6083711" ns0:contributorOriginId="06010027" ns0:contributor="ISC" ns0:contributorEventId="602381215" ns0:catalog="ISC">
        <time>
          <value>2013-02-01T05:36:42.100000Z</value>
        </time>
        <latitude>
          <value>-11.1624</value>
        </latitude>
        <longitude>
          <value>165.4883</value>
        </longitude>
        <depth>
          <value>14800.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181022083">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8991962">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6083560</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181021828</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="207">
        <text>EASTERN NEW GUINEA REG., P.N.G.</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6083560" ns0:contributorOriginId="06010012" ns0:contributor="ISC" ns0:contributorEventId="602381212" ns0:catalog="ISC">
        <time>
          <value>2013-02-01T02:17:29.320000Z</value>
        </time>
        <latitude>
          <value>-7.03</value>
        </latitude>
        <longitude>
          <value>147.8471</value>
        </longitude>
        <depth>
          <value>54000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181021828">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8991042">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6082640</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181020183</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="19">
        <text>SOUTHEASTERN ALASKA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6082640" ns0:contributorOriginId="05896863" ns0:contributor="ISC" ns0:contributorEventId="602381641" ns0:catalog="ISC">
        <time>
          <value>2013-01-31T09:53:43.210000Z</value>
        </time>
        <latitude>
          <value>55.3694</value>
        </latitude>
        <longitude>
          <value>-134.8161</value>
        </longitude>
        <depth>
          <value>10000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181020183">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8990756">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6082354</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181019516</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="184">
        <text>SANTA CRUZ ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6082354" ns0:contributorOriginId="05896817" ns0:contributor="ISC" ns0:contributorEventId="602605845" ns0:catalog="ISC">
        <time>
          <value>2013-01-31T03:57:59.630000Z</value>
        </time>
        <latitude>
          <value>-10.6687</value>
        </latitude>
        <longitude>
          <value>166.4424</value>
        </longitude>
        <depth>
          <value>31700.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181019516">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4023507">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6082333</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181019459</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="184">
        <text>SANTA CRUZ ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6082333" ns0:contributorOriginId="05896814" ns0:contributor="ISC" ns0:contributorEventId="602363415" ns0:catalog="ISC">
        <time>
          <value>2013-01-31T03:33:45.270000Z</value>
        </time>
        <latitude>
          <value>-10.6377</value>
        </latitude>
        <longitude>
          <value>166.5421</value>
        </longitude>
        <depth>
          <value>22200.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181019459">
        <mag>
          <value>6.1</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4017384">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6082127</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181018974</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="184">
        <text>SANTA CRUZ ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6082127" ns0:contributorOriginId="05896782" ns0:contributor="ISC" ns0:contributorEventId="602401006" ns0:catalog="ISC">
        <time>
          <value>2013-01-30T23:03:47.710000Z</value>
        </time>
        <latitude>
          <value>-10.6598</value>
        </latitude>
        <longitude>
          <value>166.4759</value>
        </longitude>
        <depth>
          <value>36000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181018974">
        <mag>
          <value>6.1</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=4013357">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6081990</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181018725</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="136">
        <text>CENTRAL CHILE</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6081990" ns0:contributorOriginId="05896769" ns0:contributor="ISC" ns0:contributorEventId="602363406" ns0:catalog="ISC">
        <time>
          <value>2013-01-30T20:15:43.390000Z</value>
        </time>
        <latitude>
          <value>-28.1096</value>
        </latitude>
        <longitude>
          <value>-70.8883</value>
        </longitude>
        <depth>
          <value>46100.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181018725">
        <mag>
          <value>6.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3954492">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6078999</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181013334</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="330">
        <text>LAKE ISSYK-KUL REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6078999" ns0:contributorOriginId="05896420" ns0:contributor="ISC" ns0:contributorEventId="602358258" ns0:catalog="ISC">
        <time>
          <value>2013-01-28T16:38:53.840000Z</value>
        </time>
        <latitude>
          <value>42.5824</value>
        </latitude>
        <longitude>
          <value>79.7217</value>
        </longitude>
        <depth>
          <value>14300.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181013334">
        <mag>
          <value>6.2</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3906212">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6071940</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181000203</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="706">
        <text>NORTHERN SUMATRA, INDONESIA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6071940" ns0:contributorOriginId="05895747" ns0:contributor="ISC" ns0:contributorEventId="602342576" ns0:catalog="ISC">
        <time>
          <value>2013-01-21T22:22:54.780000Z</value>
        </time>
        <latitude>
          <value>4.9625</value>
        </latitude>
        <longitude>
          <value>95.9215</value>
        </longitude>
        <depth>
          <value>24600.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=181000203">
        <mag>
          <value>6.1</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3854304">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6065559</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180988293</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="691">
        <text>PACIFIC-ANTARCTIC RIDGE</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6065559" ns0:contributorOriginId="05895098" ns0:contributor="ISC" ns0:contributorEventId="602323421" ns0:catalog="ISC">
        <time>
          <value>2013-01-15T16:09:37.750000Z</value>
        </time>
        <latitude>
          <value>-62.5094</value>
        </latitude>
        <longitude>
          <value>-161.2762</value>
        </longitude>
        <depth>
          <value>10000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180988293">
        <mag>
          <value>6.1</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8966991">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6058589</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180974073</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="294">
        <text>MYANMAR-INDIA BORDER REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6058589" ns0:contributorOriginId="05894277" ns0:contributor="ISC" ns0:contributorEventId="602264460" ns0:catalog="ISC">
        <time>
          <value>2013-01-09T01:41:53.060000Z</value>
        </time>
        <latitude>
          <value>25.322</value>
        </latitude>
        <longitude>
          <value>95.0088</value>
        </longitude>
        <depth>
          <value>92600.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180974073">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8966356">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6057954</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180972650</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="365">
        <text>AEGEAN SEA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6057954" ns0:contributorOriginId="05894174" ns0:contributor="ISC" ns0:contributorEventId="602216240" ns0:catalog="ISC">
        <time>
          <value>2013-01-08T14:16:09.590000Z</value>
        </time>
        <latitude>
          <value>39.6681</value>
        </latitude>
        <longitude>
          <value>25.5372</value>
        </longitude>
        <depth>
          <value>11100.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180972650">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3800250">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6054559</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180965900</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="19">
        <text>SOUTHEASTERN ALASKA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6054559" ns0:contributorOriginId="05893791" ns0:contributor="ISC" ns0:contributorEventId="602213332" ns0:catalog="ISC">
        <time>
          <value>2013-01-05T08:58:19.180000Z</value>
        </time>
        <latitude>
          <value>55.2347</value>
        </latitude>
        <longitude>
          <value>-134.8008</value>
        </longitude>
        <depth>
          <value>3100.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180965900">
        <mag>
          <value>7.5</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8955572">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6047170</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180951751</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="203">
        <text>BISMARCK SEA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6047170" ns0:contributorOriginId="05630471" ns0:contributor="ISC" ns0:contributorEventId="602062477" ns0:catalog="ISC">
        <time>
          <value>2012-12-29T07:59:43.690000Z</value>
        </time>
        <latitude>
          <value>-3.5881</value>
        </latitude>
        <longitude>
          <value>148.8295</value>
        </longitude>
        <depth>
          <value>19000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180951751">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8952981">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6044579</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180946940</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="134">
        <text>OFF COAST OF CENTRAL CHILE</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6044579" ns0:contributorOriginId="05630176" ns0:contributor="ISC" ns0:contributorEventId="602059082" ns0:catalog="ISC">
        <time>
          <value>2012-12-27T00:37:07.860000Z</value>
        </time>
        <latitude>
          <value>-35.7872</value>
        </latitude>
        <longitude>
          <value>-73.3197</value>
        </longitude>
        <depth>
          <value>10000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180946940">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8949324">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6040922</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180939973</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="362">
        <text>NORTHWESTERN CAUCASUS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6040922" ns0:contributorOriginId="05629672" ns0:contributor="ISC" ns0:contributorEventId="602057923" ns0:catalog="ISC">
        <time>
          <value>2012-12-23T13:31:40.320000Z</value>
        </time>
        <latitude>
          <value>42.4407</value>
        </latitude>
        <longitude>
          <value>41.0577</value>
        </longitude>
        <depth>
          <value>12900.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180939973">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3740878">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6039373</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180937129</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="186">
        <text>VANUATU ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6039373" ns0:contributorOriginId="05629506" ns0:contributor="ISC" ns0:contributorEventId="602090225" ns0:catalog="ISC">
        <time>
          <value>2012-12-21T22:28:08.080000Z</value>
        </time>
        <latitude>
          <value>-14.3591</value>
        </latitude>
        <longitude>
          <value>167.2778</value>
        </longitude>
        <depth>
          <value>198100.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180937129">
        <mag>
          <value>6.7</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8943556">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6035154</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180929187</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="694">
        <text>CENTRAL EAST PACIFIC RISE</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6035154" ns0:contributorOriginId="05629035" ns0:contributor="ISC" ns0:contributorEventId="602046727" ns0:catalog="ISC">
        <time>
          <value>2012-12-17T17:41:32.640000Z</value>
        </time>
        <latitude>
          <value>-4.0407</value>
        </latitude>
        <longitude>
          <value>-104.3582</value>
        </longitude>
        <depth>
          <value>10000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180929187">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3712118">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6034656</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180928403</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="265">
        <text>MINAHASSA PENINSULA, SULAWESI</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6034656" ns0:contributorOriginId="05628991" ns0:contributor="ISC" ns0:contributorEventId="602043844" ns0:catalog="ISC">
        <time>
          <value>2012-12-17T09:16:31.250000Z</value>
        </time>
        <latitude>
          <value>-0.6936</value>
        </latitude>
        <longitude>
          <value>123.8836</value>
        </longitude>
        <depth>
          <value>46400.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180928403">
        <mag>
          <value>6.1</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3712078">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6032918</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180925048</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="190">
        <text>NEW IRELAND REGION, P.N.G.</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6032918" ns0:contributorOriginId="05628799" ns0:contributor="ISC" ns0:contributorEventId="602043763" ns0:catalog="ISC">
        <time>
          <value>2012-12-15T19:30:03.140000Z</value>
        </time>
        <latitude>
          <value>-4.6402</value>
        </latitude>
        <longitude>
          <value>153.0768</value>
        </longitude>
        <depth>
          <value>58500.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180925048">
        <mag>
          <value>6.1</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3694259">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6032292</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180923832</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="5">
        <text>NEAR ISLANDS, ALEUTIAN ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6032292" ns0:contributorOriginId="05628727" ns0:contributor="ISC" ns0:contributorEventId="602043659" ns0:catalog="ISC">
        <time>
          <value>2012-12-15T04:49:31.450000Z</value>
        </time>
        <latitude>
          <value>52.2361</value>
        </latitude>
        <longitude>
          <value>174.0651</value>
        </longitude>
        <depth>
          <value>38800.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180923832">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8940217">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6031815</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180922973</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="181">
        <text>FIJI ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6031815" ns0:contributorOriginId="05628675" ns0:contributor="ISC" ns0:contributorEventId="602043562" ns0:catalog="ISC">
        <time>
          <value>2012-12-14T16:52:42.770000Z</value>
        </time>
        <latitude>
          <value>-15.5481</value>
        </latitude>
        <longitude>
          <value>-177.9119</value>
        </longitude>
        <depth>
          <value>35000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180922973">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3675581">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6031512</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180922451</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="47">
        <text>OFF W. COAST OF BAJA CALIFORNIA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6031512" ns0:contributorOriginId="05628646" ns0:contributor="ISC" ns0:contributorEventId="602043521" ns0:catalog="ISC">
        <time>
          <value>2012-12-14T10:36:01.840000Z</value>
        </time>
        <latitude>
          <value>31.1122</value>
        </latitude>
        <longitude>
          <value>-119.6273</value>
        </longitude>
        <depth>
          <value>15800.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180922451">
        <mag>
          <value>6.4</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3650568">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6028289</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180916221</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="266">
        <text>NORTHERN MOLUCCA SEA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6028289" ns0:contributorOriginId="05628286" ns0:contributor="ISC" ns0:contributorEventId="602036632" ns0:catalog="ISC">
        <time>
          <value>2012-12-11T06:18:28.980000Z</value>
        </time>
        <latitude>
          <value>0.5606</value>
        </latitude>
        <longitude>
          <value>126.2235</value>
        </longitude>
        <depth>
          <value>43800.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180916221">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3650528">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6027693</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180914878</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="280">
        <text>BANDA SEA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6027693" ns0:contributorOriginId="05628198" ns0:contributor="ISC" ns0:contributorEventId="605137262" ns0:catalog="ISC">
        <time>
          <value>2012-12-10T16:53:09.490000Z</value>
        </time>
        <latitude>
          <value>-6.4969</value>
        </latitude>
        <longitude>
          <value>129.8684</value>
        </longitude>
        <depth>
          <value>161500.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180914878">
        <mag>
          <value>7.1</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8935314">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6026912</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180913184</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="259">
        <text>MINDANAO, PHILIPPINES</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6026912" ns0:contributorOriginId="05628080" ns0:contributor="ISC" ns0:contributorEventId="602032405" ns0:catalog="ISC">
        <time>
          <value>2012-12-09T21:45:34.270000Z</value>
        </time>
        <latitude>
          <value>6.6672</value>
        </latitude>
        <longitude>
          <value>126.1822</value>
        </longitude>
        <depth>
          <value>56600.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180913184">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8934126">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6025724</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180910464</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="206">
        <text>NEAR S COAST OF NEW GUINEA, PNG.</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6025724" ns0:contributorOriginId="05627893" ns0:contributor="ISC" ns0:contributorEventId="602032199" ns0:catalog="ISC">
        <time>
          <value>2012-12-08T16:35:18.200000Z</value>
        </time>
        <latitude>
          <value>-7.3018</value>
        </latitude>
        <longitude>
          <value>144.0199</value>
        </longitude>
        <depth>
          <value>19700.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180910464">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3650421">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6024753</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180908090</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="159">
        <text>NORTH ISLAND, NEW ZEALAND</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6024753" ns0:contributorOriginId="05627727" ns0:contributor="ISC" ns0:contributorEventId="602090156" ns0:catalog="ISC">
        <time>
          <value>2012-12-07T18:19:07.880000Z</value>
        </time>
        <latitude>
          <value>-38.3574</value>
        </latitude>
        <longitude>
          <value>176.0726</value>
        </longitude>
        <depth>
          <value>171000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180908090">
        <mag>
          <value>6.3</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8932522">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6024120</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180906229</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="229">
        <text>OFF EAST COAST OF HONSHU, JAPAN</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6024120" ns0:contributorOriginId="05627561" ns0:contributor="ISC" ns0:contributorEventId="602005588" ns0:catalog="ISC">
        <time>
          <value>2012-12-07T08:31:13.710000Z</value>
        </time>
        <latitude>
          <value>37.9116</value>
        </latitude>
        <longitude>
          <value>143.9076</value>
        </longitude>
        <depth>
          <value>31900.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180906229">
        <mag>
          <value>6.2</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3650366">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6024110</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180906187</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="229">
        <text>OFF EAST COAST OF HONSHU, JAPAN</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6024110" ns0:contributorOriginId="05627556" ns0:contributor="ISC" ns0:contributorEventId="602005586" ns0:catalog="ISC">
        <time>
          <value>2012-12-07T08:18:23.630000Z</value>
        </time>
        <latitude>
          <value>37.8201</value>
        </latitude>
        <longitude>
          <value>144.1594</value>
        </longitude>
        <depth>
          <value>35300.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180906187">
        <mag>
          <value>7.2</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8931014">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6022612</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180903415</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="348">
        <text>NORTHERN AND CENTRAL IRAN</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6022612" ns0:contributorOriginId="05627413" ns0:contributor="ISC" ns0:contributorEventId="601996088" ns0:catalog="ISC">
        <time>
          <value>2012-12-05T17:08:13.520000Z</value>
        </time>
        <latitude>
          <value>33.5052</value>
        </latitude>
        <longitude>
          <value>59.6055</value>
        </longitude>
        <depth>
          <value>13000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180903415">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8929453">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6021051</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180900493</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="2">
        <text>SOUTHERN ALASKA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6021051" ns0:contributorOriginId="05627216" ns0:contributor="ISC" ns0:contributorEventId="601996019" ns0:catalog="ISC">
        <time>
          <value>2012-12-04T01:42:47.850000Z</value>
        </time>
        <latitude>
          <value>61.2727</value>
        </latitude>
        <longitude>
          <value>-150.8181</value>
        </longitude>
        <depth>
          <value>68400.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180900493">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3650121">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6019148</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180896848</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="186">
        <text>VANUATU ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6019148" ns0:contributorOriginId="05627010" ns0:contributor="ISC" ns0:contributorEventId="601995719" ns0:catalog="ISC">
        <time>
          <value>2012-12-02T00:54:23.210000Z</value>
        </time>
        <latitude>
          <value>-17.0957</value>
        </latitude>
        <longitude>
          <value>167.6748</value>
        </longitude>
        <depth>
          <value>40000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180896848">
        <mag>
          <value>6.1</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8925124">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6016722</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180892222</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="200">
        <text>NEAR N COAST OF NEW GUINEA, PNG.</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6016722" ns0:contributorOriginId="05593846" ns0:contributor="ISC" ns0:contributorEventId="601995233" ns0:catalog="ISC">
        <time>
          <value>2012-11-29T11:10:25.720000Z</value>
        </time>
        <latitude>
          <value>-3.592</value>
        </latitude>
        <longitude>
          <value>145.7396</value>
        </longitude>
        <depth>
          <value>25000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180892222">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8917356">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6008954</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180878249</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="129">
        <text>SALTA PROVINCE, ARGENTINA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6008954" ns0:contributorOriginId="05593108" ns0:contributor="ISC" ns0:contributorEventId="601993726" ns0:catalog="ISC">
        <time>
          <value>2012-11-22T13:07:11.880000Z</value>
        </time>
        <latitude>
          <value>-22.7006</value>
        </latitude>
        <longitude>
          <value>-63.6265</value>
        </longitude>
        <depth>
          <value>535300.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180878249">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8916612">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6008210</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180876981</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="134">
        <text>OFF COAST OF CENTRAL CHILE</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6008210" ns0:contributorOriginId="05593054" ns0:contributor="ISC" ns0:contributorEventId="601993538" ns0:catalog="ISC">
        <time>
          <value>2012-11-21T21:36:22.210000Z</value>
        </time>
        <latitude>
          <value>-33.9661</value>
        </latitude>
        <longitude>
          <value>-72.1405</value>
        </longitude>
        <depth>
          <value>17400.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180876981">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3649365">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6005508</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180871981</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="192">
        <text>NEW BRITAIN REGION, P.N.G.</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6005508" ns0:contributorOriginId="05592773" ns0:contributor="ISC" ns0:contributorEventId="601992895" ns0:catalog="ISC">
        <time>
          <value>2012-11-19T09:44:35.480000Z</value>
        </time>
        <latitude>
          <value>-5.7528</value>
        </latitude>
        <longitude>
          <value>151.6402</value>
        </longitude>
        <depth>
          <value>22900.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180871981">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8911623">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6003221</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180867777</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="174">
        <text>TONGA ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6003221" ns0:contributorOriginId="05592534" ns0:contributor="ISC" ns0:contributorEventId="601992383" ns0:catalog="ISC">
        <time>
          <value>2012-11-17T05:12:55.690000Z</value>
        </time>
        <latitude>
          <value>-18.3498</value>
        </latitude>
        <longitude>
          <value>-172.2855</value>
        </longitude>
        <depth>
          <value>6200.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180867777">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3649288">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6002826</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180867004</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="221">
        <text>KURIL ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6002826" ns0:contributorOriginId="05592478" ns0:contributor="ISC" ns0:contributorEventId="606632346" ns0:catalog="ISC">
        <time>
          <value>2012-11-16T18:12:43.260000Z</value>
        </time>
        <latitude>
          <value>49.2365</value>
        </latitude>
        <longitude>
          <value>155.6545</value>
        </longitude>
        <depth>
          <value>50800.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180867004">
        <mag>
          <value>6.5</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3649015">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6001372</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180864302</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="59">
        <text>GUERRERO, MEXICO</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6001372" ns0:contributorOriginId="05592321" ns0:contributor="ISC" ns0:contributorEventId="601901948" ns0:catalog="ISC">
        <time>
          <value>2012-11-15T09:20:21.450000Z</value>
        </time>
        <latitude>
          <value>18.1995</value>
        </latitude>
        <longitude>
          <value>-100.5249</value>
        </longitude>
        <depth>
          <value>57900.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180864302">
        <mag>
          <value>6.1</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8909730">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6001328</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180864223</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="203">
        <text>BISMARCK SEA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6001328" ns0:contributorOriginId="05592318" ns0:contributor="ISC" ns0:contributorEventId="601901505" ns0:catalog="ISC">
        <time>
          <value>2012-11-15T08:21:49.860000Z</value>
        </time>
        <latitude>
          <value>-3.1152</value>
        </latitude>
        <longitude>
          <value>148.491</value>
        </longitude>
        <depth>
          <value>17900.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180864223">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3649004">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=6000806</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180863088</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="135">
        <text>NEAR COAST OF CENTRAL CHILE</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=6000806" ns0:contributorOriginId="05592251" ns0:contributor="ISC" ns0:contributorEventId="601901485" ns0:catalog="ISC">
        <time>
          <value>2012-11-14T19:02:05.690000Z</value>
        </time>
        <latitude>
          <value>-29.1789</value>
        </latitude>
        <longitude>
          <value>-71.3863</value>
        </longitude>
        <depth>
          <value>61500.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180863088">
        <mag>
          <value>6.2</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3625781">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=5998996</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180859550</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="143">
        <text>OFF COAST OF SOUTHERN CHILE</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=5998996" ns0:contributorOriginId="05592059" ns0:contributor="ISC" ns0:contributorEventId="601890535" ns0:catalog="ISC">
        <time>
          <value>2012-11-13T04:31:30.980000Z</value>
        </time>
        <latitude>
          <value>-45.669</value>
        </latitude>
        <longitude>
          <value>-77.0841</value>
        </longitude>
        <depth>
          <value>35000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180859550">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8907068">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=5998666</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180858875</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="15">
        <text>GULF OF ALASKA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=5998666" ns0:contributorOriginId="05592021" ns0:contributor="ISC" ns0:contributorEventId="601890509" ns0:catalog="ISC">
        <time>
          <value>2012-11-12T20:42:15.870000Z</value>
        </time>
        <latitude>
          <value>57.557</value>
        </latitude>
        <longitude>
          <value>-142.8596</value>
        </longitude>
        <depth>
          <value>19000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180858875">
        <mag>
          <value>6.3</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3604966">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=5997718</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180856825</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="69">
        <text>NEAR COAST OF CHIAPAS, MEXICO</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=5997718" ns0:contributorOriginId="05591882" ns0:contributor="ISC" ns0:contributorEventId="601884078" ns0:catalog="ISC">
        <time>
          <value>2012-11-11T22:14:59.050000Z</value>
        </time>
        <latitude>
          <value>14.0705</value>
        </latitude>
        <longitude>
          <value>-92.3318</value>
        </longitude>
        <depth>
          <value>18600.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180856825">
        <mag>
          <value>6.4</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8905684">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=5997282</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180855951</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="296">
        <text>MYANMAR</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=5997282" ns0:contributorOriginId="05591830" ns0:contributor="ISC" ns0:contributorEventId="601883941" ns0:catalog="ISC">
        <time>
          <value>2012-11-11T10:54:43.440000Z</value>
        </time>
        <latitude>
          <value>22.6992</value>
        </latitude>
        <longitude>
          <value>95.7655</value>
        </longitude>
        <depth>
          <value>17500.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180855951">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3604919">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=5996883</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180855070</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="296">
        <text>MYANMAR</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=5996883" ns0:contributorOriginId="05591769" ns0:contributor="ISC" ns0:contributorEventId="601885134" ns0:catalog="ISC">
        <time>
          <value>2012-11-11T01:12:39.150000Z</value>
        </time>
        <latitude>
          <value>22.8908</value>
        </latitude>
        <longitude>
          <value>95.848</value>
        </longitude>
        <depth>
          <value>14200.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180855070">
        <mag>
          <value>6.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3604911">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=5996443</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180854222</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="116">
        <text>CENTRAL PERU</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=5996443" ns0:contributorOriginId="05591721" ns0:contributor="ISC" ns0:contributorEventId="601883740" ns0:catalog="ISC">
        <time>
          <value>2012-11-10T14:57:50.210000Z</value>
        </time>
        <latitude>
          <value>-8.9367</value>
        </latitude>
        <longitude>
          <value>-75.0884</value>
        </longitude>
        <depth>
          <value>128600.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180854222">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3580118">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=5993739</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180849125</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="25">
        <text>VANCOUVER ISLAND, CANADA REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=5993739" ns0:contributorOriginId="05591431" ns0:contributor="ISC" ns0:contributorEventId="601880080" ns0:catalog="ISC">
        <time>
          <value>2012-11-08T02:01:49.700000Z</value>
        </time>
        <latitude>
          <value>49.1261</value>
        </latitude>
        <longitude>
          <value>-128.5411</value>
        </longitude>
        <depth>
          <value>11000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180849125">
        <mag>
          <value>6.2</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3567125">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=5993344</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180848208</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="69">
        <text>NEAR COAST OF CHIAPAS, MEXICO</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=5993344" ns0:contributorOriginId="05591364" ns0:contributor="ISC" ns0:contributorEventId="601879562" ns0:catalog="ISC">
        <time>
          <value>2012-11-07T16:35:49.310000Z</value>
        </time>
        <latitude>
          <value>14.0318</value>
        </latitude>
        <longitude>
          <value>-92.0669</value>
        </longitude>
        <depth>
          <value>38700.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180848208">
        <mag>
          <value>7.4</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3560976">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=5988244</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180838802</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="259">
        <text>MINDANAO, PHILIPPINES</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=5988244" ns0:contributorOriginId="05590856" ns0:contributor="ISC" ns0:contributorEventId="601868898" ns0:catalog="ISC">
        <time>
          <value>2012-11-02T18:17:34.680000Z</value>
        </time>
        <latitude>
          <value>9.235</value>
        </latitude>
        <longitude>
          <value>126.2811</value>
        </longitude>
        <depth>
          <value>55500.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180838802">
        <mag>
          <value>6.1</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3533867">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=5984319</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180831813</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="22">
        <text>QUEEN CHARLOTTE ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=5984319" ns0:contributorOriginId="05456161" ns0:contributor="ISC" ns0:contributorEventId="601867005" ns0:catalog="ISC">
        <time>
          <value>2012-10-30T02:49:01.320000Z</value>
        </time>
        <latitude>
          <value>52.145</value>
        </latitude>
        <longitude>
          <value>-132.1115</value>
        </longitude>
        <depth>
          <value>9500.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180831813">
        <mag>
          <value>6.2</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8891248">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=5982846</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180828237</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="22">
        <text>QUEEN CHARLOTTE ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=5982846" ns0:contributorOriginId="05455897" ns0:contributor="ISC" ns0:contributorEventId="601857808" ns0:catalog="ISC">
        <time>
          <value>2012-10-28T19:09:54.670000Z</value>
        </time>
        <latitude>
          <value>52.2321</value>
        </latitude>
        <longitude>
          <value>-131.9203</value>
        </longitude>
        <depth>
          <value>18900.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180828237">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3533766">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=5982830</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180828151</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="22">
        <text>QUEEN CHARLOTTE ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=5982830" ns0:contributorOriginId="05455891" ns0:contributor="ISC" ns0:contributorEventId="601857803" ns0:catalog="ISC">
        <time>
          <value>2012-10-28T18:54:20.230000Z</value>
        </time>
        <latitude>
          <value>52.5384</value>
        </latitude>
        <longitude>
          <value>-132.6591</value>
        </longitude>
        <depth>
          <value>5100.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180828151">
        <mag>
          <value>6.4</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3533701">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=5982040</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180825614</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="22">
        <text>QUEEN CHARLOTTE ISLANDS REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=5982040" ns0:contributorOriginId="05455643" ns0:contributor="ISC" ns0:contributorEventId="603860908" ns0:catalog="ISC">
        <time>
          <value>2012-10-28T03:04:07.790000Z</value>
        </time>
        <latitude>
          <value>52.6777</value>
        </latitude>
        <longitude>
          <value>-132.1724</value>
        </longitude>
        <depth>
          <value>7400.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180825614">
        <mag>
          <value>7.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3521310">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=5976709</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180816862</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="78">
        <text>COSTA RICA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=5976709" ns0:contributorOriginId="05455251" ns0:contributor="ISC" ns0:contributorEventId="601844006" ns0:catalog="ISC">
        <time>
          <value>2012-10-24T00:45:33.800000Z</value>
        </time>
        <latitude>
          <value>10.0745</value>
        </latitude>
        <longitude>
          <value>-85.3716</value>
        </longitude>
        <depth>
          <value>22300.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180816862">
        <mag>
          <value>6.4</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8884410">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=5976008</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180815657</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="189">
        <text>SOUTHEAST OF LOYALTY ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=5976008" ns0:contributorOriginId="05455194" ns0:contributor="ISC" ns0:contributorEventId="601874209" ns0:catalog="ISC">
        <time>
          <value>2012-10-23T09:39:30.370000Z</value>
        </time>
        <latitude>
          <value>-22.273</value>
        </latitude>
        <longitude>
          <value>171.6892</value>
        </longitude>
        <depth>
          <value>122800.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180815657">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8884373">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=5975971</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180815562</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="211">
        <text>SOUTHEAST OF HONSHU, JAPAN</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=5975971" ns0:contributorOriginId="05455189" ns0:contributor="ISC" ns0:contributorEventId="601842550" ns0:catalog="ISC">
        <time>
          <value>2012-10-23T08:53:38.830000Z</value>
        </time>
        <latitude>
          <value>29.0656</value>
        </latitude>
        <longitude>
          <value>139.2767</value>
        </longitude>
        <depth>
          <value>445600.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180815562">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3480924">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=5973351</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180810924</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="186">
        <text>VANUATU ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=5973351" ns0:contributorOriginId="05454937" ns0:contributor="ISC" ns0:contributorEventId="601874182" ns0:catalog="ISC">
        <time>
          <value>2012-10-20T23:00:32.930000Z</value>
        </time>
        <latitude>
          <value>-13.573</value>
        </latitude>
        <longitude>
          <value>166.6105</value>
        </longitude>
        <depth>
          <value>43500.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180810924">
        <mag>
          <value>6.2</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8878505">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=5970103</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180805337</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="701">
        <text>WEST OF MACQUARIE ISLAND</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=5970103" ns0:contributorOriginId="05454652" ns0:contributor="ISC" ns0:contributorEventId="601838150" ns0:catalog="ISC">
        <time>
          <value>2012-10-18T01:27:16.090000Z</value>
        </time>
        <latitude>
          <value>-54.3051</value>
        </latitude>
        <longitude>
          <value>144.2082</value>
        </longitude>
        <depth>
          <value>11300.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180805337">
        <mag>
          <value>5.9</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3480694">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=5969066</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180803744</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="262">
        <text>CELEBES SEA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=5969066" ns0:contributorOriginId="05454573" ns0:contributor="ISC" ns0:contributorEventId="601837362" ns0:catalog="ISC">
        <time>
          <value>2012-10-17T04:42:31.060000Z</value>
        </time>
        <latitude>
          <value>4.1791</value>
        </latitude>
        <longitude>
          <value>124.5614</value>
        </longitude>
        <depth>
          <value>338400.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180803744">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8874440">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=5966038</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180798236</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="221">
        <text>KURIL ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=5966038" ns0:contributorOriginId="05454231" ns0:contributor="ISC" ns0:contributorEventId="601833194" ns0:catalog="ISC">
        <time>
          <value>2012-10-14T09:41:59.960000Z</value>
        </time>
        <latitude>
          <value>48.2556</value>
        </latitude>
        <longitude>
          <value>154.5874</value>
        </longitude>
        <depth>
          <value>46800.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180798236">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8874232">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=5965830</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180797890</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="193">
        <text>SOLOMON ISLANDS</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=5965830" ns0:contributorOriginId="05454214" ns0:contributor="ISC" ns0:contributorEventId="606632145" ns0:catalog="ISC">
        <time>
          <value>2012-10-14T04:58:07.170000Z</value>
        </time>
        <latitude>
          <value>-7.1731</value>
        </latitude>
        <longitude>
          <value>156.1262</value>
        </longitude>
        <depth>
          <value>62600.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180797890">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3443511">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=5963224</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180793673</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="196">
        <text>IRIAN JAYA REGION, INDONESIA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=5963224" ns0:contributorOriginId="05454043" ns0:contributor="ISC" ns0:contributorEventId="601828936" ns0:catalog="ISC">
        <time>
          <value>2012-10-12T00:31:31.370000Z</value>
        </time>
        <latitude>
          <value>-4.8858</value>
        </latitude>
        <longitude>
          <value>134.0822</value>
        </longitude>
        <depth>
          <value>34600.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180793673">
        <mag>
          <value>6.6</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3443380">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=5960515</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180789035</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="701">
        <text>WEST OF MACQUARIE ISLAND</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=5960515" ns0:contributorOriginId="05453805" ns0:contributor="ISC" ns0:contributorEventId="601810590" ns0:catalog="ISC">
        <time>
          <value>2012-10-09T12:32:08.820000Z</value>
        </time>
        <latitude>
          <value>-60.6414</value>
        </latitude>
        <longitude>
          <value>153.9333</value>
        </longitude>
        <depth>
          <value>12100.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180789035">
        <mag>
          <value>6.6</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3443291">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=5959344</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180786810</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="280">
        <text>BANDA SEA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=5959344" ns0:contributorOriginId="05453691" ns0:contributor="ISC" ns0:contributorEventId="601795099" ns0:catalog="ISC">
        <time>
          <value>2012-10-08T11:43:34.100000Z</value>
        </time>
        <latitude>
          <value>-4.5414</value>
        </latitude>
        <longitude>
          <value>129.2347</value>
        </longitude>
        <depth>
          <value>29600.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180786810">
        <mag>
          <value>6.2</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3443289">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=5959086</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180786372</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="49">
        <text>GULF OF CALIFORNIA</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=5959086" ns0:contributorOriginId="05453673" ns0:contributor="ISC" ns0:contributorEventId="606632086" ns0:catalog="ISC">
        <time>
          <value>2012-10-08T06:26:23.590000Z</value>
        </time>
        <latitude>
          <value>25.0882</value>
        </latitude>
        <longitude>
          <value>-109.6135</value>
        </longitude>
        <depth>
          <value>17000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180786372">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=8867304">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=5958902</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180786050</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="124">
        <text>CHILE-BOLIVIA BORDER REGION</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=5958902" ns0:contributorOriginId="05453658" ns0:contributor="ISC" ns0:contributorEventId="601794915" ns0:catalog="ISC">
        <time>
          <value>2012-10-08T01:50:25.960000Z</value>
        </time>
        <latitude>
          <value>-21.7976</value>
        </latitude>
        <longitude>
          <value>-68.3792</value>
        </longitude>
        <depth>
          <value>122900.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180786050">
        <mag>
          <value>5.8</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
    <event publicID="smi:service.iris.edu/fdsnws/event/1/query?eventid=3414148">
      <preferredOriginID>smi:service.iris.edu/fdsnws/event/1/query?originid=5951976</preferredOriginID>
      <preferredMagnitudeID>smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180773631</preferredMagnitudeID>
      <type>earthquake</type>
      <description ns0:FEcode="229">
        <text>OFF EAST COAST OF HONSHU, JAPAN</text>
        <type>Flinn-Engdahl region</type>
      </description>
      <origin publicID="smi:service.iris.edu/fdsnws/event/1/query?originid=5951976" ns0:contributorOriginId="05453039" ns0:contributor="ISC" ns0:contributorEventId="601776277" ns0:catalog="ISC">
        <time>
          <value>2012-10-01T22:21:46.110000Z</value>
        </time>
        <latitude>
          <value>39.8235</value>
        </latitude>
        <longitude>
          <value>143.2093</value>
        </longitude>
        <depth>
          <value>18000.0</value>
        </depth>
        <creationInfo>
          <author>ISC</author>
        </creationInfo>
      </origin>
      <magnitude publicID="smi:service.iris.edu/fdsnws/event/1/query?magnitudeid=180773631">
        <mag>
          <value>6.0</value>
        </mag>
        <type>MW</type>
        <creationInfo>
          <author>GCMT</author>
        </creationInfo>
      </magnitude>
    </event>
  </eventParameters>
</q:quakeml>
